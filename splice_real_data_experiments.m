clc;
clear all;
close all;

%% Screen size used to place plots
screensize = get(groot, 'ScreenSize');
screenwidth = screensize(3);
screenheight = screensize(4);
default_width = screenwidth / 2;
default_height = screenheight / 2;
%default_height = 1200 / 2;
fg = 1;

%% Load parameters
param = load_crazyflie_parameters();
Nd = 25;

%% Specify data files
take_num = 02;
file_prefix = ['Real Data/Step Tracking/LQR yaw control/PRBS 331 p=3 Experiments/10000 Regularizer (Large)/Take ', num2str(take_num, '%02d'), '/'];
out_1_num = (take_num - 1) * 2 + 1;
out_2_num = (take_num - 1) * 2 + 2;

u_data_DeePC_file = strcat(file_prefix, 'm_u_data_Deepc.csv');
uf_data_DeePC_file = strcat(file_prefix, 'm_uf_data_Deepc.csv');
y_data_DeePC_file = strcat(file_prefix, 'm_y_data_Deepc.csv');
yf_data_DeePC_file = strcat(file_prefix, 'm_yf_data_Deepc.csv');
r_data_DeePC_file = strcat(file_prefix, 'm_r_data_Deepc.csv');
solveTime_data_DeePC_file = strcat(file_prefix, 'm_solveTime_data_Deepc.csv');

%% Reference outputs
% Reference is x, y, z, yaw
% If DeePC yaw control is disabled, do not track yaw
if param.DeePC_yaw_control
    if param.measure_angles
        ref_outputs = [1; 2; 3; 6];
    else
        ref_outputs = [1; 2; 3; 4];
    end
    ref_states = [1; 2; 3; 9];
else
    ref_outputs = [1; 2; 3];
    ref_states = [1; 2; 3];
end

%% Get data
u_data_DeePC = csvread(u_data_DeePC_file);

uf_data_DeePC = csvread(uf_data_DeePC_file);

y_data_DeePC = csvread(y_data_DeePC_file);
y_data_DeePC = y_data_DeePC(ref_outputs,:);

yf_data_DeePC = csvread(yf_data_DeePC_file);

r_data_DeePC = csvread(r_data_DeePC_file);

solveTime_data_DeePC = csvread(solveTime_data_DeePC_file);

%% Splice data
step_up_z = 1.5;
step_down_z = 0.5;
step_mid_z = 1;
pre_step_time_sec = 2;
post_step_time_sec = 10;
pre_step_timesteps = ceil(pre_step_time_sec / param.sample_time_measurements_full_state);
post_step_timesteps = ceil(post_step_time_sec / param.sample_time_measurements_full_state);

u_data_DeePC_temp = u_data_DeePC;
uf_data_DeePC_temp = uf_data_DeePC;
y_data_DeePC_temp = y_data_DeePC;
yf_data_DeePC_temp = yf_data_DeePC;
r_data_DeePC_temp = r_data_DeePC;
solveTime_data_DeePC_temp = solveTime_data_DeePC;

% First step up
find_i = find(r_data_DeePC_temp(3,:) == step_up_z);
find_i = find_i(1);
end_i = min([find_i + post_step_timesteps, size(r_data_DeePC_temp, 2)]);
start_i = max([find_i - pre_step_timesteps, 1]);
u_data_DeePC_step_up_1 = u_data_DeePC_temp(:,start_i:end_i);
uf_data_DeePC_step_up_1 = uf_data_DeePC_temp(:,start_i:end_i);
y_data_DeePC_step_up_1 = y_data_DeePC_temp(:,start_i:end_i);
yf_data_DeePC_step_up_1 = yf_data_DeePC_temp(:,start_i:end_i);
r_data_DeePC_step_up_1 = r_data_DeePC_temp(:,start_i:end_i);
solveTime_data_DeePC_step_up_1 = solveTime_data_DeePC_temp(:,start_i:end_i);

dlmwrite(strcat(file_prefix, 'u_data_DeePC_step_up_', num2str(out_1_num, '%02d'), '.csv'), u_data_DeePC_step_up_1);
dlmwrite(strcat(file_prefix, 'uf_data_DeePC_step_up_', num2str(out_1_num, '%02d'), '.csv'), uf_data_DeePC_step_up_1);
dlmwrite(strcat(file_prefix, 'y_data_DeePC_step_up_', num2str(out_1_num, '%02d'), '.csv'), y_data_DeePC_step_up_1);
dlmwrite(strcat(file_prefix, 'yf_data_DeePC_step_up_', num2str(out_1_num, '%02d'), '.csv'), yf_data_DeePC_step_up_1);
dlmwrite(strcat(file_prefix, 'r_data_DeePC_step_up_', num2str(out_1_num, '%02d'), '.csv'), r_data_DeePC_step_up_1);
dlmwrite(strcat(file_prefix, 'solveTime_data_DeePC_step_up_', num2str(out_1_num, '%02d'), '.csv'), solveTime_data_DeePC_step_up_1);

u_data_DeePC_temp = u_data_DeePC_temp(:,find_i:end);
uf_data_DeePC_temp = uf_data_DeePC_temp(:,find_i:end);
y_data_DeePC_temp = y_data_DeePC_temp(:,find_i:end);
yf_data_DeePC_temp = yf_data_DeePC_temp(:,find_i:end);
r_data_DeePC_temp = r_data_DeePC_temp(:,find_i:end);

% First step down
find_i = find(r_data_DeePC_temp(3,:) == step_down_z);
find_i = find_i(1);
end_i = min([find_i + post_step_timesteps, size(r_data_DeePC_temp, 2)]);
start_i = max([find_i - pre_step_timesteps, 1]);
u_data_DeePC_step_down_1 = u_data_DeePC_temp(:,start_i:end_i);
uf_data_DeePC_step_down_1 = uf_data_DeePC_temp(:,start_i:end_i);
y_data_DeePC_step_down_1 = y_data_DeePC_temp(:,start_i:end_i);
yf_data_DeePC_step_down_1 = yf_data_DeePC_temp(:,start_i:end_i);
r_data_DeePC_step_down_1 = r_data_DeePC_temp(:,start_i:end_i);
solveTime_data_DeePC_step_down_1 = solveTime_data_DeePC_temp(:,start_i:end_i);

dlmwrite(strcat(file_prefix, 'u_data_DeePC_step_down_', num2str(out_1_num, '%02d'), '.csv'), u_data_DeePC_step_down_1);
dlmwrite(strcat(file_prefix, 'uf_data_DeePC_step_down_', num2str(out_1_num, '%02d'), '.csv'), uf_data_DeePC_step_down_1);
dlmwrite(strcat(file_prefix, 'y_data_DeePC_step_down_', num2str(out_1_num, '%02d'), '.csv'), y_data_DeePC_step_down_1);
dlmwrite(strcat(file_prefix, 'yf_data_DeePC_step_down_', num2str(out_1_num, '%02d'), '.csv'), yf_data_DeePC_step_down_1);
dlmwrite(strcat(file_prefix, 'r_data_DeePC_step_down_', num2str(out_1_num, '%02d'), '.csv'), r_data_DeePC_step_down_1);
dlmwrite(strcat(file_prefix, 'solveTime_data_DeePC_step_down_', num2str(out_1_num, '%02d'), '.csv'), solveTime_data_DeePC_step_down_1);

u_data_DeePC_temp = u_data_DeePC_temp(:,find_i:end);
uf_data_DeePC_temp = uf_data_DeePC_temp(:,find_i:end);
y_data_DeePC_temp = y_data_DeePC_temp(:,find_i:end);
yf_data_DeePC_temp = yf_data_DeePC_temp(:,find_i:end);
r_data_DeePC_temp = r_data_DeePC_temp(:,find_i:end);

% First step mid
find_i = find(r_data_DeePC_temp(3,:) == step_mid_z);
find_i = find_i(1);
end_i = min([find_i + post_step_timesteps, size(r_data_DeePC_temp, 2)]);
start_i = max([find_i - pre_step_timesteps, 1]);
u_data_DeePC_step_mid_1 = u_data_DeePC_temp(:,start_i:end_i);
uf_data_DeePC_step_mid_1 = uf_data_DeePC_temp(:,start_i:end_i);
y_data_DeePC_step_mid_1 = y_data_DeePC_temp(:,start_i:end_i);
yf_data_DeePC_step_mid_1 = yf_data_DeePC_temp(:,start_i:end_i);
r_data_DeePC_step_mid_1 = r_data_DeePC_temp(:,start_i:end_i);
solveTime_data_DeePC_step_mid_1 = solveTime_data_DeePC_temp(:,start_i:end_i);

dlmwrite(strcat(file_prefix, 'u_data_DeePC_step_mid_', num2str(out_1_num, '%02d'), '.csv'), u_data_DeePC_step_mid_1);
dlmwrite(strcat(file_prefix, 'uf_data_DeePC_step_mid_', num2str(out_1_num, '%02d'), '.csv'), uf_data_DeePC_step_mid_1);
dlmwrite(strcat(file_prefix, 'y_data_DeePC_step_mid_', num2str(out_1_num, '%02d'), '.csv'), y_data_DeePC_step_mid_1);
dlmwrite(strcat(file_prefix, 'yf_data_DeePC_step_mid_', num2str(out_1_num, '%02d'), '.csv'), yf_data_DeePC_step_mid_1);
dlmwrite(strcat(file_prefix, 'r_data_DeePC_step_mid_', num2str(out_1_num, '%02d'), '.csv'), r_data_DeePC_step_mid_1);
dlmwrite(strcat(file_prefix, 'solveTime_data_DeePC_step_mid_', num2str(out_1_num, '%02d'), '.csv'), solveTime_data_DeePC_step_mid_1);

u_data_DeePC_temp = u_data_DeePC_temp(:,find_i:end);
uf_data_DeePC_temp = uf_data_DeePC_temp(:,find_i:end);
y_data_DeePC_temp = y_data_DeePC_temp(:,find_i:end);
yf_data_DeePC_temp = yf_data_DeePC_temp(:,find_i:end);
r_data_DeePC_temp = r_data_DeePC_temp(:,find_i:end);

% Second step up
find_i = find(r_data_DeePC_temp(3,:) == step_up_z);
find_i = find_i(1);
end_i = min([find_i + post_step_timesteps, size(r_data_DeePC_temp, 2)]);
start_i = max([find_i - pre_step_timesteps, 1]);
u_data_DeePC_step_up_2 = u_data_DeePC_temp(:,start_i:end_i);
uf_data_DeePC_step_up_2 = uf_data_DeePC_temp(:,start_i:end_i);
y_data_DeePC_step_up_2 = y_data_DeePC_temp(:,start_i:end_i);
yf_data_DeePC_step_up_2 = yf_data_DeePC_temp(:,start_i:end_i);
r_data_DeePC_step_up_2 = r_data_DeePC_temp(:,start_i:end_i);
solveTime_data_DeePC_step_up_2 = solveTime_data_DeePC_temp(:,start_i:end_i);

dlmwrite(strcat(file_prefix, 'u_data_DeePC_step_up_', num2str(out_2_num, '%02d'), '.csv'), u_data_DeePC_step_up_2);
dlmwrite(strcat(file_prefix, 'uf_data_DeePC_step_up_', num2str(out_2_num, '%02d'), '.csv'), uf_data_DeePC_step_up_2);
dlmwrite(strcat(file_prefix, 'y_data_DeePC_step_up_', num2str(out_2_num, '%02d'), '.csv'), y_data_DeePC_step_up_2);
dlmwrite(strcat(file_prefix, 'yf_data_DeePC_step_up_', num2str(out_2_num, '%02d'), '.csv'), yf_data_DeePC_step_up_2);
dlmwrite(strcat(file_prefix, 'r_data_DeePC_step_up_', num2str(out_2_num, '%02d'), '.csv'), r_data_DeePC_step_up_2);
dlmwrite(strcat(file_prefix, 'solveTime_data_DeePC_step_up_', num2str(out_2_num, '%02d'), '.csv'), solveTime_data_DeePC_step_up_2);

u_data_DeePC_temp = u_data_DeePC_temp(:,find_i:end);
uf_data_DeePC_temp = uf_data_DeePC_temp(:,find_i:end);
y_data_DeePC_temp = y_data_DeePC_temp(:,find_i:end);
yf_data_DeePC_temp = yf_data_DeePC_temp(:,find_i:end);
r_data_DeePC_temp = r_data_DeePC_temp(:,find_i:end);

% Second step down
find_i = find(r_data_DeePC_temp(3,:) == step_down_z);
find_i = find_i(1);
end_i = min([find_i + post_step_timesteps, size(r_data_DeePC_temp, 2)]);
start_i = max([find_i - pre_step_timesteps, 1]);
u_data_DeePC_step_down_2 = u_data_DeePC_temp(:,start_i:end_i);
uf_data_DeePC_step_down_2 = uf_data_DeePC_temp(:,start_i:end_i);
y_data_DeePC_step_down_2 = y_data_DeePC_temp(:,start_i:end_i);
yf_data_DeePC_step_down_2 = yf_data_DeePC_temp(:,start_i:end_i);
r_data_DeePC_step_down_2 = r_data_DeePC_temp(:,start_i:end_i);
solveTime_data_DeePC_step_down_2 = solveTime_data_DeePC_temp(:,start_i:end_i);

dlmwrite(strcat(file_prefix, 'u_data_DeePC_step_down_', num2str(out_2_num, '%02d'), '.csv'), u_data_DeePC_step_down_2);
dlmwrite(strcat(file_prefix, 'uf_data_DeePC_step_down_', num2str(out_2_num, '%02d'), '.csv'), uf_data_DeePC_step_down_2);
dlmwrite(strcat(file_prefix, 'y_data_DeePC_step_down_', num2str(out_2_num, '%02d'), '.csv'), y_data_DeePC_step_down_2);
dlmwrite(strcat(file_prefix, 'yf_data_DeePC_step_down_', num2str(out_2_num, '%02d'), '.csv'), yf_data_DeePC_step_down_2);
dlmwrite(strcat(file_prefix, 'r_data_DeePC_step_down_', num2str(out_2_num, '%02d'), '.csv'), r_data_DeePC_step_down_2);
dlmwrite(strcat(file_prefix, 'solveTime_data_DeePC_step_down_', num2str(out_2_num, '%02d'), '.csv'), solveTime_data_DeePC_step_down_2);

u_data_DeePC_temp = u_data_DeePC_temp(:,find_i:end);
uf_data_DeePC_temp = uf_data_DeePC_temp(:,find_i:end);
y_data_DeePC_temp = y_data_DeePC_temp(:,find_i:end);
yf_data_DeePC_temp = yf_data_DeePC_temp(:,find_i:end);
r_data_DeePC_temp = r_data_DeePC_temp(:,find_i:end);

% Second step mid
find_i = find(r_data_DeePC_temp(3,:) == step_mid_z);
find_i = find_i(1);
end_i = min([find_i + post_step_timesteps, size(r_data_DeePC_temp, 2)]);
start_i = max([find_i - pre_step_timesteps, 1]);
u_data_DeePC_step_mid_2 = u_data_DeePC_temp(:,start_i:end_i);
uf_data_DeePC_step_mid_2 = uf_data_DeePC_temp(:,start_i:end_i);
y_data_DeePC_step_mid_2 = y_data_DeePC_temp(:,start_i:end_i);
yf_data_DeePC_step_mid_2 = yf_data_DeePC_temp(:,start_i:end_i);
r_data_DeePC_step_mid_2 = r_data_DeePC_temp(:,start_i:end_i);
solveTime_data_DeePC_step_mid_2 = solveTime_data_DeePC_temp(:,start_i:end_i);

dlmwrite(strcat(file_prefix, 'u_data_DeePC_step_mid_', num2str(out_2_num, '%02d'), '.csv'), u_data_DeePC_step_mid_2);
dlmwrite(strcat(file_prefix, 'uf_data_DeePC_step_mid_', num2str(out_2_num, '%02d'), '.csv'), uf_data_DeePC_step_mid_2);
dlmwrite(strcat(file_prefix, 'y_data_DeePC_step_mid_', num2str(out_2_num, '%02d'), '.csv'), y_data_DeePC_step_mid_2);
dlmwrite(strcat(file_prefix, 'yf_data_DeePC_step_mid_', num2str(out_2_num, '%02d'), '.csv'), yf_data_DeePC_step_mid_2);
dlmwrite(strcat(file_prefix, 'r_data_DeePC_step_mid_', num2str(out_2_num, '%02d'), '.csv'), r_data_DeePC_step_mid_2);
dlmwrite(strcat(file_prefix, 'solveTime_data_DeePC_step_mid_', num2str(out_2_num, '%02d'), '.csv'), solveTime_data_DeePC_step_mid_2);

%% Some text for labels
u_text = ["$F_{DeePC}$ $[N]$"; "$\omega^{(B)}_{x_{DeePC}}$ $[\frac{rad}{s}]$";...
    "$\omega^{(B)}_{y_{DeePC}}$ $[\frac{rad}{s}]$"; "$\omega^{(B)}_{z_{DeePC}}$ $[\frac{rad}{s}]$"];
upred_text = ["$F_{pred}$ $[N]$"; "$\omega^{(B)}_{x_{pred}}$ $[\frac{rad}{s}]$";...
    "$\omega^{(B)}_{y_{pred}}$ $[\frac{rad}{s}]$"; "$\omega^{(B)}_{z_{pred}}$ $[\frac{rad}{s}]$"];

x_text = ["$p^{(I)}_{x_{DeePC}}$ $[m]$"; "$p^{(I)}_{y_{DeePC}}$ $[m]$"; "$p^{(I)}_{z_{DeePC}}$ $[m]$";...
    "$\dot{p}^{(I)}_{x_{DeePC}}$ $[\frac{m}{s}]$"; "$\dot{p}^{(I)}_{y_{DeePC}}$ $[\frac{m}{s}]$"; "$\dot{p}^{(I)}_{z_{DeePC}}$ $[\frac{m}{s}]$";...
    "$\gamma_{DeePC}$ $[rad]$"; "$\beta_{DeePC}$ $[rad]$"; "$\alpha_{DeePC}$ $[rad]$"];
i = find(param.Cd(1,:) == 1);
y_text = x_text(i);
for i = 2 : size(param.Cd, 1)
    i = find(param.Cd(i,:) == 1);
    y_text = [y_text; x_text(i)];
end
yref_text = y_text(ref_outputs);

xpred_text = ["$p^{(I)}_{x_{pred}}$ $[m]$"; "$p^{(I)}_{y_{pred}}$ $[m]$"; "$p^{(I)}_{z_{pred}}$ $[m]$";...
    "$\dot{p}^{(I)}_{x_{pred}}$ $[\frac{m}{s}]$"; "$\dot{p}^{(I)}_{y_{pred}}$ $[\frac{m}{s}]$"; "$\dot{p}^{(I)}_{pred}}$ $[\frac{m}{s}]$";...
    "$\gamma_{pred}$ $[rad]$"; "$\beta_{pred}$ $[rad]$"; "$\alpha_{pred}$ $[rad]$"];
ypred_text = xpred_text(ref_states(1));
for i = 2 : length(ref_states)
    ypred_text = [ypred_text; xpred_text(ref_states(i))];
end

x_r_text = ["$p^{(I)}_{x_{ref}}$ $[m]$"; "$p^{(I)}_{y_{ref}}$ $[m]$"; "$p^{(I)}_{z_{ref}}$ $[m]$";...
    "$\dot{p}^{(I)}_{x_{ref}}$ $[\frac{m}{s}]$"; "$\dot{p}^{(I)}_{y_{ref}}$ $[\frac{m}{s}]$"; "$\dot{p}^{(I)}_{z_{ref}}$ $[\frac{m}{s}]$";...
    "$\gamma_{ref}$ $[rad]$"; "$\beta_{ref}$ $[rad]$"; "$\alpha_{ref}$ $[rad]$"];
r_text = x_r_text(ref_states(1));
for i = 2 : length(ref_states)
    r_text = [r_text; x_r_text(ref_states(i))];
end

%% Overview plot
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [0, 0, screenwidth, screenheight];

% DeePC Reference vs. output plot
subplot(3,2,1:2);
plot(r_data_DeePC(1,:), '--', 'LineWidth', 2);
lgd_text = r_text(1);
hold on;
plot(y_data_DeePC(1,:), 'LineWidth', 2);
lgd_text = [lgd_text, yref_text(1)];
for i = 2 : length(ref_outputs)
    plot(r_data_DeePC(i,:), '--', 'LineWidth', 2);
    lgd_text = [lgd_text, r_text(i)];
    plot(y_data_DeePC(i,:), 'LineWidth', 2);
    lgd_text = [lgd_text, yref_text(i)];
end
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'DeePC Reference vs. Output';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend(lgd_text);
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'northeast';

% DeePC Total thrust command plot
subplot(3,2,3);
plot(u_data_DeePC(1,:), 'LineWidth', 2);
lgd_text = u_text(1);
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'DeePC Total Thrust Command';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend(lgd_text);
lgd.Interpreter = 'latex';
lgd.FontSize = 10;
lgd.Location = 'northeast';

% DeePC Body rates command plot
subplot(3,2,4);
plot(u_data_DeePC(2,:), 'LineWidth', 2);
lgd_text = u_text(2);
hold on;
for i = 3 : param.m
    plot(u_data_DeePC(i,:), 'LineWidth', 2);
    lgd_text = [lgd_text, u_text(i)];
end
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'DeePC Body Rates Command';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend(lgd_text);
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'northeast';

% DeePC solve time plot
subplot(3,2,5:6);
plot(solveTime_data_DeePC, 'LineWidth', 2);
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Optimization Solve Time';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;
axes.YLabel.Interpreter = 'latex';
axes.YLabel.String = '$t_{solve}$ $[sec]$';
axes.YLabel.FontSize = 14;

%% Step mid plot
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [screenwidth / 4, 0, screenwidth / 2, screenheight / 2];

% DeePC Reference vs. output plot
subplot(3,2,1:2);
plot(r_data_DeePC_step_mid_1(1,:), '--', 'LineWidth', 2);
hold on;
plot(y_data_DeePC_step_mid_1(1,:));
for i = 2 : length(ref_outputs)
    plot(r_data_DeePC_step_mid_1(i,:), '--', 'LineWidth', 2);
    plot(y_data_DeePC_step_mid_1(i,:));
end
plot(y_data_DeePC_step_mid_2(1,:));
for i = 2 : length(ref_outputs)
    plot(y_data_DeePC_step_mid_2(i,:));
end
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Step Mid Reference vs. Output';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;

% DeePC Total thrust command plot
subplot(3,2,3);
plot(u_data_DeePC_step_mid_1(1,:));
hold on;
plot(u_data_DeePC_step_mid_2(1,:));
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Total Thrust Command';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;

% DeePC Body rates command plot
subplot(3,2,4);
plot(u_data_DeePC_step_mid_1(2,:));
hold on;
for i = 3 : param.m
    plot(u_data_DeePC_step_mid_1(i,:));
end
plot(u_data_DeePC_step_mid_2(2,:));
for i = 3 : param.m
    plot(u_data_DeePC_step_mid_2(i,:));
end
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Body Rates Command';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;

% DeePC solve time plot
subplot(3,2,5:6);
plot(solveTime_data_DeePC_step_mid_1);
hold on;
plot(solveTime_data_DeePC_step_mid_2);
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Optimization Solve Time';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;
axes.YLabel.Interpreter = 'latex';
axes.YLabel.String = '$t_{solve}$ $[sec]$';
axes.YLabel.FontSize = 14;

%% Step up plot
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [0, 0, screenwidth / 2, screenheight / 2];

% DeePC Reference vs. output plot
subplot(3,2,1:2);
plot(r_data_DeePC_step_up_1(1,:), '--', 'LineWidth', 2);
hold on;
plot(y_data_DeePC_step_up_1(1,:));
for i = 2 : length(ref_outputs)
    plot(r_data_DeePC_step_up_1(i,:), '--', 'LineWidth', 2);
    plot(y_data_DeePC_step_up_1(i,:));
end
plot(y_data_DeePC_step_up_2(1,:));
for i = 2 : length(ref_outputs)
    plot(y_data_DeePC_step_up_2(i,:));
end
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Step Up Reference vs. Output';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;

% DeePC Total thrust command plot
subplot(3,2,3);
plot(u_data_DeePC_step_up_1(1,:));
hold on;
plot(u_data_DeePC_step_up_2(1,:));
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Total Thrust Command';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;

% DeePC Body rates command plot
subplot(3,2,4);
plot(u_data_DeePC_step_up_1(2,:));
hold on;
for i = 3 : param.m
    plot(u_data_DeePC_step_up_1(i,:));
end
plot(u_data_DeePC_step_up_2(2,:));
for i = 3 : param.m
    plot(u_data_DeePC_step_up_2(i,:));
end
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Body Rates Command';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;

% DeePC solve time plot
subplot(3,2,5:6);
plot(solveTime_data_DeePC_step_up_1);
hold on;
plot(solveTime_data_DeePC_step_up_2);
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Optimization Solve Time';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;
axes.YLabel.Interpreter = 'latex';
axes.YLabel.String = '$t_{solve}$ $[sec]$';
axes.YLabel.FontSize = 14;

%% Step down plot
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [screenwidth / 2, 0, screenwidth / 2, screenheight / 2];

% DeePC Reference vs. output plot
subplot(3,2,1:2);
plot(r_data_DeePC_step_down_1(1,:), '--', 'LineWidth', 2);
hold on;
plot(y_data_DeePC_step_down_1(1,:));
for i = 2 : length(ref_outputs)
    plot(r_data_DeePC_step_down_1(i,:), '--', 'LineWidth', 2);
    plot(y_data_DeePC_step_down_1(i,:));
end
plot(y_data_DeePC_step_down_2(1,:));
for i = 2 : length(ref_outputs)
    plot(y_data_DeePC_step_down_2(i,:));
end
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Step Down Reference vs. Output';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;

% DeePC Total thrust command plot
subplot(3,2,3);
plot(u_data_DeePC_step_down_1(1,:));
hold on;
plot(u_data_DeePC_step_down_2(1,:));
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Total Thrust Command';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;

% DeePC Body rates command plot
subplot(3,2,4);
plot(u_data_DeePC_step_down_1(2,:));
hold on;
for i = 3 : param.m
    plot(u_data_DeePC_step_down_1(i,:));
end
plot(u_data_DeePC_step_down_2(2,:));
for i = 3 : param.m
    plot(u_data_DeePC_step_down_2(i,:));
end
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Body Rates Command';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;

% DeePC solve time plot
subplot(3,2,5:6);
plot(solveTime_data_DeePC_step_down_1);
hold on;
plot(solveTime_data_DeePC_step_down_2);
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Optimization Solve Time';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;
axes.YLabel.Interpreter = 'latex';
axes.YLabel.String = '$t_{solve}$ $[sec]$';
axes.YLabel.FontSize = 14;
