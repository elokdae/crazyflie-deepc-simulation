clear;
close all;
clc;

%% Load parameters
param = load_crazyflie_parameters();

%% Turn noise on/off prior to data collection
param.measurement_noise_full_state = 1;
param.measurement_noise_body_rates = 1;
param.measurement_noise_body_accelerations = 1;

%% Set initial condition prior to data collection
param.nrotor_initial_condition(1:3) = [0.0; 0.0; 1.0];

%% Collect data
% Main tuning parmeters

% Additional data points over persistence of excitation
Td = 331;

% Tini
Tini = 6;

% exc_signal_select: 1 = random signal, 2 = PRBS
exc_signal_select = 2;

% rng_seed: Random seed used for random excitation signal generation (not
% used for PRBS)
rng_seed = 0;

% Generate excitation signals
exc_param = generate_excitation_signal(param,...
    exc_signal_select, rng_seed, Td, Tini);

if param.use_real_data
    u_data = csvread('Real Data/m_u_data.csv');
    y_data = csvread('Real Data/m_y_data.csv');
    if ~param.DeePC_yaw_control
        u_data(end,:) = [];
        y_data(end,:) = [];
    end
    if ~param.measure_angles
        y_data(end,:) = [];
        y_data(end,:) = [];
    end
else
    % Data collection mode: 0 => linear model with gravity
    %                       1 => non-linear model
    data_collection_mode = 1;
    [u_data, y_data] = collect_DeePC_data(param, exc_param,...
        data_collection_mode, Tini);
end
fprintf('Data collected\n\n');
    
%% Run data collection on linear model with no noise for comparison if
% required
if param.compare_to_perf
    param.measurement_noise_full_state = 0;
    param.measurement_noise_body_rates = 0;
    param.measurement_noise_body_accelerations = 0;
    
    % Data collection mode: 0 => linear model with gravity
    data_collection_mode_perf = 0;
    [u_data_perf, y_data_perf] = collect_DeePC_data(param, exc_param,...
    data_collection_mode_perf, Tini);
    
    fprintf('Linear noise-free data collected for comparison\n\n');
end


%% Setup DeePC optimization
% Main tuning parameters

% Quadratic state cost matrix coefficients
Q_x = param.Q(1,1);
Q_y = param.Q(2,2);
Q_z = param.Q(3,3);
Q_yaw = param.Q(end,end);

% Quadratic input cost matrix coefficients
R_thrust = param.R(1,1);
R_roll = param.R(2,2);
R_pitch = param.R(3,3);
R_yaw = param.R(4,4);

% Quadratic state cost matrix coefficients
% P_x = param.P(1,1);
% P_x_pitch = param.P(1,8);
% P_y = param.P(2,2);
% P_y_roll = param.P(2,7);
% P_z = param.P(3,3);
% P_roll = param.P(7,7);
% P_pitch = param.P(8,8);
% P_yaw = param.P(9,9);
P_x = 0;
P_x_pitch = 0;
P_y = 0;
P_y_roll = 0;
P_z = 0;
P_roll = 0;
P_pitch = 0;
P_yaw = 0;

% SVD weighing flag. When true V.'*g is norm-2 penalized instead of g
svd_weighing = false;

% SVD weighing cutoff. Singular values above and including cutoff are not
% penalized when SVD weighing is enabled
svd_cutoff = 0;

% 1-norm penalty on g
lambda1_g = 0e0;

% 2-norm penalty on g. If SVD weighing is enabled, 2-norm penalty on V.'*g
% below svd_cutoff
lambda2_g = 5.0e2;

% 1-norm penalty on yini slack
lambda1_s = 0e5;

% 2-norm penalty on yini slack
lambda2_s = 7.5e8;

switch param.optimizer
    case 'gurobi'
        if param.setup_sparse_opt
            [DeePC_param, grb_model, grb_params]...
                = setup_DeePC_gurobi_sparse(param, exc_param, u_data, y_data,...
                Tini, svd_weighing, svd_cutoff, lambda2_g, lambda2_s,...
                Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
                P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw);
        else
            [DeePC_param, grb_model, grb_params]...
                = setup_DeePC_gurobi(param, exc_param, u_data, y_data,...
                Tini, svd_weighing, svd_cutoff, lambda2_g, lambda2_s,...
                Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
                P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw);
        end
        us_out = 0;
    case 'osqp'
        if param.setup_sparse_opt
            if param.opt_steady_state
                [DeePC_param, osqp_model]...
                    = setup_DeePC_osqp_sparse_ss(param, exc_param, u_data, y_data,...
                    Tini, svd_weighing, svd_cutoff, lambda2_g, lambda2_s,...
                    Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
                    P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw);
            else
                [DeePC_param, osqp_model]...
                    = setup_DeePC_osqp_sparse(param, exc_param, u_data, y_data,...
                    Tini, svd_weighing, svd_cutoff, lambda2_g, lambda2_s,...
                    Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
                    P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw);
            end
        else
            [DeePC_param, osqp_model]...
                = setup_DeePC_osqp(param, exc_param, u_data, y_data,...
                Tini, svd_weighing, svd_cutoff, lambda2_g, lambda2_s,...
                Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
                P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw);
        end
        us_out = 0;
    otherwise   % Use yalmip by default
        if param.setup_sparse_opt
            if param.opt_steady_state
                [DeePC_param, yalmip_optimizer, yalmip_slack_optimizer]...
                    = setup_DeePC_yalmip_sparse_ss(param, exc_param, u_data, y_data,...
                    Tini, svd_weighing, svd_cutoff, lambda1_g, lambda2_g, lambda1_s, lambda2_s,...
                    Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
                    P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw);
            else
                [DeePC_param, yalmip_optimizer, yalmip_slack_optimizer]...
                    = setup_DeePC_yalmip_sparse(param, exc_param, u_data, y_data,...
                    Tini, svd_weighing, svd_cutoff, lambda1_g, lambda2_g, lambda1_s, lambda2_s,...
                    Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
                    P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw);
            end
        else
            [DeePC_param, yalmip_optimizer, yalmip_slack_optimizer]...
                = setup_DeePC_yalmip(param, exc_param, u_data, y_data,...
                Tini, svd_weighing, svd_cutoff, lambda1_g, lambda2_g, lambda1_s, lambda2_s,...
                Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
                P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw);
        end
end
fprintf('DeePC controller ready\n\n');

if param.compare_to_perf
    % Regularization is off in 'perfect' controller
    switch param.optimizer
        case 'gurobi'
            if param.setup_sparse_opt
                [DeePC_param_perf, grb_model_perf, grb_params_perf]...
                    = setup_DeePC_gurobi_sparse(param, exc_param, u_data_perf, y_data_perf,...
                    Tini, false, 0, 0, lambda2_s,...
                    Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
                    P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw);
            else
                [DeePC_param_perf, grb_model_perf, grb_params_perf]...
                    = setup_DeePC_gurobi(param, exc_param, u_data_perf, y_data_perf,...
                    Tini, false, 0, 1e-3, lambda2_s,...
                    Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
                    P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw);
            end
        case 'osqp'
            if param.setup_sparse_opt
                if param.opt_steady_state
                    [DeePC_param_perf, osqp_model_perf]...
                        = setup_DeePC_osqp_sparse_ss(param, exc_param, u_data_perf, y_data_perf,...
                        Tini, false, 0, 0, lambda2_s,...
                        Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
                        P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw);
                else
                    [DeePC_param_perf, osqp_model_perf]...
                        = setup_DeePC_osqp_sparse(param, exc_param, u_data_perf, y_data_perf,...
                        Tini, false, 0, 0, lambda2_s,...
                        Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
                        P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw);
                end
            else
                [DeePC_param_perf, osqp_model_perf]...
                    = setup_DeePC_osqp(param, exc_param, u_data_perf, y_data_perf,...
                    Tini, false, 0, 0, lambda2_s,...
                    Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
                    P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw);
            end
        otherwise   % Use yalmip by default
            if param.setup_sparse_opt
                if param.opt_steady_state
                    [DeePC_param_perf, yalmip_optimizer_perf, yalmip_slack_optimizer_perf]...
                        = setup_DeePC_yalmip_sparse_ss(param, exc_param, u_data_perf, y_data_perf,...
                        Tini, false, 0, 0, 0, lambda1_s, lambda2_s,...
                        Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
                        P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw);
                else
                    [DeePC_param_perf, yalmip_optimizer_perf, yalmip_slack_optimizer_perf]...
                        = setup_DeePC_yalmip_sparse(param, exc_param, u_data_perf, y_data_perf,...
                        Tini, false, 0, 0, 0, lambda1_s, lambda2_s,...
                        Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
                        P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw);
                end
            else
                [DeePC_param_perf, yalmip_optimizer_perf, yalmip_slack_optimizer_perf]...
                    = setup_DeePC_yalmip(param, exc_param, u_data_perf, y_data_perf,...
                    Tini, false, 0, 0, 0, lambda1_s, lambda2_s,...
                    Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
                    P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw);
            end
    end
    fprintf('Linear noise-free DeePC controller ready\n\n');
else
    DeePC_param_perf = [];
    switch param.optimizer
        case 'gurobi'
            grb_model_perf = [];
            grb_params_perf = [];
        case 'osqp'
            osqp_model_perf = [];
        otherwise   % Use yalmip by default
            yalmip_optimizer_perf = [];
            yalmip_slack_optimizer_perf =[];
    end
end

%% Turn noise on/off prior to running
param.measurement_noise_full_state = 1;
param.measurement_noise_body_rates = 1;
param.measurement_noise_body_accelerations = 1;

%% Set initial condition prior to running
param.nrotor_initial_condition(1:3) = [-0.5; -0.5; 0.5];

%% Run DeePC simulation
DeePC_run_time = 10;
Crazyflie_Run_DeePC;

%% Get average solve time
solve_time = mean(solve_time_out);

%% Get input cost
u = eye(param.m, 4) * [total_thrust_cmd(Tini+1:end,:) body_rates_cmd(Tini+1:end,:)].';
input_cost = sum(diag((u - DeePC_param.us).' * (u - DeePC_param.us))).';

%% Get output cost
x = full_state_act(Tini+1:end,1:9).';
y = param.Cd * x;
yref = y(DeePC_param.ref_outputs,:);
ref = r_out(Tini+1:end,:).';
output_cost = sum(diag((yref - ref).' * (yref - ref)).');

%% Get optimization costs
% Input cost
u_f = DeePC_param.U_f * g_out.';
us_mat = repmat(DeePC_param.us, 1, size(u_f, 2));
opt_input_cost = 0;
for i = 1 : exc_param.Nd
    u_curr = u_f((i-1)*param.m+1:i*param.m,:);
    opt_input_cost = opt_input_cost...
        + 1/2 * sum(diag(u_curr.' * DeePC_param.R * u_curr))...
        - sum(diag(us_mat.' * DeePC_param.R * u_curr));
end

% Output cost
y_f = DeePC_param.Y_f * g_out.';
r = zeros(param.p, 1);
for i = 1 : length(DeePC_param.ref_outputs)
    j = DeePC_param.ref_outputs(i);
    r(j) = ref(i,1);
end
r_mat = repmat(r, 1, size(y_f, 2));
opt_output_cost = 0;
for i = 1 : exc_param.Nd
    y_curr = y_f((i-1)*param.p+1:i*param.p,:);
    opt_output_cost = opt_output_cost...
        + 1/2 * sum(diag(y_curr.' * DeePC_param.Q * y_curr))...
        -sum(diag(r_mat.' * DeePC_param.Q * y_curr));
end
y_curr = y_f(end-param.p+1:end,:);
opt_output_cost = opt_output_cost...
    + 1/2 * sum(diag(y_curr.' * DeePC_param.P * y_curr))...
    -sum(diag(r_mat.' * DeePC_param.P * y_curr));

% 1-norm g regularization cost
A_gs = [DeePC_param.U_p; DeePC_param.U_f; DeePC_param.Y_p; DeePC_param.Y_f];
b_gs = [repmat(DeePC_param.us, Tini + exc_param.Nd, 1); repmat(r, Tini + exc_param.Nd + 1, 1)];
gs =  A_gs \ b_gs;
if svd_weighing
    Vt_g = DeePC_param.V.' * g_out.';
    Vt_gs = DeePC_param.V.' * gs;
    curr_opt_cost_lambda1_g = lambda1_g * sum(vecnorm(Vt_g(svd_cutoff+1:end,:) - Vt_gs(svd_cutoff+1:end,:), 1, 1));
else
    curr_opt_cost_lambda1_g = lambda1_g * sum(vecnorm(g_out - gs.', 1, 2));
end

% 2-norm g regularization cost
gs_mat = repmat(gs, 1, size(g_out, 1));
opt_cost_lambda2_g = 1/2 * lambda2_g * sum(diag(g_out * DeePC_param.Wg * g_out.'))...
    - lambda2_g * sum(diag(gs_mat.' * DeePC_param.Wg * g_out.'));

% 1-norm slack regularization cost
opt_cost_lambda1_s = lambda1_s * sum(vecnorm(sigma_out, 1, 2));

% 2-norm slack regularization cost
opt_cost_lambda2_s = 1/2 * lambda2_s * sum(diag(sigma_out * sigma_out.'));

%% Plot Results
fg = plot_DeePC(param, exc_param, DeePC_param, DeePC_param_perf,...
    total_thrust_cmd, body_rates_cmd, full_state_act, full_state_meas,...
    r_out, g_out, sigma_out, us_out, solve_time_out,...
    g_perf_out, solve_time_perf_out,...
    Tini);