function [DeePC_param, grb_model, grb_params]...
    = setup_DeePC_gurobi_sparse(param, exc_param,...
    u_data, y_data,...
    Tini, svd_weighing, svd_cutoff, lambda2_g, lambda2_s,...
    Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
    P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw)

%% DECISION VARIABLES: [g; sigma; u_f; y_f; y_t]
% g: Trajectory mapper [U_p; Y_p; U_f; Y_f] * g = [uini; yini; u_f; y_f; y_t] [R^Ng]
% sigma: Slack variable on yini [R^(p * Tini)]
% u_f: Furture inputs [R^(m * Nd)]
% y_f: Future outputs [R^(p * Nd)]
% y_t: Terminal future output [R^p]

%% Hankel matrices
H_u = data2hankel(u_data, Tini + exc_param.Nd + 1);
H_y = data2hankel(y_data, Tini + exc_param.Nd + 1);
DeePC_param.U_p = H_u(1:param.m*Tini,:);
DeePC_param.U_f = H_u(param.m*Tini+1:end-param.m,:);
DeePC_param.Y_p = H_y(1:param.p*Tini,:);
DeePC_param.Y_f = H_y(param.p*Tini+1:end,:);

%% SVD Weighing
DeePC_param.Ng = size(DeePC_param.U_p, 2);
[~, ~, DeePC_param.V] = svd(H_y);
if svd_weighing
    DeePC_param.Wg = DeePC_param.V * blkdiag(zeros(svd_cutoff), eye(DeePC_param.Ng - svd_cutoff)) * DeePC_param.V.';
else
    DeePC_param.Wg = eye(DeePC_param.Ng);
end

%% Reference outputs
% Reference is x, y, z, yaw
% If DeePC yaw control is disabled, do not track yaw
if param.DeePC_yaw_control
    if param.measure_angles
        DeePC_param.ref_outputs = [1; 2; 3; 6];
    else
        DeePC_param.ref_outputs = [1; 2; 3; 4];
    end
    DeePC_param.ref_states = [1; 2; 3; 9];
else
    DeePC_param.ref_outputs = [1; 2; 3];
    DeePC_param.ref_states = [1; 2; 3];
end
DeePC_param.Nref = length(DeePC_param.ref_outputs);

%% Cost matrices
DeePC_param.Q = zeros(param.p); % Output cost matrix
DeePC_param.Q(1,1) = Q_x;
DeePC_param.Q(2,2) = Q_y;
DeePC_param.Q(3,3) = Q_z;
DeePC_param.R = zeros(param.m); % Input cost matrix
DeePC_param.R(1,1) = R_thrust;
DeePC_param.R(2,2) = R_roll;
DeePC_param.R(3,3) = R_pitch;
DeePC_param.P = zeros(param.p); % Terminal cost matrix
DeePC_param.P(1,1) = P_x;
DeePC_param.P(2,2) = P_y;
DeePC_param.P(3,3) = P_z;
if param.measure_angles
    DeePC_param.P(1,5) = P_x_pitch;
    DeePC_param.P(2,4) = P_y_roll;
    DeePC_param.P(4,2) = P_y_roll;
    DeePC_param.P(4,4) = P_roll;
    DeePC_param.P(5,1) = P_x_pitch;
    DeePC_param.P(5,5) = P_pitch;
end
if param.DeePC_yaw_control
    DeePC_param.Q(end,end) = Q_yaw;
    DeePC_param.R(end,end) = R_yaw;
    DeePC_param.P(end,end) = P_yaw;
end

%% Quadratic cost matrix in style of Gurobi
DeePC_param.NuIni = param.m * Tini;
DeePC_param.NyIni = param.p * Tini;
Nu = param.m * exc_param.Nd;
Ny = param.p * exc_param.Nd;
DeePC_param.Ns = DeePC_param.NyIni;
Q_g = lambda2_g * DeePC_param.Wg;
Q_s = lambda2_s * eye(DeePC_param.Ns);
Q_uf = kron(eye(exc_param.Nd), DeePC_param.R);
Q_yf = kron(eye(exc_param.Nd), DeePC_param.Q);
Q_yt = DeePC_param.P;
Q = blkdiag(Q_g, Q_s, Q_uf, Q_yf, Q_yt);
Q_min_eig = min(eig(Q))

%% Linear cost vector in style of Gurobi
if param.DeePC_yaw_control
    DeePC_param.us = [param.nrotor_vehicle_mass_for_controller * param.g;...
        0; 0; 0];
else
    DeePC_param.us = [param.nrotor_vehicle_mass_for_controller * param.g;...
        0; 0];
end
us_vec = repmat(DeePC_param.us, exc_param.Nd, 1);

DeePC_param.c_uf = -2 * kron(eye(exc_param.Nd), DeePC_param.R) * us_vec;

%% Inequality constraints in style of Gurobi
A = [];
b = [];

lb_g = -inf * ones(DeePC_param.Ng, 1);
lb_s = -inf * ones(DeePC_param.Ns, 1);
lb_uf = repmat(param.input_min, exc_param.Nd, 1);
lb_yf = repmat(param.output_min, exc_param.Nd, 1);
lb_yt = param.output_min;
grb_model.lb = [lb_g; lb_s; lb_uf; lb_yf; lb_yt];

ub_g = inf * ones(DeePC_param.Ng, 1);
ub_s = inf * ones(DeePC_param.Ns, 1);
ub_uf = repmat(param.input_max, exc_param.Nd, 1);
ub_yf = repmat(param.output_max, exc_param.Nd, 1);
ub_yt = param.output_max;
grb_model.ub = [ub_g; ub_s; ub_uf; ub_yf; ub_yt];

%% Equality constraints in style of Gurobi
A_eq = [DeePC_param.U_f zeros(Nu, DeePC_param.Ns) -eye(Nu) zeros(Nu, Ny + param.p)];
A_eq = [A_eq; DeePC_param.Y_f zeros(Ny + param.p, DeePC_param.Ns + Nu) -eye(Ny + param.p)];
A_eq = [A_eq; DeePC_param.U_p zeros(DeePC_param.NuIni, DeePC_param.Ns + Nu + Ny + param.p)];
A_eq = [A_eq; DeePC_param.Y_p -eye(DeePC_param.Ns) zeros(DeePC_param.Ns, Nu + Ny + param.p)];

b_eq = zeros(Nu + Ny + param.p, 1);

DeePC_param.b_grb = [b; b_eq];

%% Gurobi model setup
grb_model.Q = sparse(Q);
grb_model.A = sparse([A; A_eq]);
grb_model.sense = [repmat('<', 1, size(A,1)) repmat('=', 1, size(A_eq,1))];

%% Gurobi parameters
if param.verbose
    grb_params.OutputFlag = 1;
else
    grb_params.OutputFlag = 0;
end
grb_params.Presolve = 0;
%grb_params.Method = 2;
%grb_params.Threads = 0;
%grb_params.Aggregate = 0;
% grb_params.OptimalityTol = 1e-2;
% grb_params.FeasibilityTol = 1e-2;

%% Miscellaneous
DeePC_param.write_model_to_file = false;

end