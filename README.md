DEPENDENCIES:  
Matlab R2019a  
Yalmip R20190425 (latest release: https://yalmip.github.io/download/)  
Gurobi 8.10 (latest release: https://www.gurobi.com/downloads/gurobi-optimizer-eula/)  
OSQP 0.5.0 (latest release: https://osqp.org/docs/get_started/matlab.html)  

MAIN ROUTINE:  
DeePC_main.m  