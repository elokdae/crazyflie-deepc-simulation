clear;
close all;
clc;

%% Load parameters
param = load_crazyflie_parameters();

%% Turn noise on/off prior to data collection
param.measurement_noise_full_state = 0;
param.measurement_noise_body_rates = 0;
param.measurement_noise_body_accelerations = 0;

%% Collect data
% Main tuning parmeters

% Additional hidden states - more than model states
n_hidden = 0;

% Tini
Tini = 6;

% exc_signal_select: 1 = random signal, 2 = PRBS
exc_signal_select = 2;

% rng_seed: Random seed used for random excitation signal generation (not
% used for PRBS)
rng_seed = 0;

% Generate excitation signals
exc_param = generate_excitation_signal(param,...
    exc_signal_select, rng_seed, n_hidden, Tini);

if param.use_real_data
    u_data = csvread('Real Data/m_u_data.csv');
    y_data = csvread('Real Data/m_y_data.csv');
    if ~param.DeePC_yaw_control
        u_data(end,:) = [];
        y_data(end,:) = [];
    end
    if ~param.measure_angles
        y_data(end,:) = [];
        y_data(end,:) = [];
    end
else
    % Data collection mode: 0 => linear model with gravity
    %                       1 => non-linear model
    data_collection_mode = 1;
    [u_data, y_data] = collect_DeePC_data(param, exc_param,...
        data_collection_mode, n_hidden, Tini);
end
fprintf('Data collected\n\n');
    
%% Run data collection on linear model with no noise for comparison
param.measurement_noise_full_state = 0;
param.measurement_noise_body_rates = 0;
param.measurement_noise_body_accelerations = 0;

% Data collection mode: 0 => linear model with gravity
data_collection_mode_perf = 0;
[u_data_perf, y_data_perf] = collect_DeePC_data(param, exc_param,...
data_collection_mode_perf, n_hidden, Tini);

fprintf('Linear noise-free data collected for comparison\n\n');

%% Constant setup tuning parameters

% Quadratic state cost matrix coefficients
Q_x = param.Q(1,1);
Q_y = param.Q(2,2);
Q_z = param.Q(3,3);
Q_yaw = param.Q(end,end);

% Quadratic input cost matrix coefficients
R_thrust = param.R(1,1);
R_roll = param.R(2,2);
R_pitch = param.R(3,3);
R_yaw = param.R(4,4);

% Quadratic state cost matrix coefficients
% P_x = param.P(1,1);
% P_x_pitch = param.P(1,8);
% P_y = param.P(2,2);
% P_y_roll = param.P(2,7);
% P_z = param.P(3,3);
% P_roll = param.P(7,7);
% P_pitch = param.P(8,8);
% P_yaw = param.P(9,9);
P_x = Q_x;
P_x_pitch = 0;
P_y = Q_y;
P_y_roll = 0;
P_z = Q_z;
P_roll = 0;
P_pitch = 0;
P_yaw = Q_yaw;

% SVD weighing flag. When true V.'*g is norm-2 penalized instead of g
svd_weighing = false;

% SVD weighing cutoff. Singular values above and including cutoff are not
% penalized when SVD weighing is enabled
svd_cutoff = 0;

% 1-norm penalty on yini slack
lambda1_s = 0;

% 2-norm penalty on yini slack - we use ini data that we know is feasible,
% and use the 'two_stage_opt' to force slack to 0
lambda2_s = 0;

%% Setup 'perfect' optimization
% Regularization is off in 'perfect' controller
[DeePC_param_perf, yalmip_optimizer_perf, yalmip_slack_optimizer_perf]...
    = setup_DeePC_yalmip_sparse_y0(param, exc_param, u_data_perf, y_data_perf,...
    Tini, false, 0, 0, 0, lambda1_s, lambda2_s,...
    Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
    P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw);

%% Solve 'perfect' optimization
uini = repmat(DeePC_param_perf.us, Tini, 1);
yini = zeros(DeePC_param_perf.NyIni, 1);
r = zeros(param.p, 1);
for i = 1 : DeePC_param_perf.Nref
    j = DeePC_param_perf.ref_outputs(i);
    r(j) = 1;
end
gs_perf = zeros(DeePC_param_perf.Ng, 1);

result_perf = yalmip_optimizer_perf([uini; yini; r; gs_perf]);

g_perf = result_perf(1:DeePC_param_perf.Ng);
uf_perf = reshape(DeePC_param_perf.U_f * g_perf, param.m, []);
yf_perf = reshape(DeePC_param_perf.Y_f * g_perf, param.p, []);

%% Grid over g regularizer
%grid_lin = [1 2 4 6 8];
grid_lin = 1;
%grid_log = [1e-6 1e-5 1e-4 1e-3 1e-2 1e-1 1e0 1e1 1e2 1e3 1e5 1e6];
grid_log = [1e-4 1e-3 1e-2 1e-1 1e0 1e1 1e2 1e3 1e4];

% 1-norm penalty on g
%grid_lambda1_g = [0 kron(grid_log, grid_lin)];
grid_lambda1_g = 0;

% 2-norm penalty on g. If SVD weighing is enabled, 2-norm penalty on V.'*g
% below svd_cutoff
%grid_lambda2_g = [0 kron(grid_log, grid_lin)];
grid_lambda2_g = 1e0;

lambda1_g_start_i = 1;
lambda2_g_start_i = 1;

lambda1_g_loop_start = lambda1_g_start_i;
lambda2_g_loop_start = lambda2_g_start_i;

reset_loop_start = false;

g = cell(length(grid_lambda1_g), length(grid_lambda2_g));
uf = cell(length(grid_lambda1_g), length(grid_lambda2_g));
yf = cell(length(grid_lambda1_g), length(grid_lambda2_g));
opt_cost = cell(length(grid_lambda1_g), length(grid_lambda2_g));
opt_input_cost = cell(length(grid_lambda1_g), length(grid_lambda2_g));
opt_output_cost = cell(length(grid_lambda1_g), length(grid_lambda2_g));
opt_cost_lambda1_g = cell(length(grid_lambda1_g), length(grid_lambda2_g));
opt_cost_lambda2_g = cell(length(grid_lambda1_g), length(grid_lambda2_g));
y_sim = cell(length(grid_lambda1_g), length(grid_lambda2_g));

%% g 1-norm penalty grid
for lambda1_g_i = lambda1_g_loop_start : length(grid_lambda1_g)
lambda1_g = grid_lambda1_g(lambda1_g_i);

if lambda1_g_i ~= lambda1_g_start_i
    reset_loop_start = true;
end

%% g 2-norm penalty grid
if reset_loop_start
    lambda2_g_loop_start = 1;
end
for lambda2_g_i = lambda2_g_loop_start : length(grid_lambda2_g)
lambda2_g = grid_lambda2_g(lambda2_g_i);

%% Tell user where we are
fprintf('Performing iteration with:\n');
fprintf('lambda1_g = %1.1e\n', lambda1_g);
fprintf('lambda2_g = %1.1e\n', lambda2_g);

%% Setup optimization for current regularizers
[DeePC_param, yalmip_optimizer, yalmip_slack_optimizer]...
    = setup_DeePC_yalmip_sparse_y0(param, exc_param, u_data, y_data,...
    Tini, svd_weighing, svd_cutoff, lambda1_g, lambda2_g, lambda1_s, lambda2_s,...
    Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
    P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw);

%% Solve optimization for current regularizers
% Expensive operations to do once
if ~exist('us_vec', 'var')
    us_vec = repmat(DeePC_param.us, exc_param.Nd, 1);
    us_cost = 1/2 * us_vec.' * kron(eye(exc_param.Nd), DeePC_param.R) * us_vec;
    
    r_vec = repmat(r, exc_param.Nd, 1);
    r_cost = 1/2 * r_vec.' * kron(eye(exc_param.Nd), DeePC_param.Q) * r_vec;
    r_t_cost = 1/2 * r.' * DeePC_param.P * r;
    
    A_gs = [DeePC_param.U_p; DeePC_param.U_f; DeePC_param.Y_p; DeePC_param.Y_f];
    b_gs = [repmat(DeePC_param.us, Tini + exc_param.Nd, 1); repmat(r, Tini + exc_param.Nd + 1, 1)];
    gs =  A_gs \ b_gs;
    A_constr = [DeePC_param.U_p; DeePC_param.Y_p; DeePC_param.Y_f(1:param.p,:)];
    b_constr = [uini; yini];
    %gs_constr = gs;
    gs_constr = gs - pinv(A_constr) * (A_constr * gs - b_constr);
    %gs_constr = zeros(DeePC_param.Ng, 1);
    
    fprintf('Done initialization\n\n');
end

result = yalmip_optimizer([uini; yini; r; gs_constr]);

curr_g = result(1:DeePC_param.Ng);

curr_uf = DeePC_param.U_f * curr_g;
curr_yf = DeePC_param.Y_f * curr_g;

%% Get optimization costs
% Input cost
curr_opt_input_cost = 1/2 * curr_uf.' * kron(eye(exc_param.Nd), DeePC_param.R) * curr_uf...
    - us_vec.' * kron(eye(exc_param.Nd), DeePC_param.R) * curr_uf...
    + us_cost;

% Output cost
curr_opt_output_cost = 1/2 * curr_yf(1:end-param.p).' * kron(eye(exc_param.Nd), DeePC_param.Q) * curr_yf(1:end-param.p)...
    - r_vec.' * kron(eye(exc_param.Nd), DeePC_param.Q) * curr_yf(1:end-param.p)...
    + r_cost;
curr_opt_output_cost = curr_opt_output_cost...
    + 1/2 * curr_yf(end-param.p+1:end).' * DeePC_param.P * curr_yf(end-param.p+1:end)...
    - r.' * DeePC_param.P * curr_yf(end-param.p+1:end)...
    + r_t_cost;

% 1-norm g regularization cost
if svd_weighing
    Vt_g = DeePC_param.V.' * curr_g.';
    Vt_gs = DeePC_param.V.' * gs_constr;
    curr_opt_cost_lambda1_g = lambda1_g * norm(Vt_g(svd_cutoff+1:end,:) - Vt_gs(svd_cutoff+1:end,:), 1);
else
    curr_opt_cost_lambda1_g = lambda1_g * norm(curr_g - gs_constr, 1);
end

% 2-norm g regularization cost
gs_cost = 1/2 * lambda2_g * gs_constr.' * DeePC_param.Wg * gs_constr;
curr_opt_cost_lambda2_g = 1/2 * lambda2_g * curr_g.' * DeePC_param.Wg * curr_g...
    - lambda2_g * gs_constr.' * DeePC_param.Wg * curr_g...
    + gs_cost;

% Total cost
curr_opt_cost = result(end) + us_cost + r_cost + r_t_cost + gs_cost;

g{lambda1_g_i,lambda2_g_i} = curr_g;

uf{lambda1_g_i,lambda2_g_i} = reshape(curr_uf, param.m, []);
yf{lambda1_g_i, lambda2_g_i} = reshape(curr_yf, param.p, []);

opt_cost{lambda1_g_i,lambda2_g_i} = curr_opt_cost;
opt_input_cost{lambda1_g_i,lambda2_g_i} = curr_opt_input_cost;
opt_output_cost{lambda1_g_i,lambda2_g_i} = curr_opt_output_cost;
opt_cost_lambda1_g{lambda1_g_i,lambda2_g_i} = curr_opt_cost_lambda1_g;
opt_cost_lambda2_g{lambda1_g_i,lambda2_g_i} = curr_opt_cost_lambda2_g;

%% Forward simulate with model to see if predictions are realizable
curr_y_sim = forward_simulate(param, data_collection_mode,...
    uini, yini(1:end-param.p), uf{lambda1_g_i,lambda2_g_i});

y_sim{lambda1_g_i,lambda2_g_i} = curr_y_sim;

end
end

%% Find closest solution to 'perfect' one with real data
uf_perf_vec = DeePC_param_perf.U_f * g_perf;
yf_perf_vec = DeePC_param_perf.Y_f * g_perf;
trajectory_perf = [uf_perf_vec; yf_perf_vec];

g_perf_real_yalmip = sdpvar(length(g_perf), 1);

constraints = [DeePC_param.U_p; DeePC_param.Y_p; DeePC_param.Y_f(1:param.p,:)] * g_perf_real_yalmip == [uini; yini];
constraints = [constraints;...
    repmat(param.input_min, exc_param.Nd, 1) <= DeePC_param.U_f * g_perf_real_yalmip <= repmat(param.input_max, exc_param.Nd, 1)];
constraints = [constraints;...
    repmat(param.output_min, exc_param.Nd + 1, 1) <= DeePC_param.Y_f * g_perf_real_yalmip <= repmat(param.output_max, exc_param.Nd + 1, 1)];
objective = 1/2 * ([DeePC_param.U_f; DeePC_param.Y_f] * g_perf_real_yalmip - trajectory_perf).'...
    * ([DeePC_param.U_f; DeePC_param.Y_f] * g_perf_real_yalmip - trajectory_perf);

if param.verbose
    options = sdpsettings('solver', param.yalmip_solver, 'verbose', 2);
else
    options = sdpsettings('solver', param.yalmip_solver, 'verbose', 0);
end

options.osqp.eps_abs = 1e-10;
options.osqp.eps_rel = 1e-10;
options.osqp.eps_prim_inf = 1e-10;
options.osqp.eps_dual_inf = 1e-10;

optimize(constraints, objective, options);
g_perf_real = value(g_perf_real_yalmip);

uf_perf_real = reshape(DeePC_param.U_f * g_perf_real, param.m, []);
yf_perf_real = reshape(DeePC_param.Y_f * g_perf_real, param.p, []);

%% Forward simulate closest solution to 'perfect' with model to see if predictions are realizable
y_sim_perf_real = forward_simulate(param, data_collection_mode,...
    uini, yini(1:end-param.p), uf_perf_real);

%% Save workspace when done
save('DeePC_lambda_g_tuner.mat');

%% Plot Results
fg = plot_DeePC_lambda_g_tuner(param, exc_param,...
    DeePC_param, DeePC_param_perf,...
    grid_lambda1_g, grid_lambda2_g, Tini, uini, yini(1:end-param.p), r, uf_perf, yf_perf,...
    g, uf, yf, opt_cost, opt_input_cost, opt_output_cost,...
    opt_cost_lambda1_g, opt_cost_lambda2_g,...
    y_sim,...
    uf_perf_real, yf_perf_real, y_sim_perf_real);