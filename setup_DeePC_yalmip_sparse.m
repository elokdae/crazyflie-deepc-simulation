function [DeePC_param, yalmip_optimizer, yalmip_slack_optimizer]...
    = setup_DeePC_yalmip_sparse(param, exc_param,...
    u_data, y_data,...
    Tini, svd_weighing, svd_cutoff, lambda1_g, lambda2_g, lambda1_s, lambda2_s,...
    Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
    P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw)

%% DECISION VARIABLES: [g; sigma; u_f; y_f; y_t]
% g: Trajectory mapper [U_p; Y_p; U_f; Y_f] * g = [uini; yini; u; y] [R^Ng]
% sigma: Slack variable on yini [R^(p * Tini)]
% u_f: Furture inputs [R^(m * Nd)]
% y_f: Future outputs [R^(p * Nd)]
% y_t: Terminal future output [R^p]

%% Hankel matrices
H_u = data2hankel(u_data, Tini + exc_param.Nd + 1);
H_y = data2hankel(y_data, Tini + exc_param.Nd + 1);
DeePC_param.U_p = H_u(1:param.m*Tini,:);
DeePC_param.U_f = H_u(param.m*Tini+1:end-param.m,:);
DeePC_param.Y_p = H_y(1:param.p*Tini,:);
DeePC_param.Y_f = H_y(param.p*Tini+1:end,:);

%% SVD Weighing
DeePC_param.Ng = size(DeePC_param.U_p, 2);
[~, ~, DeePC_param.V] = svd(H_y);
if svd_weighing
    DeePC_param.Wg = DeePC_param.V * blkdiag(zeros(svd_cutoff), eye(DeePC_param.Ng - svd_cutoff)) * DeePC_param.V.';
else
    DeePC_param.Wg = eye(DeePC_param.Ng);
end

%% YALMIP input variables
% These variables are passed to YALMIP optimizer in every iteration
% They are used to define dynamic objectives/constraints
DeePC_param.NuIni = param.m * Tini;
DeePC_param.NyIni = param.p * Tini;
uini_yalmip = sdpvar(DeePC_param.NuIni, 1);
yini_yalmip = sdpvar(DeePC_param.NyIni, 1);
r_yalmip = sdpvar(param.p, 1);
gs_yalmip = sdpvar(DeePC_param.Ng, 1);

%% Reference outputs
% Reference is x, y, z, yaw
% If DeePC yaw control is disabled, do not track yaw
if param.DeePC_yaw_control
    if param.measure_angles
        DeePC_param.ref_outputs = [1; 2; 3; 6];
    else
        DeePC_param.ref_outputs = [1; 2; 3; 4];
    end
    DeePC_param.ref_states = [1; 2; 3; 9];
else
    DeePC_param.ref_outputs = [1; 2; 3];
    DeePC_param.ref_states = [1; 2; 3];
end
DeePC_param.Nref = length(DeePC_param.ref_outputs);

%% Cost matrices
DeePC_param.Q = zeros(param.p); % Output cost matrix
DeePC_param.Q(1,1) = Q_x;
DeePC_param.Q(2,2) = Q_y;
DeePC_param.Q(3,3) = Q_z;
DeePC_param.R = zeros(param.m); % Input cost matrix
DeePC_param.R(1,1) = R_thrust;
DeePC_param.R(2,2) = R_roll;
DeePC_param.R(3,3) = R_pitch;
DeePC_param.P = zeros(param.p); % Terminal cost matrix
DeePC_param.P(1,1) = P_x;
DeePC_param.P(2,2) = P_y;
DeePC_param.P(3,3) = P_z;
if param.measure_angles
    DeePC_param.P(1,5) = P_x_pitch;
    DeePC_param.P(2,4) = P_y_roll;
    DeePC_param.P(4,2) = P_y_roll;
    DeePC_param.P(4,4) = P_roll;
    DeePC_param.P(5,1) = P_x_pitch;
    DeePC_param.P(5,5) = P_pitch;
end
if param.DeePC_yaw_control
    DeePC_param.Q(end,end) = Q_yaw;
    DeePC_param.R(end,end) = R_yaw;
    DeePC_param.P(end,end) = P_yaw;
end

%% Quadratic cost matrix in style of quadprog
Nu = param.m * exc_param.Nd;
Ny = param.p * exc_param.Nd;
DeePC_param.Ns = DeePC_param.NyIni;
H_g = lambda2_g * DeePC_param.Wg;
H_s = lambda2_s * eye(DeePC_param.Ns);
H_uf = kron(eye(exc_param.Nd), DeePC_param.R);
H_yf = kron(eye(exc_param.Nd), DeePC_param.Q);
H_yt = DeePC_param.P;
H = blkdiag(H_g, H_s, H_uf, H_yf, H_yt);
H_min_eig = min(eig(H))

%% Linear cost vector in style of quadprog
if param.DeePC_yaw_control
    DeePC_param.us = [param.nrotor_vehicle_mass_for_controller * param.g;...
        0; 0; 0];
else
    DeePC_param.us = [param.nrotor_vehicle_mass_for_controller * param.g;...
        0; 0];
    %DeePC_param.us = [0; 0; 0];
end
us_vec = repmat(DeePC_param.us, exc_param.Nd, 1);
r_vec_yalmip = repmat(r_yalmip, exc_param.Nd, 1);

f_g = -lambda2_g * DeePC_param.Wg * gs_yalmip;
f_s = zeros(DeePC_param.Ns, 1);
f_uf = -kron(eye(exc_param.Nd), DeePC_param.R) * us_vec;
f_yf = -kron(eye(exc_param.Nd), DeePC_param.Q) * r_vec_yalmip;
f_yt = -DeePC_param.P * r_yalmip;

f = [f_g; f_s; f_uf; f_yf; f_yt];

%% Inequality constraints in style of quadprog
A = [];
b = [];

lb_g = -inf * ones(DeePC_param.Ng, 1);
lb_s = -inf * ones(DeePC_param.Ns, 1);
lb_uf = repmat(param.input_min, exc_param.Nd, 1);
lb_yf = repmat(param.output_min, exc_param.Nd, 1);
lb_yt = param.output_min;
lb = [lb_g; lb_s; lb_uf; lb_yf; lb_yt];

ub_g = inf * ones(DeePC_param.Ng, 1);
ub_s = inf * ones(DeePC_param.Ns, 1);
ub_uf = repmat(param.input_max, exc_param.Nd, 1);
ub_yf = repmat(param.output_max, exc_param.Nd, 1);
ub_yt = param.output_max;
ub = [ub_g; ub_s; ub_uf; ub_yf; ub_yt];

%% Equality constraints in style of quadprog - main optimization
Aeq = [DeePC_param.U_f zeros(Nu, DeePC_param.Ns) -eye(Nu) zeros(Nu, Ny + param.p)];
Aeq = [Aeq; DeePC_param.Y_f zeros(Ny + param.p, DeePC_param.Ns + Nu) -eye(Ny + param.p)];
Aeq = [Aeq; DeePC_param.U_p zeros(DeePC_param.NuIni, DeePC_param.Ns + Nu + Ny + param.p)];
if param.two_stage_opt
    Aeq = [Aeq; DeePC_param.Y_p zeros(DeePC_param.Ns) zeros(DeePC_param.Ns, Nu + Ny + param.p)];
else
    Aeq = [Aeq; DeePC_param.Y_p -eye(DeePC_param.Ns) zeros(DeePC_param.Ns, Nu + Ny + param.p)];
end

beq = [zeros(Nu + Ny + param.p, 1); uini_yalmip; yini_yalmip];

%% Equality constraints in style of quadprog - slack optimization
% Find minimum slack that satisfies equality constraint first
if param.two_stage_opt
    Aeq_slack = [DeePC_param.U_p zeros(DeePC_param.NuIni, DeePC_param.Ns)];
    Aeq_slack = [Aeq_slack; DeePC_param.Y_p -eye(DeePC_param.Ns)];
    
    beq_slack = [uini_yalmip; yini_yalmip];
end

%% YALMIP setup - main optimization
if param.verbose
    options = sdpsettings('solver', param.yalmip_solver, 'verbose', 2);
else
    options = sdpsettings('solver', param.yalmip_solver, 'verbose', 0);
end

% OSQP tolerances
options.osqp.eps_abs = 1e-6;
options.osqp.eps_rel = 1e-6;
options.osqp.eps_prim_inf = 1e-6;
options.osqp.eps_dual_inf = 1e-6;

% Decision variables
x_yalmip = sdpvar(DeePC_param.Ng + DeePC_param.Ns + Nu + Ny + param.p, 1);

% Constraints
constraints = lb <= x_yalmip;
constraints = [constraints; x_yalmip <= ub];
constraints = [constraints; Aeq * x_yalmip == beq];

% Objective
objective = 1/2 * x_yalmip.' * H * x_yalmip;
objective = objective + f.' * x_yalmip;
if svd_weighing
    Vt_g = DeePC_param.V.' * x_yalmip(1:DeePC_param.Ng);
    Vt_gs = DeePC_param.V.' * gs_yalmip;
    objective = objective + lambda1_g * norm(Vt_g(svd_cutoff+1:end) - Vt_gs(svd_cutoff+1:end), 1);
else
    objective = objective + lambda1_g * norm(x_yalmip(1:DeePC_param.Ng) - gs_yalmip, 1);
end
objective = objective + lambda1_s * norm(x_yalmip(DeePC_param.Ng+1:DeePC_param.Ng+DeePC_param.Ns), 1);

yalmip_optimizer = optimizer(constraints, objective, options,...
    [uini_yalmip; yini_yalmip; r_yalmip; gs_yalmip], [x_yalmip(1:DeePC_param.Ng+DeePC_param.Ns); DeePC_param.us; objective]);

%% YALMIP setup - slack optimization
% Find minimum slack that satisfies equality constraint first
% Everything stays the same except constraints and objective
if param.two_stage_opt
    % Decision variables - sparsity doesn't help as we are not optimizing
    % over future inputs/outputs
    x_yalmip = sdpvar(DeePC_param.Ng + DeePC_param.Ns, 1);
    
    % Constraints - only care about equality constraint
    constraints_slack = Aeq_slack * x_yalmip == beq_slack;

    % Objective - only penalize slack
    objective_slack = 1/2 * x_yalmip(DeePC_param.Ng+1:end).' * x_yalmip(DeePC_param.Ng+1:end);

    yalmip_slack_optimizer = optimizer(constraints_slack, objective_slack, options,...
        [uini_yalmip; yini_yalmip], x_yalmip(DeePC_param.Ng+1:end));
else
    yalmip_slack_optimizer = [];
end
end