function output = DeePC_Optimizer_gurobi(input)

%% This function is used in Simulink as interpreted Matlab function

%% GLOBAL VARIABLES
global start_flag uini yini

%% PERSISTENT VARIABLES
% Parameters: Get from workspace
persistent param exc_param DeePC_param DeePC_param_perf Tini lambda2_g...
    grb_model grb_params grb_model_perf grb_params_perf...
    index

if isempty(param) || start_flag
    param = evalin('base', 'param');
    exc_param = evalin('base', 'exc_param');
    DeePC_param = evalin('base', 'DeePC_param');
    DeePC_param_perf = evalin('base', 'DeePC_param_perf');
    Tini = evalin('base', 'Tini');
    lambda2_g = evalin('base', 'lambda2_g');
    grb_model = evalin('base', 'grb_model');
    grb_params = evalin('base', 'grb_params');
    grb_model_perf = evalin('base', 'grb_model_perf');
    grb_params_perf = evalin('base', 'grb_params_perf');
    
    index = 1;
    
    disp('Initialization Successful');
    start_flag = false;
end

%% INPUT VARIABLES: r_in
% r_in: Reference input [R^Nref]
r_in = input;

r = zeros(param.p, 1);
for i = 1 : DeePC_param.Nref
    j = DeePC_param.ref_outputs(i);
    r(j) = r_in(i);
end
r_vec = repmat(r, exc_param.Nd, 1);

%% Steady state trajectory mapper
A_gs = [DeePC_param.U_p; DeePC_param.U_f; DeePC_param.Y_p; DeePC_param.Y_f];
b_gs = [repmat(DeePC_param.us, Tini + exc_param.Nd, 1); repmat(r, Tini + exc_param.Nd + 1, 1)];
gs =  A_gs \ b_gs;

if param.compare_to_perf
    A_gs_perf = [DeePC_param_perf.U_p; DeePC_param_perf.U_f; DeePC_param_perf.Y_p; DeePC_param_perf.Y_f];
    b_gs_perf = [repmat(DeePC_param_perf.us, Tini + exc_param.Nd, 1); repmat(r, Tini + exc_param.Nd + 1, 1)];
    gs_perf =  A_gs_perf \ b_gs_perf;
end

%% Linear cost update
if param.setup_sparse_opt
    c_g = -2 * lambda2_g * DeePC_param.Wg * gs;
    c_s = zeros(DeePC_param.Ns, 1);
    c_uf = DeePC_param.c_uf;
    c_yf = -2 * kron(eye(exc_param.Nd), DeePC_param.Q) * r_vec;
    c_yt = -2 * DeePC_param.P * r;
    c = [c_g; c_s; c_uf; c_yf; c_yt];
else
    c_g = DeePC_param.c_g...
        - 2 * DeePC_param.Y_f(1:end-param.p,:).' * kron(eye(exc_param.Nd), DeePC_param.Q) * r_vec...
        - 2 * DeePC_param.Y_f(end-param.p+1:end,:).' * DeePC_param.P * r...
        - 2 * lambda2_g * DeePC_param.Wg * gs;
    c_s = zeros(DeePC_param.Ns, 1);
    c = [c_g; c_s];
end
grb_model.obj = c;

if param.compare_to_perf
    if param.setup_sparse_opt
        c_g_perf = -2 * 0 * DeePC_param_perf.Wg * gs_perf;
        c_s_perf = zeros(DeePC_param_perf.Ns, 1);
        c_uf_perf = DeePC_param_perf.c_uf;
        c_yf_perf = -2 * kron(eye(exc_param.Nd), DeePC_param_perf.Q) * r_vec;
        c_yt_perf = -2 * DeePC_param_perf.P * r;
        c_perf = [c_g_perf; c_s_perf; c_uf_perf; c_yf_perf; c_yt_perf];
    else
        c_g_perf = DeePC_param_perf.c_g...
            - 2 * DeePC_param_perf.Y_f(1:end-param.p,:).' * kron(eye(exc_param.Nd), DeePC_param_perf.Q) * r_vec...
            - 2 * DeePC_param_perf.Y_f(end-param.p+1:end,:).' * DeePC_param_perf.P * r...
            - 2 * 0 * DeePC_param_perf.Wg * gs_perf;
        c_s_perf = zeros(DeePC_param_perf.Ns, 1);
        c_perf = [c_g_perf; c_s_perf];
    end
    grb_model_perf.obj = c_perf;
end

%% Equality constraint update
b_eq = [uini; yini];
grb_model.rhs = [DeePC_param.b_grb; b_eq];

if param.compare_to_perf
    grb_model_perf.rhs = [DeePC_param_perf.b_grb; b_eq];
end

%% Solve model
grb_result = gurobi(grb_model, grb_params);

if param.compare_to_perf
    grb_result_perf = gurobi(grb_model_perf, grb_params_perf);
end

%% OUTPUT VARIABLES: [g; sigma; cost; g_perf; sigma_perf; cost_perf; u]:
% g: Trajectory mapper [R^Ng]
% sigma: Slack on yini [R^(p * Tini)]
% cost: Optimization cost [R^1]
% solve_time: Optimization solve time [R^1]
% g_perf: Trajectory mapper using 'perfect' data [R^Ng]
% sigma_perf: Slack on yini using 'perfect' data [R^(p * Tini)]
% cost_perf: Optimization cost using 'perfect' data [R^1]
% solve_time_perf: Optimization solve time using 'perfect' data [R^1]
% u: Input to apply
if grb_result.status == "OPTIMAL"
output = [grb_result.x(1:DeePC_param.Ng+DeePC_param.Ns);...
    grb_result.objval; grb_result.runtime];
else
    output = zeros(DeePC_param.Ng + DeePC_param.Ns + 2, 1);
end

if param.compare_to_perf && grb_result_perf.status == "OPTIMAL"
    output_perf = [grb_result_perf.x(1:DeePC_param_perf.Ng+DeePC_param_perf.Ns);...
        grb_result_perf.objval; grb_result_perf.runtime];
else
    output_perf = zeros(length(output), 1);
end

%% Control input to apply
% U_f(1:m,:)*g gets first input in optimal input sequence
u = DeePC_param.U_f(1:param.m,:) * output(1:DeePC_param.Ng);
% Augment with 0 if DeePC is not controlling yaw
if ~param.DeePC_yaw_control
    u = [u; 0];
end

output = [output; output_perf; u];

%% Write gurobi model to file
if DeePC_param.write_model_to_file
    gurobi_write(grb_model, strcat('Gurobi-Models/grb_model_', int2str(index), '.mps'));
end

index = index + 1;

end