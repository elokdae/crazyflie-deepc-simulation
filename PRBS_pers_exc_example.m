% Generate sufficiently persistently exciting PRBS signal
m = 3;
L = 5;
P = m * L;
T = (m + 1) * L - 1;
PRBS_band = [0, 1];
PRBS_range = [-1, 1];
PRBS_signal = idinput([P, 1, 1], 'prbs', PRBS_band, PRBS_range).';

PRBS_signals = repmat(PRBS_signal, m, 1);

for i = 2 : m
    shift = floor((i-1) / m * P);
    PRBS_signals(i,:) = circshift(PRBS_signal, shift);
end

input = [PRBS_signals, zeros(m, T - m * L)];

H = data2hankel(input, L);

size(H)
rank(H)