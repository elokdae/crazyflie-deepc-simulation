% Initialize uini yini
global start_flag uini yini
start_flag = true;
uini = zeros(DeePC_param.NuIni, 1);
yini = zeros(DeePC_param.NyIni, 1);

start_time = max([2 Tini * param.sample_time_controller_outer]);

sim_time = start_time + DeePC_run_time;

sim('Crazyflie_DeePC_Control');
%sim('Crazyflie_Linear_Model_Gravity_DeePC_Control');

fprintf("Done running DeePC\n\n");