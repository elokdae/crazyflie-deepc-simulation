clear;
close all;
clc;

%% Load parameters
param = load_crazyflie_parameters();

%% Define grid parameters
grid_Td = 331;
grid_Tini = [1 2];
grid_Q_x_y = param.Q(1,1);
grid_Q_z = param.Q(3,3);
grid_Q_yaw = param.Q(end,end);
grid_R_thrust = param.R(1,1);
grid_R_roll_pitch = param.R(2,2);
%grid_R_yaw = [0.25 0.5 0.75 1 1.25 1.75 2 2.25 2.5 2.75 3 3.25 3.5 3.75 4] * param.R(4,4);
grid_R_yaw = param.R(4,4);
% grid_P_x_y = param.P(1,1);
% grid_P_xy_ang = param.P(1,8);
% grid_P_z = param.P(3,3);
% grid_P_roll_pitch = param.P(7,7);
% grid_P_yaw = param.P(9,9);
grid_P_x_y = 0;
grid_P_xy_ang = 0;
grid_P_z = 0;
grid_P_roll_pitch = 0;
grid_P_yaw = 0;
grid_svd_weighing = false;
%grid_lambda1_g = [1e1 2e1 4e1 6e1 8e1 1e2 2e2 4e2 6e2 8e2 1e3];
%grid_lambda1_g = [1e-4 1e-3 1e-2 1e-1 1e0 1e1 1e2 1e3 1e4];
%grid_lambda1_g = [1e-1 1e0 1e1 1e2];
% grid_lambda1_g = [1e0 2e0 4e0 6e0 8e0 1e1 2e1 4e1 6e1 8e1 1e2 2e2 4e2 6e2 8e2 1e3 ...
%     2e3 4e3 6e3 8e3 1e4 2e4 4e4 6e4 8e4 1e5 2e5 4e5 6e5 8e5 1e6 2e6 4e6 6e6 8e6 1e7 ...
%     2e7 4e7 6e7 8e7 1e8 2e8 4e8 6e8 8e8 1e9 2e9 4e9 6e9 8e9 1e10 ...
%     2e10 4e10 6e10 8e10 1e11 2e11 4e11 6e11 8e11 1e12];
grid_lambda1_g = 0e0;
%grid_lambda2_g = [1e0 2e0 4e0 6e0 8e0 1e1 2e1 4e1 6e1 8e1 1e2 2e2 4e2 6e2 8e2 1e3];
%grid_lambda2_g = [2e3 4e3 6e3 8e3 1e4 2e4 4e4 6e4 8e4 1e5 2e5 4e5 6e5 8e5 1e6 ...
%    2e6 4e6 6e6 8e6 1e7 2e7 4e7 6e7 8e7 1e8 2e8 4e8 6e8 8e8 1e9 2e9 4e9 6e9 8e9 1e10 ...
%    2e10 4e10 6e10 8e10 1e11 2e11 4e11 6e11 8e11 1e12];
%grid_lambda2_g = [1e-1 1e0 1e1 1e2 1e3];
grid_lambda2_g = [1e0 2.5e0 5e0 7.5e0 1e1 2.5e1 5e1 7.5e1 1e2 2.5e2 5e2 7.5e2 ...
    1e3 2.5e3 5e3 7.5e3 1e4 2.5e4 5e4 7.5e4 1e5 2.5e5 5e5 7.5e5 1e6 2.5e6 5e6 7.5e6 ...
    1e7 2.5e7 5e7 7.5e7 1e8];
%grid_lambda2_g = 5e2;
%grid_lambda1_s = [1e3 2e3 4e3 6e3 8e3 1e4 2e4 4e4 6e4 8e4 1e5 2e5 4e5 6e5 8e5 1e6];
%grid_lambda1_s = [1e3 1e4 1e5];
grid_lambda1_s = 0e6;
%grid_lambda2_s = [1e5 2e5 4e5 6e5 8e5 1e6 2e6 4e6 6e6 8e6 1e7 2e7 4e7 6e7 8e7 1e8];
% grid_lambda2_s = [1e5 2e5 4e5 6e5 8e5 1e6 2e6 4e6 6e6 8e6 1e7 2e7 4e7 6e7 ...
%     8e7 1e8 2e8 4e8 6e8 8e8 1e9 2e9 4e9 6e9 8e9 1e10 2e10 4e10 6e10 8e10 1e11 ...
%     2e11 4e11 6e11 8e11 1e12];
grid_lambda2_s = [1e5 2.5e5 5e5 7.5e5 1e6 2.5e6 5e6 7.5e6 1e7 2.5e7 5e7 7.5e7 ...
    1e8 2.5e8 5e8 7.5e8 1e9 2.5e9 5e9 7.5e9 1e10];
%grid_lambda2_s = [2e8 4e8 6e8 8e8 1e9 2e9 4e9 6e9 8e9 1e10 2e10 4e10 6e10 8e10 1e11 ...
%   2e11 4e11 6e11 8e11 1e12];
%grid_lambda2_s = [1e5 1e6 1e7 1e8];
% grid_lambda2_s = 7.5e8;
grid_svd_cutoff = 0;

%% Constant parmeters
% exc_signal_select: 1 = random signal, 2 = PRBS
exc_signal_select = 2;

% rng_seed: Random seed used for random excitation signal generation (not
% used for PRBS)
rng_seed = 0;

% Data collection mode: 0 => linear model
%                       1 => non-linear model
data_collection_mode = 1;

% DeePC run time - how long to run for each iteration
DeePC_run_time = 10;

% Simulation max time
sim_max_time = 5;
myTimer = timer('StartDelay', sim_max_time, 'TimerFcn', 'set_param(''Crazyflie_DeePC_Control'',''SimulationCommand'',''stop'')');

% Comparison to 'perfect' controller not used during grid search
DeePC_param_perf = [];
switch param.optimizer
    case 'gurobi'
        grb_model_perf = [];
        grb_params_perf = [];
    case 'osqp'
        osqp_model_perf = [];
    otherwise   % Use yalmip by default
        yalmip_optimizer_perf = [];
        yalmip_slack_optimizer_perf =[];
end

%% Setup grid search output file
if ~isfile('Grid-Search-Results\DeePC_grid_search.csv')
    header = ["Td", "Tini", "Q_x_y", "Q_z", "Q_yaw",...
        "R_thrust", "R_roll_pitch", "R_yaw",...
        "P_x_y", "P_xy_ang", "P_z", "P_roll_pitch", "P_yaw",...
        "svd_weighing", "svd_cutoff",...
        "lambda1_g", "lambda2_g", "lambda1_s", "lambda2_s",...
        "solve_time",...
        "input_cost", "output_cost",...
        "opt_input_cost", "opt_output_cost",...
        "opt_cost_lambda1_g", "opt_cost_lambda2_g",...
        "opt_cost_lambda1_s", "opt_cost_lambda2_s"];
    fout = fopen('Grid-Search-Results\DeePC_grid_search.csv', 'w');
    for i = 1 : length(header) - 1
        fprintf(fout, '%s,', header(i));
    end
    fprintf(fout, '%s\n', header(i+1));
    fclose(fout);
end

%% Grid search
% Start indices allow resuming grid search midway through
Td_start_i = 1;
Tini_start_i = 1;
Q_x_y_start_i = 1;
Q_z_start_i = 1;
Q_yaw_start_i = 1;
R_thrust_start_i = 1;
R_roll_pitch_start_i = 1;
R_yaw_start_i = 1;
P_x_y_start_i = 1;
P_xy_ang_start_i = 1;
P_z_start_i = 1;
P_roll_pitch_start_i = 1;
P_yaw_start_i = 1;
svd_weighing_start_i = 1;
lambda1_g_start_i = 1;
lambda2_g_start_i = 8;
lambda1_s_start_i = 1;
lambda2_s_start_i = 13;
svd_cutoff_start_i = 1;

Td_loop_start = Td_start_i;
Tini_loop_start = Tini_start_i;
Q_x_y_loop_start = Q_x_y_start_i;
Q_z_loop_start = Q_z_start_i;
Q_yaw_loop_start = Q_yaw_start_i;
R_thrust_loop_start = R_thrust_start_i;
R_roll_pitch_loop_start = R_roll_pitch_start_i;
R_yaw_loop_start = R_yaw_start_i;
P_x_y_loop_start = P_x_y_start_i;
P_xy_ang_loop_start = P_xy_ang_start_i;
P_z_loop_start = P_z_start_i;
P_roll_pitch_loop_start = P_roll_pitch_start_i;
P_yaw_loop_start = P_yaw_start_i;
svd_weighing_loop_start = svd_weighing_start_i;
lambda1_g_loop_start = lambda1_g_start_i;
lambda2_g_loop_start = lambda2_g_start_i;
lambda1_s_loop_start = lambda1_s_start_i;
lambda2_s_loop_start = lambda2_s_start_i;
svd_cutoff_loop_start = svd_cutoff_start_i;

reset_loop_start = false;

%% Td grid
for Td_i = Td_loop_start : length(grid_Td)
Td = grid_Td(Td_i);

if Td_i ~= Td_start_i
    reset_loop_start = true;
end

%% Tini grid
if reset_loop_start
    Tini_loop_start = 1;
end
for Tini_i = Tini_loop_start : length(grid_Tini)
Tini = grid_Tini(Tini_i);

% Generate excitation signals
exc_param = generate_excitation_signal(param,...
    exc_signal_select, rng_seed, Td, Tini);

% Turn noise on/off prior to data collection
param.measurement_noise_full_state = 1;
param.measurement_noise_body_rates = 1;
param.measurement_noise_body_accelerations = 1;

% Set initial condition prior to data collection
param.nrotor_initial_condition(1:3) = [0.0; 0.0; 1.0];

% Collect data
if param.use_real_data
    u_data = csvread('Real Data/m_u_data.csv');
    y_data = csvread('Real Data/m_y_data.csv');
    if ~param.DeePC_yaw_control
        u_data(end,:) = [];
        y_data(end,:) = [];
    end
    if ~param.measure_angles
        y_data(end,:) = [];
        y_data(end,:) = [];
    end
else
    [u_data, y_data] = collect_DeePC_data(param, exc_param,...
        data_collection_mode, Tini);
end

% Turn noise on/off prior to running
param.measurement_noise_full_state = 1;
param.measurement_noise_body_rates = 1;
param.measurement_noise_body_accelerations = 1;

% Set initial condition prior to running
param.nrotor_initial_condition(1:3) = [-0.5; -0.5; 0.5];

if Tini_i ~= Tini_start_i
    reset_loop_start = true;
end

%% x/y cost grid
if reset_loop_start
    Q_x_y_loop_start = 1;
end
for Q_x_y_i = Q_x_y_loop_start : length(grid_Q_x_y)
Q_x = grid_Q_x_y(Q_x_y_i);
Q_y = grid_Q_x_y(Q_x_y_i);

if Q_x_y_i ~= Q_x_y_start_i
    reset_loop_start = true;
end

%% z cost grid
if reset_loop_start
    Q_z_loop_start = 1;
end
for Q_z_i = Q_z_loop_start : length(grid_Q_z)
Q_z = grid_Q_z(Q_z_i);

if Q_z_i ~= Q_z_start_i
    reset_loop_start = true;
end

%% yaw cost grid
if reset_loop_start
    Q_yaw_loop_start = 1;
end
for Q_yaw_i = Q_yaw_loop_start : length(grid_Q_yaw)
Q_yaw = grid_Q_yaw(Q_yaw_i);

if Q_yaw_i ~= Q_yaw_start_i
    reset_loop_start = true;
end

%% thrust cost grid
if reset_loop_start
    R_thrust_loop_start = 1;
end
for R_thrust_i = R_thrust_loop_start : length(grid_R_thrust)
R_thrust = grid_R_thrust(R_thrust_i);

if R_thrust_i ~= R_thrust_start_i
    reset_loop_start = true;
end

%% roll/pitch rate cost grid
if reset_loop_start
    R_roll_pitch_loop_start = 1;
end
for R_roll_pitch_i = R_roll_pitch_loop_start : length(grid_R_roll_pitch)
R_roll = grid_R_roll_pitch(R_roll_pitch_i);
R_pitch = grid_R_roll_pitch(R_roll_pitch_i);

if R_roll_pitch_i ~= R_roll_pitch_start_i
    reset_loop_start = true;
end

%% yaw rate cost grid
if reset_loop_start
    R_yaw_loop_start = 1;
end
for R_yaw_i = R_yaw_loop_start : length(grid_R_yaw)
R_yaw = grid_R_yaw(R_yaw_i);

if R_yaw_i ~= R_yaw_start_i
    reset_loop_start = true;
end

%% x/y terminal cost grid
if reset_loop_start
    P_x_y_loop_start = 1;
end
for P_x_y_i = P_x_y_loop_start : length(grid_P_x_y)
P_x = grid_P_x_y(P_x_y_i);
P_y = grid_P_x_y(P_x_y_i);

if P_x_y_i ~= P_x_y_start_i
    reset_loop_start = true;
end

%% x/y-pitch/roll terminal cost grid
if reset_loop_start
    P_xy_ang_loop_start = 1;
end
for P_xy_ang_i = P_xy_ang_loop_start : length(grid_P_xy_ang)
P_x_pitch = grid_P_xy_ang(P_xy_ang_i);
P_y_roll = -grid_P_xy_ang(P_xy_ang_i);

if P_xy_ang_i ~= P_xy_ang_start_i
    reset_loop_start = true;
end

%% z terminal cost grid
if reset_loop_start
    P_z_loop_start = 1;
end
for P_z_i = P_z_loop_start : length(grid_P_z)
P_z = grid_P_z(P_z_i);

if P_z_i ~= P_z_start_i
    reset_loop_start = true;
end

%% roll/pitch terminal cost grid
if reset_loop_start
    P_roll_pitch_loop_start = 1;
end
for P_roll_pitch_i = P_roll_pitch_loop_start : length(grid_P_roll_pitch)
P_roll = grid_P_roll_pitch(P_roll_pitch_i);
P_pitch = grid_P_roll_pitch(P_roll_pitch_i);

if P_roll_pitch_i ~= P_roll_pitch_start_i
    reset_loop_start = true;
end

%% yaw terminal cost grid
if reset_loop_start
    P_yaw_loop_start = 1;
end
for P_yaw_i = P_yaw_loop_start : length(grid_P_yaw)
P_yaw = grid_P_yaw(P_yaw_i);

if P_yaw_i ~= P_yaw_start_i
    reset_loop_start = true;
end

%% SVD weighing grid
if reset_loop_start
    svd_weighing_loop_start = 1;
end
for svd_weighing_i = svd_weighing_loop_start : length(grid_svd_weighing)
svd_weighing = grid_svd_weighing(svd_weighing_i);

if svd_weighing_i ~= svd_weighing_start_i
    reset_loop_start = true;
end

%% g 1-norm penalty grid
if reset_loop_start
    lambda1_g_loop_start = 1;
end
for lambda1_g_i = lambda1_g_loop_start : length(grid_lambda1_g)
lambda1_g = grid_lambda1_g(lambda1_g_i);

if lambda1_g_i ~= lambda1_g_start_i
    reset_loop_start = true;
end

%% g 2-norm penalty grid
if reset_loop_start
    lambda2_g_loop_start = 1;
end
for lambda2_g_i = lambda2_g_loop_start : length(grid_lambda2_g)
lambda2_g = grid_lambda2_g(lambda2_g_i);

if lambda2_g_i ~= lambda2_g_start_i
    reset_loop_start = true;
end

%% slack 1-norm penalty grid
if reset_loop_start
    lambda1_s_loop_start = 1;
end
for lambda1_s_i = lambda1_s_loop_start : length(grid_lambda1_s)
lambda1_s = grid_lambda1_s(lambda1_s_i);

if lambda1_s_i ~= lambda1_s_start_i
    reset_loop_start = true;
end

%% slack 2-norm penalty grid
if reset_loop_start
    lambda2_s_loop_start = 1;
end
for lambda2_s_i = lambda2_s_loop_start : length(grid_lambda2_s)
lambda2_s = grid_lambda2_s(lambda2_s_i);

if lambda2_s_i ~= lambda2_s_start_i
    reset_loop_start = true;
end

%% svd cutoff grid
if reset_loop_start
    svd_cutoff_loop_start = 1;
end
if svd_weighing
    svd_cutoff_loop_end = length(grid_svd_cutoff);
else
    svd_cutoff_loop_end = svd_cutoff_loop_start;
end
for svd_cutoff_i = svd_cutoff_loop_start : svd_cutoff_loop_end
svd_cutoff = grid_svd_cutoff(svd_cutoff_i);

% Tell user where we are
fprintf('Performing iteration with:\n');
fprintf('Td = %d\n', exc_param.T);
fprintf('Tini = %d\n', Tini);
fprintf('Q_x = Q_y = %.1f\n', Q_x);
fprintf('Q_z = %.1f\n', Q_z);
fprintf('R_thrust = %.1f\n', R_thrust);
fprintf('R_roll = R_pitch = %.1f\n', R_roll);
fprintf('R_yaw = %.1f\n', R_yaw);
fprintf('P_x = P_y = %.1f\n', P_x);
fprintf('P_x_pitch = -P_y_roll = %.1f\n', P_x_pitch);
fprintf('P_z = %.1f\n', P_z);
fprintf('P_roll = P_pitch = %.1f\n', P_roll);
fprintf('P_yaw = %.1f\n', P_yaw);
fprintf('svd_weighing = %d\n', svd_weighing);
fprintf('svd_cutoff = %d\n', svd_cutoff);
fprintf('lambda1_g = %1.1e\n', lambda1_g);
fprintf('lambda2_g = %1.1e\n', lambda2_g);
fprintf('lambda1_s = %1.1e\n', lambda1_s);
fprintf('lambda2_s = %1.1e\n\n', lambda2_s);

% Setup DeePC optimization
switch param.optimizer
    case 'gurobi'
        if param.setup_sparse_opt
            [DeePC_param, grb_model, grb_params]...
                = setup_DeePC_gurobi_sparse(param, exc_param, u_data, y_data,...
                Tini, svd_weighing, svd_cutoff, lambda2_g, lambda2_s,...
                Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
                P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw);
        else
            [DeePC_param, grb_model, grb_params]...
                = setup_DeePC_gurobi(param, exc_param, u_data, y_data,...
                Tini, svd_weighing, svd_cutoff, lambda2_g, lambda2_s,...
                Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
                P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw);
        end
        us_out = 0;
    case 'osqp'
        if param.setup_sparse_opt
            if param.opt_steady_state
                [DeePC_param, osqp_model]...
                    = setup_DeePC_osqp_sparse_ss(param, exc_param, u_data, y_data,...
                    Tini, svd_weighing, svd_cutoff, lambda2_g, lambda2_s,...
                    Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
                    P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw);
            else
                [DeePC_param, osqp_model]...
                    = setup_DeePC_osqp_sparse(param, exc_param, u_data, y_data,...
                    Tini, svd_weighing, svd_cutoff, lambda2_g, lambda2_s,...
                    Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
                    P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw);
            end
        else
            [DeePC_param, osqp_model]...
                = setup_DeePC_osqp(param, exc_param, u_data, y_data,...
                Tini, svd_weighing, svd_cutoff, lambda2_g, lambda2_s,...
                Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
                P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw);
        end
        us_out = 0;
    otherwise   % Use yalmip by default
        if param.setup_sparse_opt
            if param.opt_steady_state
                [DeePC_param, yalmip_optimizer, yalmip_slack_optimizer]...
                    = setup_DeePC_yalmip_sparse_ss(param, exc_param, u_data, y_data,...
                    Tini, svd_weighing, svd_cutoff, lambda1_g, lambda2_g, lambda1_s, lambda2_s,...
                    Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
                    P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw);
            else
                [DeePC_param, yalmip_optimizer, yalmip_slack_optimizer]...
                    = setup_DeePC_yalmip_sparse(param, exc_param, u_data, y_data,...
                    Tini, svd_weighing, svd_cutoff, lambda1_g, lambda2_g, lambda1_s, lambda2_s,...
                    Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
                    P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw);
            end
        else
            [DeePC_param, yalmip_optimizer, yalmip_slack_optimizer]...
                = setup_DeePC_yalmip(param, exc_param, u_data, y_data,...
                Tini, svd_weighing, svd_cutoff, lambda1_g, lambda2_g, lambda1_s, lambda2_s,...
                Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
                P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw);
        end
end
fprintf('DeePC controller ready\n\n');

% Run DeePC simulation
start(myTimer);
Crazyflie_Run_DeePC;
stop(myTimer);

% Get average solve time
solve_time = mean(solve_time_out);

% Get input cost
u = eye(param.m, 4) * [total_thrust_cmd(Tini+1:end,:) body_rates_cmd(Tini+1:end,:)].';
input_cost = sum(diag((u - DeePC_param.us).' * (u - DeePC_param.us))).';

% Get output cost
x = full_state_act(Tini+1:end,1:9).';
y = param.Cd * x;
yref = y(DeePC_param.ref_outputs,:);
ref = r_out(Tini+1:end,:).';
output_cost = sum(diag((yref - ref).' * (yref - ref)).');

% Get optimization costs
% Input cost
u_f = DeePC_param.U_f * g_out.';
us_mat = repmat(DeePC_param.us, 1, size(u_f, 2));
opt_input_cost = 0;
for i = 1 : exc_param.Nd
    u_curr = u_f((i-1)*param.m+1:i*param.m,:);
    opt_input_cost = opt_input_cost...
        + 1/2 * sum(diag(u_curr.' * DeePC_param.R * u_curr))...
        - sum(diag(us_mat.' * DeePC_param.R * u_curr));
end

% Output cost
y_f = DeePC_param.Y_f * g_out.';
r = zeros(param.p, 1);
for i = 1 : length(DeePC_param.ref_outputs)
    j = DeePC_param.ref_outputs(i);
    r(j) = ref(i,1);
end
r_mat = repmat(r, 1, size(y_f, 2));
opt_output_cost = 0;
for i = 1 : exc_param.Nd
    y_curr = y_f((i-1)*param.p+1:i*param.p,:);
    opt_output_cost = opt_output_cost...
        + 1/2 * sum(diag(y_curr.' * DeePC_param.Q * y_curr))...
        -sum(diag(r_mat.' * DeePC_param.Q * y_curr));
end
y_curr = y_f(end-param.p+1:end,:);
opt_output_cost = opt_output_cost...
    + 1/2 * sum(diag(y_curr.' * DeePC_param.P * y_curr))...
    -sum(diag(r_mat.' * DeePC_param.P * y_curr));

% 1-norm g regularization cost
A_gs = [DeePC_param.U_p; DeePC_param.U_f; DeePC_param.Y_p; DeePC_param.Y_f];
b_gs = [repmat(DeePC_param.us, Tini + exc_param.Nd, 1); repmat(r, Tini + exc_param.Nd + 1, 1)];
gs =  A_gs \ b_gs;
if svd_weighing
    Vt_g = DeePC_param.V.' * g_out.';
    Vt_gs = DeePC_param.V.' * gs;
    opt_cost_lambda1_g = lambda1_g * sum(vecnorm(Vt_g(svd_cutoff+1:end,:) - Vt_gs(svd_cutoff+1:end,:), 1, 1));
else
    opt_cost_lambda1_g = lambda1_g * sum(vecnorm(g_out - gs.', 1, 2));
end

% 2-norm g regularization cost
gs_mat = repmat(gs, 1, size(g_out, 1));
opt_cost_lambda2_g = 1/2 * lambda2_g * sum(diag(g_out * DeePC_param.Wg * g_out.'))...
    - lambda2_g * sum(diag(gs_mat.' * DeePC_param.Wg * g_out.'));

% 1-norm slack regularization cost
opt_cost_lambda1_s = lambda1_s * sum(vecnorm(sigma_out, 1, 2));

% 2-norm slack regularization cost
opt_cost_lambda2_s = 1/2 * lambda2_s * sum(diag(sigma_out * sigma_out.'));

% Save workspace at the end of each iteration
save('Grid-Search-Results\DeePC_grid_search.mat');

% Append grid search results file
cost = [input_cost, output_cost,...
    opt_input_cost, opt_output_cost,...
    opt_cost_lambda1_g, opt_cost_lambda2_g,...
    opt_cost_lambda1_s, opt_cost_lambda2_s];
line = [exc_param.T, Tini, Q_x, Q_z, Q_yaw,...
    R_thrust, R_roll, R_yaw,...
    P_x, P_x_pitch, P_z, P_roll, P_yaw,...
    svd_weighing, svd_cutoff,...
    lambda1_g, lambda2_g, lambda1_s,lambda2_s,...
    solve_time,...
    cost];
dlmwrite('Grid-Search-Results\DeePC_grid_search.csv', line, '-append');
end
end
end
end
end
end
end
end
end
end
end
end
end
end
end
end
end
end
end

delete(myTimer);

%% Plot Results of last iteration
fg = plot_DeePC(param, exc_param, DeePC_param, DeePC_param_perf,...
    total_thrust_cmd, body_rates_cmd, full_state_act, full_state_meas,...
    r_out, g_out, sigma_out, us_out, solve_time_out,...
    g_perf_out, solve_time_perf_out,...
    Tini);