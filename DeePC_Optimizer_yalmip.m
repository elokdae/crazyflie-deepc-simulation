function output = DeePC_Optimizer_yalmip(input)

%% This function is used in Simulink as interpreted Matlab function

%% GLOBAL VARIABLES
global start_flag uini yini

%% PERSISTENT VARIABLES
% Parameters: Get from workspace
persistent param exc_param DeePC_param DeePC_param_perf Tini...
    yalmip_optimizer yalmip_slack_optimizer...
    yalmip_optimizer_perf yalmip_slack_optimizer_perf

if isempty(param) || start_flag
    param = evalin('base', 'param');
    exc_param = evalin('base', 'exc_param');
    DeePC_param = evalin('base', 'DeePC_param');
    DeePC_param_perf = evalin('base', 'DeePC_param_perf');
    Tini = evalin('base', 'Tini');
    yalmip_optimizer = evalin('base', 'yalmip_optimizer');
    yalmip_slack_optimizer = evalin('base', 'yalmip_slack_optimizer');
    yalmip_optimizer_perf = evalin('base', 'yalmip_optimizer_perf');
    yalmip_slack_optimizer_perf = evalin('base', 'yalmip_slack_optimizer_perf');
    
    disp('Initialization Successful');
    start_flag = false;
end

%% INPUT VARIABLES: r_in
% r_in: Reference input [R^Nref]
r_in = input;

r = zeros(param.p, 1);
for i = 1 : DeePC_param.Nref
    j = DeePC_param.ref_outputs(i);
    r(j) = r_in(i);
end

%% Steady state trajectory mapper
% Only perform if we are not optimizing over steady state gs already
if ~param.opt_steady_state
    A_gs = [DeePC_param.U_p; DeePC_param.U_f; DeePC_param.Y_p; DeePC_param.Y_f];
    b_gs = [repmat(DeePC_param.us, Tini + exc_param.Nd, 1); repmat(r, Tini + exc_param.Nd + 1, 1)];
    gs =  A_gs \ b_gs;
    %gs =  zeros(DeePC_param.Ng, 1);

    if param.compare_to_perf
        A_gs_perf = [DeePC_param_perf.U_p; DeePC_param_perf.U_f; DeePC_param_perf.Y_p; DeePC_param_perf.Y_f];
        b_gs_perf = [repmat(DeePC_param_perf.us, Tini + exc_param.Nd, 1); repmat(r, Tini + exc_param.Nd + 1, 1)];
        gs_perf =  A_gs_perf \ b_gs_perf;
    end
else
    gs = zeros(DeePC_param.Ng, 1);
    
    if param.compare_to_perf
        gs_perf = zeros(DeePC_param.Ng, 1);
    end
end

%% OUTPUT VARIABLES: [g; sigma; cost; us; solve_time; g_perf; sigma_perf; cost_perf; us_perf; u]:
% g: Trajectory mapper [R^Ng] - 1st output of yalmip optimizer
% sigma: Slack on yini [R^(p * Tini)] - 2nd output of yalmip optimizer
% us: Steady state input [R^m] - 3rd output of yalmip optimizer
% cost: Optimization cost [R^1] - 4th output of yalmip optimizer
% solve_time: Optimization solve time [R^1]
% g_perf: Trajectory mapper using 'perfect' data [R^Ng] - 1st output of yalmip optimizer
% sigma_perf: Slack on yini using 'perfect' data [R^(p * Tini)] - 2nd output of yalmip optimizer
% us_perf: Steady state input using 'perfect' data [R^m] - 3rd output of yalmip optimizer
% cost_perf: Optimization cost using 'perfect' data [R^1] - 4th output of yalmip optimizer
% solve_time_perf: Optimization solve time using 'perfect' data [R^1]
% u: Input to apply
if param.two_stage_opt
    [sigma, ~, ~, ~, ~, diagn_slack] = yalmip_slack_optimizer([uini; yini]);
    yini_slack = yini + sigma;
    [output, ~, ~, ~, ~, diagn] = yalmip_optimizer([uini; yini_slack; r; gs]);
    output(DeePC_param.Ng+1:DeePC_param.Ng+DeePC_param.Ns) = sigma;
    solve_time = diagn_slack.solvertime + diagn.solvertime;
else
    [output, ~, ~, ~, ~, diagn] = yalmip_optimizer([uini; yini; r; gs]);
    solve_time = diagn.solvertime;
end

if param.compare_to_perf
    if param.two_stage_opt
        [sigma, ~, ~, ~, ~, diagn_slack] = yalmip_slack_optimizer_perf([uini; yini]);
        yini_slack = yini + sigma;
        [output_perf, ~, ~, ~, ~, diagn] = yalmip_optimizer_perf([uini; yini_slack; r; gs_perf]);
        output_perf(DeePC_param_perf.Ng+1:DeePC_param.Ng+DeePC_param.Ns) = sigma;
        solve_time_perf = diagn_slack.solvertime + diagn.solvertime;
    else
        [output_perf, ~, ~, ~, ~, diagn] = yalmip_optimizer_perf([uini; yini; r; gs_perf]);
        solve_time_perf = diagn.solvertime;
    end
else
    output_perf = zeros(size(output));
    solve_time_perf = 0;
end

%% Control input to apply
% U_f(1:m,:)*g gets first input in optimal input sequence
u = DeePC_param.U_f(1:param.m,:) * output(1:DeePC_param.Ng);
% Augment with 0 if DeePC is not controlling yaw
if ~param.DeePC_yaw_control
    u = [u; 0];
end

output = [output; solve_time; output_perf; solve_time_perf; u];
output(isnan(output))=0;
output(isinf(output))=0;

end