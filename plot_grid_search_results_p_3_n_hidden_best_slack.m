close all;

%% Screen size used to place plots
screensize = get(groot, 'ScreenSize');
screenwidth = screensize(3);
screenheight = screensize(4);
default_width = screenwidth / 2;
default_height = screenheight / 2;
fg = 1;

%% Manual entry of data
grid_n_hidden = 0 : 1 : 50;
grid_cost = [119610 232940 1419.4 348190 393.95 74784 ...
    71833 1195.7 45611 692.56 1183.1 ...
    27031 158.53 210.37 43.12 46.299 ...
    42.18 40.918 40.998 38.865 38.618 ...
    45.44 42.018 50.361 40.351 52.566 ...
    36.954 39.809 45.516 48.695 40.34 ...
    44.328 45.204 39.882 54.764 34.937 ...
    40.174 41.108 46.086 38.543 38.925 ...
    37.703 38.253 44.625 40.949 38.252 ...
    37.667 37.647 38.592 44.95 45.645];

n_hidden_good = 14;
cost_good = grid_cost(grid_n_hidden == n_hidden_good);

%% Plot
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [0, 0, default_width, default_height];
stem(grid_n_hidden, grid_cost, 'Linewidth', 2);
hold on;
stem(n_hidden_good, cost_good, 'Linewidth', 2);
axes = gca;
axis(axes, 'tight');
xl = xlim(axes);
xl(1) = xl(1) - 1;
xl(2) = xl(2) + 1;
xlim(axes, xl);
yl = ylim(axes);
%yl(1) = yl(2) * 0.99;
%yl(2) = yl(2) * 1.1;
yl(2) = 100;
ylim(axes, yl);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Number of Hidden States vs. Best Sum of Square Tracking Error';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$n_h$';
axes.XLabel.FontSize = 14;
axes.YLabel.Interpreter = 'latex';
axes.YLabel.String = '$\sum \|y_r-r\|_2^2$';
axes.YLabel.FontSize = 14;

%% Helper function
function y_semi_tight(ax, scale)
    axis(ax, 'tight'); % Set axis tight
    yl = ylim(ax); % Get tight axis limits
    range = yl(2) - yl(1); % Get tight axis range
    sc_range = scale * range; % Scale range
    yl(1) = yl(1) - (sc_range - range) / 2; % New ymin
    yl(2) = yl(1) + sc_range; % New ymax
    ylim(ax, yl);
end