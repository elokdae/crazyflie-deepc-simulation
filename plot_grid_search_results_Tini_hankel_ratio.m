close all;

%% Screen size used to place plots
screensize = get(groot, 'ScreenSize');
screenwidth = screensize(3);
screenheight = screensize(4);
default_width = screenwidth / 2;
default_height = screenheight / 2;
fg = 1;

%% Manual entry of data
grid_Tini = 3 : 12;
%grid_ratio = {'0.5371'; '0.5316'; '0.5265'; '0.5217'; '0.5172'; '0.8253'; '0.7978'; '0.7719'; '0.7474'; '0.7243'};
grid_ratio = [0.537 0.532 0.527 0.522 0.517 0.825 0.798 0.772 0.747 0.724];
grid_cost = [90.77 46.444 54.007 55.854 111.89 38.534 41.01 41.002 39.761 40.84];

grid_label = cell(size(grid_Tini));
for i = 1 : length(grid_Tini)
    grid_label{i} = strcat(num2str(grid_Tini(i)), ' $[', num2str(grid_ratio(i)), ']$');
end

Tini_good = 8;
i = find(grid_Tini == Tini_good);
cost_good = grid_cost(i);

%% Plot
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [0, 0, default_width, default_height];
stem(grid_Tini, grid_cost, 'Linewidth', 2);
hold on;
stem(Tini_good, cost_good, 'Linewidth', 2);
axes = gca;
axis(axes, 'tight');
xl = xlim(axes);
xl(1) = xl(1) - 1;
xl(2) = xl(2) + 1;
xlim(axes, xl);
yl = ylim(axes);
%yl(1) = yl(2) * 0.99;
yl(2) = yl(2) * 1.1;
%yl(2) = 100;
ylim(axes, yl);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Hankel Matrix Shape vs. Best Sum of Square Tracking Error';
axes.Title.FontSize = 18;
axes.XAxis.TickLabels(2:end-1) = grid_label;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.XAxis.TickLabelRotation = 45;
axes.XAxis.TickLabelFormat
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$T_{ini}$ $[cols/rows]$';
axes.XLabel.FontSize = 14;
axes.YLabel.Interpreter = 'latex';
axes.YLabel.String = '$\sum \|y_r-r\|_2^2$';
axes.YLabel.FontSize = 14;

%% Helper function
function y_semi_tight(ax, scale)
    axis(ax, 'tight'); % Set axis tight
    yl = ylim(ax); % Get tight axis limits
    range = yl(2) - yl(1); % Get tight axis range
    sc_range = scale * range; % Scale range
    yl(1) = yl(1) - (sc_range - range) / 2; % New ymin
    yl(2) = yl(1) + sc_range; % New ymax
    ylim(ax, yl);
end