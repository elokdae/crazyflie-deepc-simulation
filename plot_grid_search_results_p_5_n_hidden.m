close all;

%% Screen size used to place plots
screensize = get(groot, 'ScreenSize');
screenwidth = screensize(3);
screenheight = screensize(4);
default_width = screenwidth / 2;
default_height = screenheight / 2;
fg = 1;

%% Manual entry of data
grid_n_hidden = 0 : 1 : 50;
grid_cost = [77.217 143.47 166.27 107.35 173.53 101.99 ...
    238.17 186.12 141.07 2816.1 122.1 ...
    150.32 72.913 337.53 179.17 87.496 ...
    61.896 52.598 40.726 36.381 45.392 ...
    42.701 42.712 44.697 44.715 42.297 ...
    36.587 44.914 37.288 37.085 37.422 ...
    41.198 37.132 36.205 28.96 50.299 ...
    36.706 35.238 35.087 36.613 36.198 ...
    37.769 36.26 38.424 38.497 39.196 ...
    37.515 35.802 36.312 37.388 36.459];

%% Plot
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [0, 0, default_width, default_height];
stem(grid_n_hidden, grid_cost, 'Linewidth', 2);
axes = gca;
axis(axes, 'tight');
xl = xlim(axes);
xl(1) = xl(1) - 1;
xl(2) = xl(2) + 1;
xlim(axes, xl);
yl = ylim(axes);
%yl(1) = yl(2) * 0.99;
%yl(2) = yl(2) * 1.1;
yl(2) = 100;
ylim(axes, yl);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Number of Hidden States vs. Best Sum of Square Tracking Error';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$n_h$';
axes.XLabel.FontSize = 14;
axes.YLabel.Interpreter = 'latex';
axes.YLabel.String = '$\sum \|y_r-r\|_2^2$';
axes.YLabel.FontSize = 14;

%% Helper function
function y_semi_tight(ax, scale)
    axis(ax, 'tight'); % Set axis tight
    yl = ylim(ax); % Get tight axis limits
    range = yl(2) - yl(1); % Get tight axis range
    sc_range = scale * range; % Scale range
    yl(1) = yl(1) - (sc_range - range) / 2; % New ymin
    yl(2) = yl(1) + sc_range; % New ymax
    ylim(ax, yl);
end