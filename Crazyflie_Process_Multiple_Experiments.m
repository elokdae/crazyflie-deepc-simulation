clear;
close all;
clc;

% Screen size used to place plots
screensize = get(groot, 'ScreenSize');
screenwidth = screensize(3);
screenheight = screensize(4);
fg = 1;

%% Get data from all experiments
num_exp = 10;
u_real_data = cell(num_exp, 1);
y_real_data = cell(num_exp, 1);
for i = 1 : num_exp
    u_file_name = strcat('Real Data\PRBS P = 255, T = 10, w = 60\m_u_data_', num2str(i), '.csv');
    y_file_name = strcat('Real Data\PRBS P = 255, T = 10, w = 60\m_y_data_', num2str(i), '.csv');
    u_real_data{i} = csvread(u_file_name).';
    y_real_data{i} = csvread(y_file_name).';
end

%% Find average data from experiments
exp_to_average = 1 : num_exp;
exp_to_average(2) = [];
num_exp_to_average = length(exp_to_average);

exp_i = exp_to_average(1);
u_real_data_mean = u_real_data{exp_i} / num_exp_to_average;
y_real_data_mean = y_real_data{exp_i} / num_exp_to_average;
for exp_i = 2 : num_exp_to_average
    u_real_data_mean = u_real_data_mean + u_real_data{exp_i} / num_exp_to_average;
    y_real_data_mean = y_real_data_mean + y_real_data{exp_i} / num_exp_to_average;
end

% Wrtie mean to .csv file in same format as Crazyflie files to be used by
% other code
dlmwrite('Real Data\m_u_data_mean.csv', u_real_data_mean.');
dlmwrite('Real Data\m_y_data_mean.csv', y_real_data_mean.');

%% Input Plot
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [0, 0, screenwidth, screenheight];
% thrust sub-plot
subplot(2,2,1);
plot(u_real_data{1}(1,:));
hold on;
for i = 2 : num_exp
    plot(u_real_data{i}(1,:));
end
plot(u_real_data_mean(1,:), 'linewidth', 2);
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Real total thrust from $10$ trials, with mean';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$t$ $[timesteps]$';
axes.XLabel.FontSize = 14;
axes.YLabel.Interpreter = 'latex';
axes.YLabel.String = '$F$ $[N]$';
axes.YLabel.FontSize = 14;
% roll rate sub-plot
subplot(2,2,2);
plot(u_real_data{1}(2,:));
hold on;
for i = 2 : num_exp
    plot(u_real_data{i}(2,:));
end
plot(u_real_data_mean(2,:), 'linewidth', 2);
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Real $x$ body angular rate from $10$ trials, with mean';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$t$ $[timesteps]$';
axes.XLabel.FontSize = 14;
axes.YLabel.Interpreter = 'latex';
axes.YLabel.String = '$\omega^{(B)}_x$ $[\frac{rad}{s}]$';
axes.YLabel.FontSize = 14;
% pitch rate sub-plot
subplot(2,2,3);
plot(u_real_data{1}(3,:));
hold on;
for i = 2 : num_exp
    plot(u_real_data{i}(3,:));
end
plot(u_real_data_mean(3,:), 'linewidth', 2);
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Real $y$ body angular rate from $10$ trials, with mean';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$t$ $[timesteps]$';
axes.XLabel.FontSize = 14;
axes.YLabel.Interpreter = 'latex';
axes.YLabel.String = '$\omega^{(B)}_y$ $[\frac{rad}{s}]$';
axes.YLabel.FontSize = 14;
% yaw rate sub-plot
subplot(2,2,4);
plot(u_real_data{1}(4,:));
hold on;
for i = 2 : num_exp
    plot(u_real_data{i}(4,:));
end
plot(u_real_data_mean(4,:), 'linewidth', 2);
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Real $z$ body angular rate from $10$ trials, with mean';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$t$ $[timesteps]$';
axes.XLabel.FontSize = 14;
axes.YLabel.Interpreter = 'latex';
axes.YLabel.String = '$\omega^{(B)}_z$ $[\frac{rad}{s}]$';
axes.YLabel.FontSize = 14;

%% Output Plot
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [0, 0, screenwidth, screenheight];
% x sub-plot
subplot(2,3,1);
plot(y_real_data{1}(1,:));
hold on;
for i = 2 : num_exp
    plot(y_real_data{i}(1,:));
end
plot(y_real_data_mean(1,:), 'linewidth', 2);
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Inertial $x$ position from $10$ trials, with mean';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$t$ $[timesteps]$';
axes.XLabel.FontSize = 14;
axes.YLabel.Interpreter = 'latex';
axes.YLabel.String = '$p^{(I)}_x$ $[m]$';
axes.YLabel.FontSize = 14;
% y sub-plot
subplot(2,3,2);
plot(y_real_data{1}(2,:));
hold on;
for i = 2 : num_exp
    plot(y_real_data{i}(2,:));
end
plot(y_real_data_mean(2,:), 'linewidth', 2);
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Inertial $y$ position from $10$ trials, with mean';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$t$ $[timesteps]$';
axes.XLabel.FontSize = 14;
axes.YLabel.Interpreter = 'latex';
axes.YLabel.String = '$p^{(I)}_y$ $[m]$';
axes.YLabel.FontSize = 14;
% z sub-plot
subplot(2,3,3);
plot(y_real_data{1}(3,:));
hold on;
for i = 2 : num_exp
    plot(y_real_data{i}(3,:));
end
plot(y_real_data_mean(3,:), 'linewidth', 2);
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Inertial $z$ position from $10$ trials, with mean';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$t$ $[timesteps]$';
axes.XLabel.FontSize = 14;
axes.YLabel.Interpreter = 'latex';
axes.YLabel.String = '$p^{(I)}_z$ $[m]$';
axes.YLabel.FontSize = 14;
% roll angle sub-plot
subplot(2,3,4);
plot(y_real_data{1}(4,:));
hold on;
for i = 2 : num_exp
    plot(y_real_data{i}(4,:));
end
plot(y_real_data_mean(4,:), 'linewidth', 2);
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Roll angle from $10$ trials, with mean';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$t$ $[timesteps]$';
axes.XLabel.FontSize = 14;
axes.YLabel.Interpreter = 'latex';
axes.YLabel.String = '$\gamma$ $[rad]$';
axes.YLabel.FontSize = 14;
% pitch angle sub-plot
subplot(2,3,5);
plot(y_real_data{1}(5,:));
hold on;
for i = 2 : num_exp
    plot(y_real_data{i}(5,:));
end
plot(y_real_data_mean(5,:), 'linewidth', 2);
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Pitch angle from $10$ trials, with mean';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$t$ $[timesteps]$';
axes.XLabel.FontSize = 14;
axes.YLabel.Interpreter = 'latex';
axes.YLabel.String = '$\beta$ $[rad]$';
axes.YLabel.FontSize = 14;
% yaw angle sub-plot
subplot(2,3,6);
plot(y_real_data{1}(6,:));
hold on;
for i = 2 : num_exp
    plot(y_real_data{i}(6,:));
end
plot(y_real_data_mean(6,:), 'linewidth', 2);
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Yaw angle from $10$ trials, with mean';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$t$ $[timesteps]$';
axes.XLabel.FontSize = 14;
axes.YLabel.Interpreter = 'latex';
axes.YLabel.String = '$\alpha$ $[rad]$';
axes.YLabel.FontSize = 14;

%% Helper function
function y_semi_tight(ax, scale)
    axis(ax, 'tight'); % Set axis tight
    yl = ylim(ax); % Get tight axis limits
    range = yl(2) - yl(1); % Get tight axis range
    sc_range = scale * range; % Scale range
    yl(1) = yl(1) - (sc_range - range) / 2; % New ymin
    yl(2) = yl(1) + sc_range; % New ymax
    ylim(ax, yl);
end