close all;

%% Screen size used to place plots
screensize = get(groot, 'ScreenSize');
screenwidth = screensize(3);
screenheight = screensize(4);
default_width = screenwidth / 2;
default_height = screenheight / 2;
fg = 1;

%% Manual entry of data
grid_Tini = 3 : 12;
grid_cost = [90.77 46.444 54.007 55.854 111.89 38.534 41.01 41.002 39.761 40.84];

%% Plot
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [0, 0, default_width, default_height];
stem(grid_Tini, grid_cost, 'Linewidth', 2);
axes = gca;
axis(axes, 'tight');
xl = xlim(axes);
xl(1) = xl(1) - 1;
xl(2) = xl(2) + 1;
xlim(axes, xl);
yl = ylim(axes);
%yl(1) = yl(2) * 0.99;
yl(2) = yl(2) * 1.1;
%yl(2) = 100;
ylim(axes, yl);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Number of Initial State Inputs/Outputs vs. Best Sum of Square Tracking Error';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$T_{ini}$';
axes.XLabel.FontSize = 14;
axes.YLabel.Interpreter = 'latex';
axes.YLabel.String = '$\sum \|y_r-r\|_2^2$';
axes.YLabel.FontSize = 14;

%% Helper function
function y_semi_tight(ax, scale)
    axis(ax, 'tight'); % Set axis tight
    yl = ylim(ax); % Get tight axis limits
    range = yl(2) - yl(1); % Get tight axis range
    sc_range = scale * range; % Scale range
    yl(1) = yl(1) - (sc_range - range) / 2; % New ymin
    yl(2) = yl(1) + sc_range; % New ymax
    ylim(ax, yl);
end