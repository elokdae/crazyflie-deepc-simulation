function fg = plot_DeePC_lambda_g_tuner(param, exc_param,...
    DeePC_param, DeePC_param_perf,...
    grid_lambda1_g, grid_lambda2_g, Tini, uini, yini, r, uf_perf, yf_perf,...
    g, uf, yf, opt_cost, opt_input_cost, opt_output_cost,...
    opt_cost_lambda1_g, opt_cost_lambda2_g,...
    y_sim,...
    uf_perf_real, yf_perf_real, y_sim_perf_real)

close all;

%% Screen size used to place plots
screensize = get(groot, 'ScreenSize');
screenwidth = screensize(3);
screenheight = screensize(4);
default_width = screenwidth / 2;
default_height = screenheight / 2;
fg = 1;

%% Figure save path
save_fig = false;
save_path = 'Plots/Nonlinear predictions fat/';

%% Some text for labels
upred_text = ["$F_{pred}$ $[N]$"; "$\omega^{(B)}_{x_{pred}}$ $[\frac{rad}{s}]$";...
    "$\omega^{(B)}_{y_{pred}}$ $[\frac{rad}{s}]$"; "$\omega^{(B)}_{z_{pred}}$ $[\frac{rad}{s}]$"];
upred_perf_text = ["$F_{pred_{perf}}$ $[N]$"; "$\omega^{(B)}_{x_{pred_{perf}}}$ $[\frac{rad}{s}]$";...
    "$\omega^{(B)}_{y_{pred_{perf}}}$ $[\frac{rad}{s}]$"; "$\omega^{(B)}_{z_{pred_{perf}}}$ $[\frac{rad}{s}]$"];

x_r_text = ["$p^{(I)}_{x_{ref}}$ $[m]$"; "$p^{(I)}_{y_{ref}}$ $[m]$"; "$p^{(I)}_{z_{ref}}$ $[m]$";...
    "$p^{(I)}_{x_{ref}}$ $[\frac{m}{s}]$"; "$p^{(I)}_{y_{ref}}$ $[\frac{m}{s}]$"; "$p^{(I)}_{z_{ref}}$ $[\frac{m}{s}]$";...
    "$\gamma_{ref}$ $[rad]$"; "$\beta_{ref}$ $[rad]$"; "$\alpha_{ref}$ $[rad]$"];
r_text = x_r_text(DeePC_param.ref_states(1));
for i = 2 : length(DeePC_param.ref_states)
    r_text = [r_text; x_r_text(DeePC_param.ref_states(i))];
end

xpred_text = ["$p^{(I)}_{x_{pred}}$ $[m]$"; "$p^{(I)}_{y_{pred}}$ $[m]$"; "$p^{(I)}_{z_{pred}}$ $[m]$";...
    "$\dot{p}^{(I)}_{x_{pred}}$ $[\frac{m}{s}]$"; "$\dot{p}^{(I)}_{y_{pred}}$ $[\frac{m}{s}]$"; "$\dot{p}^{(I)}_{pred}}$ $[\frac{m}{s}]$";...
    "$\gamma_{pred}$ $[rad]$"; "$\beta_{pred}$ $[rad]$"; "$\alpha_{pred}$ $[rad]$"];
xsim_text = ["$p^{(I)}_{x_{sim}}$ $[m]$"; "$p^{(I)}_{y_{sim}}$ $[m]$"; "$p^{(I)}_{z_{sim}}$ $[m]$";...
    "$\dot{p}^{(I)}_{x_{sim}}$ $[\frac{m}{s}]$"; "$\dot{p}^{(I)}_{y_{sim}}$ $[\frac{m}{s}]$"; "$\dot{p}^{(I)}_{z_{sim}}$ $[\frac{m}{s}]$";...
    "$\gamma_{sim}$ $[rad]$"; "$\beta_{sim}$ $[rad]$"; "$\alpha_{sim}$ $[rad]$"];
xpred_perf_text = ["$x_{pred_{perf}}$ $[m]$"; "$y_{pred_{perf}}$ $[m]$"; "$z_{pred_{perf}}$ $[m]$";...
    "$v_{x_{pred_{perf}}}$ $[\frac{m}{s}]$"; "$v_{y_{pred_{perf}}}$ $[\frac{m}{s}]$"; "$v_{z_{pred_{perf}}}$ $[\frac{m}{s}]$";...
    "$\theta_{r_{pred_{perf}}}$ $[rad]$"; "$\theta_{p_{pred_{perf}}}$ $[rad]$"; "$\theta_{y_{pred_{perf}}}$ $[rad]$"];
ypred_text = xpred_text(DeePC_param.ref_states(1));
ysim_text = xsim_text(DeePC_param.ref_states(1));
ypred_perf_text = xpred_perf_text(DeePC_param.ref_states(1));
for i = 2 : length(DeePC_param.ref_states)
    ypred_text = [ypred_text; xpred_text(DeePC_param.ref_states(i))];
    ysim_text = [ysim_text; xsim_text(DeePC_param.ref_states(i))];
    ypred_perf_text = [ypred_perf_text; xpred_perf_text(DeePC_param.ref_states(i))];
end

%% Extend reference for plot
r = repmat(r, exc_param.Nd + 1, 1);
r = reshape(r, param.p, []);

%% Add [uini; yini] to beginning of trajectories
uini = reshape(uini, param.m, []);
yini = reshape(yini, param.p, []);
r = [yini, r];
u_perf = [uini, uf_perf];
y_perf = [yini, yf_perf];
u = uf;
y = yf;
for i = 1 : length(grid_lambda1_g)
    for lambda2_g_i = 1 : length(grid_lambda2_g)
        u{i,lambda2_g_i} = [uini, u{i,lambda2_g_i}];
        y{i,lambda2_g_i} = [yini, y{i,lambda2_g_i}];
    end
end
u_perf_real = [uini, uf_perf_real];
y_perf_real = [yini, yf_perf_real];

time = -Tini : 1 : exc_param.Nd;

%% Predictions plot
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [0, 0, screenwidth, screenheight];

% Reference vs. predicted output plot
subplot(3,2,1:2);
plot(time, r(1,:));
lgd_text = "$ref$";
hold on;
% y prediction
y_pred = y{1,1};
y_pred_ref = y_pred(DeePC_param.ref_outputs,:);
ypred_plot(1) = plot(time, y_pred_ref(1,:), '--', 'LineWidth', 2);
lgd_text = [lgd_text, ypred_text(1)];
ypred_plot(1).XDataSource = 'time';
ypred_plot(1).YDataSource = 'y_pred_ref(1,:)';
for i = 2 : length(DeePC_param.ref_outputs)
    ypred_plot(i) = plot(time, y_pred_ref(i,:), '--', 'LineWidth', 2);
    lgd_text = [lgd_text, ypred_text(i)];
    ypred_plot(i).XDataSource = 'time';
    ypred_plot(i).YDataSource = strcat('y_pred_ref(', num2str(i), ',:)');
end

% % 'Perfect' y prediction
% y_perf_ref = y_perf(DeePC_param_perf.ref_outputs,:);
% plot(time, y_perf_ref(1,:), '--', 'LineWidth', 1);
% lgd_text = [lgd_text, ypred_perf_text(1)];
% for i = 2 : length(DeePC_param.ref_outputs)
%     plot(time, y_perf_ref(i,:), '--', 'LineWidth', 1);
%     lgd_text = [lgd_text, ypred_perf_text(i)];
% end

% y simulation
curr_y_sim = y_sim{1,1};
y_sim_ref = curr_y_sim(DeePC_param.ref_outputs,:);
ysim_plot(1) = plot(time, y_sim_ref(1,:), '--', 'LineWidth', 1);
lgd_text = [lgd_text, ysim_text(1)];
ysim_plot(1).XDataSource = 'time';
ysim_plot(1).YDataSource = 'y_sim_ref(1,:)';
for i = 2 : length(DeePC_param.ref_outputs)
    ysim_plot(i) = plot(time, y_sim_ref(i,:), '--', 'LineWidth', 1);
    lgd_text = [lgd_text, ysim_text(i)];
    ysim_plot(i).XDataSource = 'time';
    ysim_plot(i).YDataSource = strcat('y_sim_ref(', num2str(i), ',:)');
end
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Reference vs. Output';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[optimization$ $timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend(lgd_text);
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'northwest';

% Total thrust command plot
subplot(3,2,3);
% Thurst prediction
u_pred = u{1,1};
upred_plot(1) = plot(time(1:end-1), u_pred(1,:), '--', 'LineWidth', 2);
lgd_text = upred_text(1);
upred_plot(1).XDataSource = 'time(1:end-1)';
upred_plot(1).YDataSource = 'u_pred(1,:)';
hold on;
% 'Perfect' thrust prediction
plot(time(1:end-1), u_perf(1,:), '--', 'LineWidth', 1);
lgd_text = [lgd_text, upred_perf_text(1)];
axes = gca;
y_semi_tight(axes, 1.2);
thrust_yl = ylim(axes);  % Save axis limits for later
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Total Thrust Command';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[optimization$ $timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend(lgd_text);
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'northeast';

% Body rates command plot
subplot(3,2,4);
% Body rates prediction
upred_plot(2) = plot(time(1:end-1), u_pred(2,:), '--', 'LineWidth', 2);
lgd_text = upred_text(2);
upred_plot(2).XDataSource = 'time(1:end-1)';
upred_plot(2).YDataSource = 'u_pred(2,:)';
hold on;
for i = 3 : param.m
    upred_plot(i) = plot(time(1:end-1), u_pred(i,:), '--', 'LineWidth', 2);
    lgd_text = [lgd_text, upred_text(i)];
    upred_plot(i).XDataSource = 'time(1:end-1)';
    upred_plot(i).YDataSource = strcat('u_pred(', num2str(i), ',:)');
end
% 'Perfect' body rates prediction
for i = 2 : length(DeePC_param.ref_outputs)
    plot(time(1:end-1), u_perf(i,:), '--', 'LineWidth', 1);
    lgd_text = [lgd_text, upred_perf_text(i)];
end
axes = gca;
y_semi_tight(axes, 1.2);
omega_yl = ylim(axes);  % Save axis limits for later
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Body Rates Command';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[optimization$ $timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend(lgd_text);
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'northeast';

% Individual cost plot
subplot(3,2,5);
% Input + output cost
lambda_g_run = max([grid_lambda1_g(1) grid_lambda2_g(1)]);
opt_input_output_cost_run = opt_input_cost{1,1} + opt_output_cost{1,1};
cost_plot(1) = loglog(lambda_g_run, opt_input_output_cost_run);
lgd_text = "$cost_{in+out}$";
cost_plot(1).XDataSource = 'lambda_g_run';
cost_plot(1).YDataSource = 'opt_input_output_cost_run';
hold on;
opt_cost_lambda_g_run = opt_cost_lambda1_g{1,1} + opt_cost_lambda2_g{1,1};
cost_plot(2) = loglog(lambda_g_run, opt_cost_lambda_g_run);
lgd_text = [lgd_text, "$cost_{reg}$"];
cost_plot(2).XDataSource = 'lambda_g_run';
cost_plot(2).YDataSource = 'opt_cost_lambda_g_run';
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Individual Optimization Costs';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$\lambda_{g}$';
axes.XLabel.FontSize = 14;
lgd = legend(lgd_text);
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'northwest';

% Total cost plot
subplot(3,2,6);
opt_cost_run = opt_cost{1,1};
cost_plot(3) = loglog(lambda_g_run, opt_cost_run);
cost_plot(3).XDataSource = 'lambda_g_run';
cost_plot(3).YDataSource = 'opt_cost_run';
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Total Optimization Cost';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$\lambda_{g}$';
axes.XLabel.FontSize = 14;
axes.YLabel.Interpreter = 'latex';
axes.YLabel.String = '$cost_{tot}$';
axes.YLabel.FontSize = 14;

% % Update predictions on button click
% w = waitforbuttonpress;
% if w
%     if length(grid_lambda2_g) >= length(grid_lambda1_g)
%         lambda1_g_start_i = 1;
%         lambda2_g_start_i = 2;
%     else
%         lambda1_g_start_i = 2;
%         lambda2_g_start_i = 1;
%     end
%     for lambda1_g_i = lambda1_g_start_i : length(grid_lambda1_g)
%         for lambda2_g_i = lambda2_g_start_i : length(grid_lambda2_g)
%             % y
%             y_pred = y{lambda1_g_i,lambda2_g_i};
%             y_pred_ref = y_pred(DeePC_param.ref_outputs,:);
%             refreshdata(ypred_plot, 'caller');
%             
%             % y simulation
%             curr_y_sim = y_sim{lambda1_g_i,lambda2_g_i};
%             y_sim_ref = curr_y_sim(DeePC_param.ref_outputs,:);
%             refreshdata(ysim_plot, 'caller');
%             
%             % u
%             u_pred = u{lambda1_g_i,lambda2_g_i};
%             refreshdata(upred_plot, 'caller');
%             
%             % costs
%             lambda_g_run = [lambda_g_run,...
%                 max([grid_lambda1_g(lambda1_g_i) grid_lambda2_g(lambda2_g_i)])];
%             opt_input_output_cost_run = [opt_input_output_cost_run,...
%                 opt_input_cost{lambda1_g_i,lambda2_g_i} + opt_output_cost{lambda1_g_i,lambda2_g_i}];
%             opt_cost_lambda_g_run = [opt_cost_lambda_g_run,...
%                 opt_cost_lambda1_g{lambda1_g_i,lambda2_g_i} + opt_cost_lambda2_g{lambda1_g_i,lambda2_g_i}];
%             opt_cost_run = [opt_cost_run, opt_cost{lambda1_g_i,lambda2_g_i}];
%             refreshdata(cost_plot, 'caller');
%             
%             w = waitforbuttonpress;
%             if ~w
%                 break;
%             end
%             %pause(0.01);
%         end
%     end
% end

%% Plot of closest solution to 'perfect' one with real data
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [0, 0, default_width, default_height];

% Reference vs. predicted output plot
% If reference is identical across all outputs, only plot once as 'ref'
same_ref = true;
for i = 1 : length(DeePC_param.ref_outputs)
    for j = i + 1 : length(DeePC_param.ref_outputs)
        if ~isequal(r(i,:), r(j,:))
            same_ref = false;
            break;
        end
        if ~same_ref
            break;
        end
    end
end
subplot(2,2,1:2);
plot(time, r(1,:), '--', 'LineWidth', 2);
if same_ref
    lgd_text = "$ref$";
else
    lgd_text = r_text(1);
end
hold on;

% y prediction
y_pred = y_perf_real;
y_pred_ref = y_pred(DeePC_param.ref_outputs,:);
plot(time, y_pred_ref(1,:), 'LineWidth', 2);
lgd_text = [lgd_text, ypred_text(1)];
for i = 2 : length(DeePC_param.ref_outputs)
    if ~same_ref
        plot(time, r(i,:), '--', 'LineWidth', 2);
        lgd_text = [lgd_text, r_text(i)];
    end
    plot(time, y_pred_ref(i,:), 'LineWidth', 2);
    lgd_text = [lgd_text, ypred_text(i)];
end

% y simulation
curr_y_sim = y_sim_perf_real;
y_sim_ref = curr_y_sim(DeePC_param.ref_outputs,:);
plot(time, y_sim_ref(1,:), ':', 'LineWidth', 2);
lgd_text = [lgd_text, ysim_text(1)];
for i = 2 : length(DeePC_param.ref_outputs)
    plot(time, y_sim_ref(i,:), ':', 'LineWidth', 2);
    lgd_text = [lgd_text, ysim_text(i)];
end
axes_y = gca;
y_semi_tight(axes_y, 1.2);
axes_y.Title.Interpreter = 'latex';
axes_y.Title.String = 'Reference vs. Closest to True Optimal Prediction vs. Forward Simulation';
axes_y.Title.FontSize = 18;
axes_y.XAxis.TickLabelInterpreter = 'latex';
axes_y.XAxis.FontSize = 10;
axes_y.YAxis.TickLabelInterpreter = 'latex';
axes_y.YAxis.FontSize = 10;
axes_y.XLabel.Interpreter = 'latex';
axes_y.XLabel.String = '$k$ $[optimization$ $timesteps]$';
axes_y.XLabel.FontSize = 14;
lgd = legend(lgd_text);
lgd.Interpreter = 'latex';
lgd.FontSize = 10;
lgd.Location = 'northwest';

% Total thrust command plot
subplot(2,2,3);
% Thurst prediction
u_pred = u_perf_real;
plot(time(1:end-1), u_pred(1,:), 'LineWidth', 2);
lgd_text = upred_text(1);
axis tight;
axes_thrust = gca;
% yl = [sum(param.nrotor_vehicle_thrust_min) 0.75 * sum(param.nrotor_vehicle_thrust_max)];
% y_semi_tight_lim(axes_thrust, 1.2, yl);
%y_semi_tight(axes_thrust, 1.2);
load('yl_thrust.mat');
ylim(axes_thrust, yl_thrust);
axes_thrust.Title.Interpreter = 'latex';
axes_thrust.Title.String = 'Closest to True Optimal Total Thrust Command';
axes_thrust.Title.FontSize = 14;
axes_thrust.XAxis.TickLabelInterpreter = 'latex';
axes_thrust.XAxis.FontSize = 10;
axes_thrust.YAxis.TickLabelInterpreter = 'latex';
axes_thrust.YAxis.FontSize = 10;
axes_thrust.XLabel.Interpreter = 'latex';
axes_thrust.XLabel.String = '$k$ $[optimization$ $timesteps]$';
axes_thrust.XLabel.FontSize = 14;
lgd = legend(lgd_text);
lgd.Interpreter = 'latex';
lgd.FontSize = 10;
lgd.Location = 'northeast';

% Body rates command plot
subplot(2,2,4);
% Body rates prediction
plot(time(1:end-1), u_pred(2,:), 'LineWidth', 2);
lgd_text = upred_text(2);
hold on;
for i = 3 : param.m
    plot(time(1:end-1), u_pred(i,:), 'LineWidth', 2);
    lgd_text = [lgd_text, upred_text(i)];
end
axis tight;
axes_ang = gca;
% yl = [-pi pi];
% y_semi_tight_lim(axes_ang, 1.2, yl);
%y_semi_tight(axes_ang, 1.2);
load('yl_ang.mat');
ylim(axes_ang, yl_ang);
axes_ang.Title.Interpreter = 'latex';
axes_ang.Title.String = 'Closest to True Optimal Body Rates Command';
axes_ang.Title.FontSize = 14;
axes_ang.XAxis.TickLabelInterpreter = 'latex';
axes_ang.XAxis.FontSize = 10;
axes_ang.YAxis.TickLabelInterpreter = 'latex';
axes_ang.YAxis.FontSize = 10;
axes_ang.XLabel.Interpreter = 'latex';
axes_ang.XLabel.String = '$k$ $[optimization$ $timesteps]$';
axes_ang.XLabel.FontSize = 14;
lgd = legend(lgd_text);
lgd.Interpreter = 'latex';
lgd.FontSize = 10;
lgd.Location = 'northeast';

%% Plot with no costs
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [default_width, 0, default_width, default_height];

% Reference vs. predicted output plot
% If reference is identical across all outputs, only plot once as 'ref'
same_ref = true;
for i = 1 : length(DeePC_param.ref_outputs)
    for j = i + 1 : length(DeePC_param.ref_outputs)
        if ~isequal(r(i,:), r(j,:))
            same_ref = false;
            break;
        end
        if ~same_ref
            break;
        end
    end
end
subplot(2,2,1:2);
plot(time, r(1,:), '--', 'LineWidth', 2);
if same_ref
    lgd_text = "$ref$";
else
    lgd_text = r_text(1);
end
hold on;

% y prediction
y_pred = y{1,1};
y_pred_ref = y_pred(DeePC_param.ref_outputs,:);
ypred_plot(1) = plot(time, y_pred_ref(1,:), 'LineWidth', 2);
lgd_text = [lgd_text, ypred_text(1)];
ypred_plot(1).XDataSource = 'time';
ypred_plot(1).YDataSource = 'y_pred_ref(1,:)';
for i = 2 : length(DeePC_param.ref_outputs)
    if ~same_ref
        plot(time, r(i,:), '--', 'LineWidth', 2);
        lgd_text = [lgd_text, r_text(i)];
    end
    ypred_plot(i) = plot(time, y_pred_ref(i,:), 'LineWidth', 2);
    lgd_text = [lgd_text, ypred_text(i)];
    ypred_plot(i).XDataSource = 'time';
    ypred_plot(i).YDataSource = strcat('y_pred_ref(', num2str(i), ',:)');
end

% y simulation
curr_y_sim = y_sim{1,1};
y_sim_ref = curr_y_sim(DeePC_param.ref_outputs,:);
ysim_plot(1) = plot(time, y_sim_ref(1,:), ':', 'LineWidth', 2);
lgd_text = [lgd_text, ysim_text(1)];
ysim_plot(1).XDataSource = 'time';
ysim_plot(1).YDataSource = 'y_sim_ref(1,:)';
for i = 2 : length(DeePC_param.ref_outputs)
    ysim_plot(i) = plot(time, y_sim_ref(i,:), ':', 'LineWidth', 2);
    lgd_text = [lgd_text, ysim_text(i)];
    ysim_plot(i).XDataSource = 'time';
    ysim_plot(i).YDataSource = strcat('y_sim_ref(', num2str(i), ',:)');
end
axes_y = gca;
y_semi_tight(axes_y, 1.2);
axes_y.Title.Interpreter = 'latex';
axes_y.Title.String = 'Reference vs. DeePC Prediction vs. Forward Simulation';
axes_y.Title.FontSize = 18;
axes_y.XAxis.TickLabelInterpreter = 'latex';
axes_y.XAxis.FontSize = 10;
axes_y.YAxis.TickLabelInterpreter = 'latex';
axes_y.YAxis.FontSize = 10;
axes_y.XLabel.Interpreter = 'latex';
axes_y.XLabel.String = '$k$ $[optimization$ $timesteps]$';
axes_y.XLabel.FontSize = 14;
lgd = legend(lgd_text);
lgd.Interpreter = 'latex';
lgd.FontSize = 10;
lgd.Location = 'northwest';

% Total thrust command plot
subplot(2,2,3);
% Thurst prediction
u_pred = u{1,1};
upred_plot(1) = plot(time(1:end-1), u_pred(1,:), 'LineWidth', 2);
lgd_text = upred_text(1);
upred_plot(1).XDataSource = 'time(1:end-1)';
upred_plot(1).YDataSource = 'u_pred(1,:)';
axis tight;
axes_thrust = gca;
% yl = [sum(param.nrotor_vehicle_thrust_min) 0.75 * sum(param.nrotor_vehicle_thrust_max)];
% y_semi_tight_lim(axes_thrust, 1.2, yl);
%y_semi_tight(axes_thrust, 1.2);
load('yl_thrust.mat');
ylim(axes_thrust, yl_thrust);
axes_thrust.Title.Interpreter = 'latex';
axes_thrust.Title.String = 'Optimal Total Thrust Command';
axes_thrust.Title.FontSize = 18;
axes_thrust.XAxis.TickLabelInterpreter = 'latex';
axes_thrust.XAxis.FontSize = 10;
axes_thrust.YAxis.TickLabelInterpreter = 'latex';
axes_thrust.YAxis.FontSize = 10;
axes_thrust.XLabel.Interpreter = 'latex';
axes_thrust.XLabel.String = '$k$ $[optimization$ $timesteps]$';
axes_thrust.XLabel.FontSize = 14;
lgd = legend(lgd_text);
lgd.Interpreter = 'latex';
lgd.FontSize = 10;
lgd.Location = 'northeast';

% Body rates command plot
subplot(2,2,4);
% Body rates prediction
upred_plot(2) = plot(time(1:end-1), u_pred(2,:), 'LineWidth', 2);
lgd_text = upred_text(2);
upred_plot(2).XDataSource = 'time(1:end-1)';
upred_plot(2).YDataSource = 'u_pred(2,:)';
hold on;
for i = 3 : param.m
    upred_plot(i) = plot(time(1:end-1), u_pred(i,:), 'LineWidth', 2);
    lgd_text = [lgd_text, upred_text(i)];
    upred_plot(i).XDataSource = 'time(1:end-1)';
    upred_plot(i).YDataSource = strcat('u_pred(', num2str(i), ',:)');
end
axis tight;
axes_ang = gca;
% yl = [-pi pi];
% y_semi_tight_lim(axes_ang, 1.2, yl);
%y_semi_tight(axes_ang, 1.2);
load('yl_ang.mat');
ylim(axes_ang, yl_ang);
axes_ang.Title.Interpreter = 'latex';
axes_ang.Title.String = 'Optimal Body Rates Command';
axes_ang.Title.FontSize = 18;
axes_ang.XAxis.TickLabelInterpreter = 'latex';
axes_ang.XAxis.FontSize = 10;
axes_ang.YAxis.TickLabelInterpreter = 'latex';
axes_ang.YAxis.FontSize = 10;
axes_ang.XLabel.Interpreter = 'latex';
axes_ang.XLabel.String = '$k$ $[optimization$ $timesteps]$';
axes_ang.XLabel.FontSize = 14;
lgd = legend(lgd_text);
lgd.Interpreter = 'latex';
lgd.FontSize = 10;
lgd.Location = 'northeast';

if save_fig
    fig_name = strcat(save_path, num2str(lambda_g_run(end)), '.bmp');
    saveas(gcf, fig_name);
end

% Update predictions on button click
w = waitforbuttonpress;
if w
    if length(grid_lambda2_g) >= length(grid_lambda1_g)
        lambda1_g_start_i = 1;
        lambda2_g_start_i = 2;
    else
        lambda1_g_start_i = 2;
        lambda2_g_start_i = 1;
    end
    for lambda1_g_i = lambda1_g_start_i : length(grid_lambda1_g)
        for lambda2_g_i = lambda2_g_start_i : length(grid_lambda2_g)
            % y
            y_pred = y{lambda1_g_i,lambda2_g_i};
            y_pred_ref = y_pred(DeePC_param.ref_outputs,:);
            refreshdata(ypred_plot, 'caller');
            
            % y simulation
            curr_y_sim = y_sim{lambda1_g_i,lambda2_g_i};
            y_sim_ref = curr_y_sim(DeePC_param.ref_outputs,:);
            refreshdata(ysim_plot, 'caller');
            y_semi_tight(axes_y, 1.2);
            
            % u
            u_pred = u{lambda1_g_i,lambda2_g_i};
            refreshdata(upred_plot, 'caller');
            %y_semi_tight(axes_thrust, 1.2);
            %y_semi_tight(axes_ang, 1.2);
            
            % costs
            lambda_g_run = [lambda_g_run,...
                max([grid_lambda1_g(lambda1_g_i) grid_lambda2_g(lambda2_g_i)])];
            opt_input_output_cost_run = [opt_input_output_cost_run,...
                opt_input_cost{lambda1_g_i,lambda2_g_i} + opt_output_cost{lambda1_g_i,lambda2_g_i}];
            opt_cost_lambda_g_run = [opt_cost_lambda_g_run,...
                opt_cost_lambda1_g{lambda1_g_i,lambda2_g_i} + opt_cost_lambda2_g{lambda1_g_i,lambda2_g_i}];
            opt_cost_run = [opt_cost_run, opt_cost{lambda1_g_i,lambda2_g_i}];
            refreshdata(cost_plot, 'caller');
            
            if save_fig
                fig_name = strcat(save_path, num2str(lambda_g_run(end)), '.bmp');
                saveas(gcf, fig_name);
            end
            
            w = waitforbuttonpress;
            if ~w
                break;
            end
            %pause(0.01);
        end
    end
end

fprintf('Done plotting\n');
end

function y_semi_tight(ax, scale)
    axis(ax, 'tight'); % Set axis tight
    yl = ylim(ax); % Get tight axis limits
    range = yl(2) - yl(1); % Get tight axis range
    sc_range = scale * range; % Scale range
    yl(1) = yl(1) - (sc_range - range) / 2; % New ymin
    yl(2) = yl(1) + sc_range; % New ymax
    ylim(ax, yl);
end

function y_semi_tight_lim(ax, scale, yl)
    axis(ax, 'tight');
    range = yl(2) - yl(1); % Get tight axis range
    sc_range = scale * range; % Scale range
    yl(1) = yl(1) - (sc_range - range) / 2; % New ymin
    yl(2) = yl(1) + sc_range; % New ymax
    ylim(ax, yl);
end