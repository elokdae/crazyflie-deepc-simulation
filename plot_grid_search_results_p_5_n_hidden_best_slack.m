close all;

%% Screen size used to place plots
screensize = get(groot, 'ScreenSize');
screenwidth = screensize(3);
screenheight = screensize(4);
default_width = screenwidth / 2;
default_height = screenheight / 2;
fg = 1;

%% Manual entry of data
grid_n_hidden = 0 : 1 : 50;
grid_cost = [129.34 202.56 1339 107.78 188.52 131.96 ...
    269.27 286.95 315.26 20661 73201 ...
    1813.2 83.099 34286 39187 258.83 ...
    105.42 126.31 51.139 39.424 55.217 ...
    84.152 79.6 53.322 58.874 43.989 ...
    38.225 45.187 37.288 37.085 37.778 ...
    41.198 37.241 36.205 39.427 51.895 ...
    36.706 35.383 35.087 36.698 36.387 ...
    37.8 36.359 38.482 38.61 39.533 ...
    38.025 36.015 36.398 37.414 36.684];

n_hidden_good = 25;
cost_good = grid_cost(grid_n_hidden == n_hidden_good);

%% Plot
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [0, 0, default_width, default_height];
stem(grid_n_hidden, grid_cost, 'Linewidth', 2);
hold on;
stem(n_hidden_good, cost_good, 'Linewidth', 2);
axes = gca;
axis(axes, 'tight');
xl = xlim(axes);
xl(1) = xl(1) - 1;
xl(2) = xl(2) + 1;
xlim(axes, xl);
yl = ylim(axes);
%yl(1) = yl(2) * 0.99;
%yl(2) = yl(2) * 1.1;
yl(2) = 100;
ylim(axes, yl);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Number of Hidden States vs. Best Sum of Square Tracking Error';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$n_h$';
axes.XLabel.FontSize = 14;
axes.YLabel.Interpreter = 'latex';
axes.YLabel.String = '$\sum \|y_r-r\|_2^2$';
axes.YLabel.FontSize = 14;

%% Helper function
function y_semi_tight(ax, scale)
    axis(ax, 'tight'); % Set axis tight
    yl = ylim(ax); % Get tight axis limits
    range = yl(2) - yl(1); % Get tight axis range
    sc_range = scale * range; % Scale range
    yl(1) = yl(1) - (sc_range - range) / 2; % New ymin
    yl(2) = yl(1) + sc_range; % New ymax
    ylim(ax, yl);
end