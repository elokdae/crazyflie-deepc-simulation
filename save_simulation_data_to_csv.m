u_data_DeePC = [total_thrust_cmd.'; body_rates_cmd.'];
uf_data_DeePC = DeePC_param.U_f * g_out.';
y_data_DeePC = full_state_meas(:,1:3).';
yf_data_DeePC = DeePC_param.Y_f * g_out.';
r_data_DeePC = r_out.';
solveTime_data_DeePC = solve_time_out.';

dlmwrite('Simulation Data\u_data_DeePC.csv', u_data_DeePC);
dlmwrite('Simulation Data\uf_data_DeePC.csv', uf_data_DeePC);
dlmwrite('Simulation Data\y_data_DeePC.csv', y_data_DeePC);
dlmwrite('Simulation Data\yf_data_DeePC.csv', yf_data_DeePC);
dlmwrite('Simulation Data\r_data_DeePC.csv', r_data_DeePC);
dlmwrite('Simulation Data\solveTime_data_DeePC.csv', solveTime_data_DeePC);