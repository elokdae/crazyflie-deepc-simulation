clc;
clear all;
close all;

%% Screen size used to place plots
screensize = get(groot, 'ScreenSize');
screenwidth = screensize(3);
screenheight = screensize(4);
default_width = screenwidth / 2;
default_height = screenheight / 2;
%default_height = 1200 / 2;
fg = 1;

%% Load parameters
param = load_crazyflie_parameters();

%% Specify data files
u_data_LQR_file = 'Real Data/Step Tracking/m_u_data_lqr.csv';
y_data_LQR_file = 'Real Data/Step Tracking/m_y_data_lqr.csv';
r_data_LQR_file = 'Real Data/Step Tracking/m_r_data_lqr.csv';
% u_data_LQR_file = 'Real Data/Step Tracking/LQR yaw control/PRBS 255 Real Data no P/m_u_data_Deepc.csv';
% y_data_LQR_file = 'Real Data/Step Tracking/LQR yaw control/PRBS 255 Real Data no P/m_y_data_Deepc.csv';
% r_data_LQR_file = 'Real Data/Step Tracking/LQR yaw control/PRBS 255 Real Data no P/m_r_data_Deepc.csv';

u_data_DeePC_file = 'Real Data/Step Tracking/LQR yaw control/PRBS 255 Real Data with P/m_u_data_Deepc.csv';
y_data_DeePC_file = 'Real Data/Step Tracking/LQR yaw control/PRBS 255 Real Data with P/m_y_data_Deepc.csv';
r_data_DeePC_file = 'Real Data/Step Tracking/LQR yaw control/PRBS 255 Real Data with P/m_r_data_Deepc.csv';
% u_data_DeePC_file = 'Real Data/Step Tracking/LQR yaw control/PRBS 255 Linear Data offset-free/m_u_data_Deepc.csv';
% y_data_DeePC_file = 'Real Data/Step Tracking/LQR yaw control/PRBS 255 Linear Data offset-free/m_y_data_Deepc.csv';
% r_data_DeePC_file = 'Real Data/Step Tracking/LQR yaw control/PRBS 255 Linear Data offset-free/m_r_data_Deepc.csv';

%% Some parameters for plotting
plot_time = 14;
Tini = 6;
Ttot = ceil(plot_time / param.sample_time_measurements_full_state);
z_find = 1.3;

%% Reference outputs
% Reference is x, y, z, yaw
% If DeePC yaw control is disabled, do not track yaw
if param.DeePC_yaw_control
    if param.measure_angles
        ref_outputs = [1; 2; 3; 6];
    else
        ref_outputs = [1; 2; 3; 4];
    end
    ref_states = [1; 2; 3; 9];
else
    ref_outputs = [1; 2; 3];
    ref_states = [1; 2; 3];
end

%% Some text for labels
u_text = ["$F$ $[N]$"; "$\omega^{(B)}_{x}$ $[\frac{rad}{s}]$";...
    "$\omega^{(B)}_{y}$ $[\frac{rad}{s}]$"; "$\omega^{(B)}_{z}$ $[\frac{rad}{s}]$"];

x_text = ["$p^{(I)}_x$ $[m]$"; "$p^{(I)}_y$ $[m]$"; "$p^{(I)}_z$ $[m]$";...
    "$\dot{p}^{(I)}_{x}$ $[\frac{m}{s}]$"; "$\dot{p}^{(I)}_{y}$ $[\frac{m}{s}]$"; "$\dot{p}^{(I)}_{z}$ $[\frac{m}{s}]$";...
    "$\gamma$ $[rad]$"; "$\beta$ $[rad]$"; "$\alpha$ $[rad]$"];
i = find(param.Cd(1,:) == 1);
y_text = x_text(i);
for i = 2 : size(param.Cd, 1)
    i = find(param.Cd(i,:) == 1);
    y_text = [y_text; x_text(i)];
end
yref_text = y_text(ref_outputs);

x_r_text = ["$p^{(I)}_{x_{ref}}$ $[m]$"; "$p^{(I)}_{y_{ref}}$ $[m]$"; "$p^{(I)}_{z_{ref}}$ $[m]$";...
    "$\dot{p}^{(I)}_{x_{ref}}$ $[\frac{m}{s}]$"; "$\dot{p}^{(I)}_{y_{ref}}$ $[\frac{m}{s}]$"; "$\dot{p}^{(I)}_{z_{ref}}$ $[\frac{m}{s}]$";...
    "$\gamma_{ref}$ $[rad]$"; "$\beta_{ref}$ $[rad]$"; "$\alpha_{ref}$ $[rad]$"];
r_text = x_r_text(ref_states(1));
for i = 2 : length(ref_states)
    r_text = [r_text; x_r_text(ref_states(i))];
end

u_text_LQR = ["$F_{LQR}$ $[N]$"; "$\omega^{(B)}_{x_{LQR}}$ $[\frac{rad}{s}]$";...
    "$\\omega^{(B)}_{y_{LQR}}$ $[\frac{rad}{s}]$"; "$\omega^{(B)}_{z_{LQR}}$ $[\frac{rad}{s}]$"];
u_text_DeePC = ["$F_{DeePC}$ $[N]$"; "$\omega^{(B)}_{x_{DeePC}}$ $[\frac{rad}{s}]$";...
    "$\omega^{(B)}_{y_{DeePC}}$ $[\frac{rad}{s}]$"; "$\omega^{(B)}_{z_{DeePC}}$ $[\frac{rad}{s}]$"];

x_text_LQR = ["$p^{(I)}_{x_{LQR}}$ $[m]$"; "$p^{(I)}_{y_{LQR}}$ $[m]$"; "$p^{(I)}_{z_{LQR}}$ $[m]$";...
    "$\dot{p}^{(I)}_{x_{LQR}}$ $[\frac{m}{s}]$"; "$\dot{p}^{(I)}_{y_{LQR}}$ $[\frac{m}{s}]$"; "$\dot{p}^{(I)}_{z_{LQR}}$ $[\frac{m}{s}]$";...
    "$\gamma_{LQR}$ $[rad]$"; "$\beta_{LQR}$ $[rad]$"; "$\alpha_{LQR}$ $[rad]$"];
i = find(param.Cd(1,:) == 1);
y_text_LQR = x_text_LQR(i);
for i = 2 : size(param.Cd, 1)
    i = find(param.Cd(i,:) == 1);
    y_text_LQR = [y_text_LQR; x_text_LQR(i)];
end
yref_text_LQR = y_text_LQR(ref_outputs);

x_text_DeePC = ["$p^{(I)}_{x_{DeePC}}$ $[m]$"; "$p^{(I)}_{y_{DeePC}}$ $[m]$"; "$p^{(I)}_{z_{DeePC}}$ $[m]$";...
    "$\dot{p}^{(I)}_{x_{DeePC}}$ $[\frac{m}{s}]$"; "$\dot{p}^{(I)}_{y_{DeePC}}$ $[\frac{m}{s}]$"; "$\dot{p}^{(I)}_{z_{DeePC}}$ $[\frac{m}{s}]$";...
    "$\gamma_{DeePC}$ $[rad]$"; "$\beta_{DeePC}$ $[rad]$"; "$\alpha_{DeePC}$ $[rad]$"];
i = find(param.Cd(1,:) == 1);
y_text_DeePC = x_text_DeePC(i);
for i = 2 : size(param.Cd, 1)
    i = find(param.Cd(i,:) == 1);
    y_text_DeePC = [y_text_DeePC; x_text_DeePC(i)];
end
yref_text_DeePC = y_text_DeePC(ref_outputs);

%% Get data
u_data_LQR = csvread(u_data_LQR_file);
u_data_LQR = eye(param.m, 4) * u_data_LQR;

y_data_LQR = csvread(y_data_LQR_file);
y_data_LQR = y_data_LQR(ref_outputs,:);

r_data_LQR = csvread(r_data_LQR_file);
if ~param.DeePC_yaw_control
    r_data_LQR(end,:) = [];
end

u_data_DeePC = csvread(u_data_DeePC_file);
u_data_DeePC = eye(param.m, 4) * u_data_DeePC;

y_data_DeePC = csvread(y_data_DeePC_file);
y_data_DeePC = y_data_DeePC(ref_outputs,:);

r_data_DeePC = csvread(r_data_DeePC_file);
if ~param.DeePC_yaw_control
    r_data_DeePC(end,:) = [];
end

%% Get plot range
start_i_LQR = find(r_data_LQR(3,:) == z_find) - Tini;
%start_i_LQR = find(r_data_LQR(3,:) ~= 0.4) - Tini;
start_i_LQR = start_i_LQR(1);
end_i_LQR = start_i_LQR + Ttot;

u_data_LQR_plot = u_data_LQR(:,start_i_LQR:end_i_LQR);
y_data_LQR_plot = y_data_LQR(:,start_i_LQR:end_i_LQR);
r_data_LQR_plot = r_data_LQR(:,start_i_LQR:end_i_LQR);

error_LQR = y_data_LQR_plot(:,Tini+1:end) - r_data_LQR_plot(:,Tini+1:end);
output_cost_LQR = sum(diag(error_LQR.' * error_LQR));

%r_data_DeePC = circshift(r_data_DeePC, 17, 2);
start_i_DeePC = find(r_data_DeePC(3,:) == z_find) - Tini;
%start_i_DeePC = find(r_data_DeePC(3,:) ~= 0.4) - Tini;
start_i_DeePC = start_i_DeePC(1);
end_i_DeePC = start_i_DeePC + Ttot;

u_data_DeePC_plot = u_data_DeePC(:,start_i_DeePC:end_i_DeePC);
y_data_DeePC_plot = y_data_DeePC(:,start_i_DeePC:end_i_DeePC);
r_data_DeePC_plot = r_data_DeePC(:,start_i_DeePC:end_i_DeePC);

error_DeePC = y_data_DeePC_plot(:,Tini+1:end) - r_data_DeePC_plot(:,Tini+1:end);
output_cost_DeePC = sum(diag(error_DeePC.' * error_DeePC));

%% Overview plot
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [0, 0, screenwidth, screenheight];

% LQR Reference vs. output plot
subplot(3,2,1);
plot(r_data_LQR(1,:));
lgd_text = r_text(1);
hold on;
plot(y_data_LQR(1,:));
lgd_text = [lgd_text, yref_text_LQR(1)];
for i = 2 : length(ref_outputs)
    plot(r_data_LQR(i,:));
    lgd_text = [lgd_text, r_text(i)];
    plot(y_data_LQR(i,:));
    lgd_text = [lgd_text, yref_text_LQR(i)];
end
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'LQR Reference vs. Output';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend(lgd_text);
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'northwest';

% DeePC Reference vs. output plot
subplot(3,2,2);
plot(r_data_DeePC(1,:));
lgd_text = r_text(1);
hold on;
plot(y_data_DeePC(1,:));
lgd_text = [lgd_text, yref_text_DeePC(1)];
for i = 2 : length(ref_outputs)
    plot(r_data_DeePC(i,:));
    lgd_text = [lgd_text, r_text(i)];
    plot(y_data_DeePC(i,:));
    lgd_text = [lgd_text, yref_text_DeePC(i)];
end
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'DeePC Reference vs. Output';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend(lgd_text);
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'northwest';

% LQR Total thrust command plot
subplot(3,2,3);
plot(u_data_LQR(1,:));
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'LQR Total Thrust Command';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;
axes.YLabel.Interpreter = 'latex';
axes.YLabel.String = u_text_LQR(1);
axes.YLabel.FontSize = 14;

% DeePC Total thrust command plot
subplot(3,2,4);
plot(u_data_DeePC(1,:));
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'LQR Total Thrust Command';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;
axes.YLabel.Interpreter = 'latex';
axes.YLabel.String = u_text_DeePC(1);
axes.YLabel.FontSize = 14;

% LQR Body rates command plot
subplot(3,2,5);
plot(u_data_LQR(2,:));
lgd_text = u_text_LQR(2);
hold on;
for i = 3 : param.m
    plot(u_data_LQR(i,:));
    lgd_text = [lgd_text, u_text_LQR(i)];
end
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'LQR Body Rates Command';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend(lgd_text);
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'northwest';

% DeePC Body rates command plot
subplot(3,2,6);
plot(u_data_DeePC(2,:));
lgd_text = u_text_DeePC(2);
hold on;
for i = 3 : param.m
    plot(u_data_DeePC(i,:));
    lgd_text = [lgd_text, u_text_DeePC(i)];
end
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'DeePC Body Rates Command';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend(lgd_text);
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'northwest';

%% LQR plot
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [0, 0, default_width, default_height];

% Reference vs. measured output plot
% Reference is identical across all outputs, only plot once as 'ref'
same_ref = true;
for i = 1 : length(ref_outputs)
    for j = i + 1 : length(ref_outputs)
        if ~isequal(r_data_LQR_plot(i,:), r_data_LQR_plot(j,:))
            same_ref = false;
            break;
        end
        if ~same_ref
            break;
        end
    end
end
subplot(3,2,1:4);
plot(r_data_LQR_plot(1,:), '--', 'LineWidth', 2);
if same_ref
    lgd_text = "$ref$";
else
    lgd_text = r_text(1);
end
hold on;
plot(y_data_LQR_plot(1,:), 'LineWidth', 2);
lgd_text = [lgd_text, yref_text(1)];
for i = 2 : length(ref_outputs)
    if ~same_ref
        plot(r_data_LQR_plot(i,:), '--', 'LineWidth', 2);
        lgd_text = [lgd_text, r_text(i)];
    end
    plot(y_data_LQR_plot(i,:), 'LineWidth', 2);
    lgd_text = [lgd_text, yref_text(i)];
end
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Reference vs. Output';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$t$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend(lgd_text);
lgd.Interpreter = 'latex';
lgd.FontSize = 10;
lgd.Location = 'southeast';
%lgd.Location = 'bestoutside';

% Total thrust command plot
subplot(3,2,5);
plot(u_data_LQR_plot(1,:), 'LineWidth', 2);
lgd_text = u_text(1);
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Total Thrust Command';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$t$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend(lgd_text);
lgd.Interpreter = 'latex';
lgd.FontSize = 10;
lgd.Location = 'northeast';
%lgd.Location = 'bestoutside';

% Body rates command plot
subplot(3,2,6);
plot(u_data_LQR_plot(2,:), 'LineWidth', 2);
lgd_text = u_text(2);
hold on;
for i = 3 : param.m
    plot(u_data_LQR_plot(i,:), 'LineWidth', 2);
    lgd_text = [lgd_text, u_text(i)];
end
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Body Rates Command';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$t$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend(lgd_text);
lgd.Interpreter = 'latex';
lgd.FontSize = 10;
lgd.Location = 'northeast';
%lgd.Location = 'bestoutside';

%% DeePC plot
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [default_width, 0, default_width, default_height];

% Reference vs. measured output plot
% Reference is identical across all outputs, only plot once as 'ref'
same_ref = true;
for i = 1 : length(ref_outputs)
    for j = i + 1 : length(ref_outputs)
        if ~isequal(r_data_DeePC_plot(i,:), r_data_DeePC_plot(j,:))
            same_ref = false;
            break;
        end
        if ~same_ref
            break;
        end
    end
end
subplot(3,2,1:4);
plot(r_data_DeePC_plot(1,:), '--', 'LineWidth', 2);
if same_ref
    lgd_text = "$ref$";
else
    lgd_text = r_text(1);
end
hold on;
plot(y_data_DeePC_plot(1,:), 'LineWidth', 2);
lgd_text = [lgd_text, yref_text(1)];
for i = 2 : length(ref_outputs)
    if ~same_ref
        plot(r_data_DeePC_plot(i,:), '--', 'LineWidth', 2);
        lgd_text = [lgd_text, r_text(i)];
    end
    plot(y_data_DeePC_plot(i,:), 'LineWidth', 2);
    lgd_text = [lgd_text, yref_text(i)];
end
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Reference vs. Output';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$t$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend(lgd_text);
lgd.Interpreter = 'latex';
lgd.FontSize = 10;
lgd.Location = 'southeast';
%lgd.Location = 'bestoutside';

% Total thrust command plot
subplot(3,2,5);
plot(u_data_DeePC_plot(1,:), 'LineWidth', 2);
lgd_text = u_text(1);
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Total Thrust Command';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$t$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend(lgd_text);
lgd.Interpreter = 'latex';
lgd.FontSize = 10;
lgd.Location = 'northeast';
%lgd.Location = 'bestoutside';

% Body rates command plot
subplot(3,2,6);
plot(u_data_DeePC_plot(2,:), 'LineWidth', 2);
lgd_text = u_text(2);
hold on;
for i = 3 : param.m
    plot(u_data_DeePC_plot(i,:), 'LineWidth', 2);
    lgd_text = [lgd_text, u_text(i)];
end
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Body Rates Command';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$t$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend(lgd_text);
lgd.Interpreter = 'latex';
lgd.FontSize = 10;
lgd.Location = 'northeast';
%lgd.Location = 'southeast';
%lgd.Location = 'bestoutside';

fprintf('Done plotting\n');

function y_semi_tight(ax, scale)
    axis tight; % Set axis tight
    yl = ylim(ax); % Get tight axis limits
    range = yl(2) - yl(1); % Get tight axis range
    sc_range = scale * range; % Scale range
    yl(1) = yl(1) - (sc_range - range) / 2; % New ymin
    yl(2) = yl(1) + sc_range; % New ymax
    ylim(ax, yl);
end