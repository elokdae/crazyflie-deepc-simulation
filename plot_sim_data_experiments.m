clc;
clear all;
close all;

%% Screen size used to place plots
screensize = get(groot, 'ScreenSize');
screenwidth = screensize(3);
screenheight = screensize(4);
default_width = screenwidth / 2;
default_height = screenheight / 2;
%default_height = 1200 / 2;
fg = 1;

%% Load parameters
param = load_crazyflie_parameters();
Nd = 25;

%% Specify data files
file_prefix = 'Simulation Data/PRBS 331 p=3 Experiments/75 Regularizer (Small)/';

u_data_DeePC_file = [file_prefix, 'u_data_Deepc.csv'];
uf_data_DeePC_file = [file_prefix, 'uf_data_Deepc.csv'];
y_data_DeePC_file = [file_prefix, 'y_data_Deepc.csv'];
yf_data_DeePC_file = [file_prefix, 'yf_data_Deepc.csv'];
r_data_DeePC_file = [file_prefix, 'r_data_Deepc.csv'];
solveTime_data_DeePC_file = [file_prefix, 'solveTime_data_Deepc.csv'];

%% Reference outputs
% Reference is x, y, z, yaw
% If DeePC yaw control is disabled, do not track yaw
if param.DeePC_yaw_control
    if param.measure_angles
        ref_outputs = [1; 2; 3; 6];
    else
        ref_outputs = [1; 2; 3; 4];
    end
    ref_states = [1; 2; 3; 9];
else
    ref_outputs = [1; 2; 3];
    ref_states = [1; 2; 3];
end

%% Some text for labels
u_text = ["$F_{DeePC}$ $[N]$"; "$\omega^{(B)}_{x_{DeePC}}$ $[\frac{rad}{s}]$";...
    "$\omega^{(B)}_{y_{DeePC}}$ $[\frac{rad}{s}]$"; "$\omega^{(B)}_{z_{DeePC}}$ $[\frac{rad}{s}]$"];
upred_text = ["$F_{pred}$ $[N]$"; "$\omega^{(B)}_{x_{pred}}$ $[\frac{rad}{s}]$";...
    "$\omega^{(B)}_{y_{pred}}$ $[\frac{rad}{s}]$"; "$\omega^{(B)}_{z_{pred}}$ $[\frac{rad}{s}]$"];

x_text = ["$p^{(I)}_{x_{DeePC}}$ $[m]$"; "$p^{(I)}_{y_{DeePC}}$ $[m]$"; "$p^{(I)}_{z_{DeePC}}$ $[m]$";...
    "$\dot{p}^{(I)}_{x_{DeePC}}$ $[\frac{m}{s}]$"; "$\dot{p}^{(I)}_{y_{DeePC}}$ $[\frac{m}{s}]$"; "$\dot{p}^{(I)}_{z_{DeePC}}$ $[\frac{m}{s}]$";...
    "$\gamma_{DeePC}$ $[rad]$"; "$\beta_{DeePC}$ $[rad]$"; "$\alpha_{DeePC}$ $[rad]$"];
i = find(param.Cd(1,:) == 1);
y_text = x_text(i);
for i = 2 : size(param.Cd, 1)
    i = find(param.Cd(i,:) == 1);
    y_text = [y_text; x_text(i)];
end
yref_text = y_text(ref_outputs);

xpred_text = ["$p^{(I)}_{x_{pred}}$ $[m]$"; "$p^{(I)}_{y_{pred}}$ $[m]$"; "$p^{(I)}_{z_{pred}}$ $[m]$";...
    "$\dot{p}^{(I)}_{x_{pred}}$ $[\frac{m}{s}]$"; "$\dot{p}^{(I)}_{y_{pred}}$ $[\frac{m}{s}]$"; "$\dot{p}^{(I)}_{pred}}$ $[\frac{m}{s}]$";...
    "$\gamma_{pred}$ $[rad]$"; "$\beta_{pred}$ $[rad]$"; "$\alpha_{pred}$ $[rad]$"];
ypred_text = xpred_text(ref_states(1));
for i = 2 : length(ref_states)
    ypred_text = [ypred_text; xpred_text(ref_states(i))];
end

x_r_text = ["$p^{(I)}_{x_{ref}}$ $[m]$"; "$p^{(I)}_{y_{ref}}$ $[m]$"; "$p^{(I)}_{z_{ref}}$ $[m]$";...
    "$\dot{p}^{(I)}_{x_{ref}}$ $[\frac{m}{s}]$"; "$\dot{p}^{(I)}_{y_{ref}}$ $[\frac{m}{s}]$"; "$\dot{p}^{(I)}_{z_{ref}}$ $[\frac{m}{s}]$";...
    "$\gamma_{ref}$ $[rad]$"; "$\beta_{ref}$ $[rad]$"; "$\alpha_{ref}$ $[rad]$"];
r_text = x_r_text(ref_states(1));
for i = 2 : length(ref_states)
    r_text = [r_text; x_r_text(ref_states(i))];
end

%% Get data
u_data_DeePC = csvread(u_data_DeePC_file);
uf_data_DeePC = csvread(uf_data_DeePC_file);
y_data_DeePC = csvread(y_data_DeePC_file);
yf_data_DeePC = csvread(yf_data_DeePC_file);
r_data_DeePC = csvread(r_data_DeePC_file);
solveTime_data_DeePC = csvread(solveTime_data_DeePC_file);

%% Overview plot
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [0, 0, screenwidth, screenheight];

% DeePC Reference vs. output plot
subplot(3,2,1:2);
plot(r_data_DeePC(1,:), '--', 'LineWidth', 2);
lgd_text = r_text(1);
hold on;
plot(y_data_DeePC(1,:), 'LineWidth', 2);
lgd_text = [lgd_text, yref_text(1)];
for i = 2 : length(ref_outputs)
    plot(r_data_DeePC(i,:), '--', 'LineWidth', 2);
    lgd_text = [lgd_text, r_text(i)];
    plot(y_data_DeePC(i,:), 'LineWidth', 2);
    lgd_text = [lgd_text, yref_text(i)];
end
if param.plot_predictions
    % y Predictions
    y_pred = yf_data_DeePC(:,1);
    y_pred = reshape(y_pred, param.p, []);
    y_pred_ref = y_pred(ref_outputs,:);
    find_i = find(r_data_DeePC(1,:) ~= r_data_DeePC(1,1));
    find_i = find_i(1);
    y_pred_ind = find_i : find_i + Nd;
    ypred_plot(1) = plot(y_pred_ind, y_pred_ref(1,:), ':', 'LineWidth', 2);
    lgd_text = [lgd_text, ypred_text(1)];
    ypred_plot(1).XDataSource = 'y_pred_ind';
    ypred_plot(1).YDataSource = 'y_pred_ref(1,:)';
    for i = 2 : length(ref_outputs)
        ypred_plot(i) = plot(y_pred_ind, y_pred_ref(i,:), ':', 'LineWidth', 2);
        lgd_text = [lgd_text, ypred_text(i)];
        ypred_plot(i).XDataSource = 'y_pred_ind';
        ypred_plot(i).YDataSource = strcat('y_pred_ref(', num2str(i), ',:)');
    end
end
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'DeePC Reference vs. Output';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend(lgd_text);
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'northeast';

% DeePC Total thrust command plot
subplot(3,2,3);
plot(u_data_DeePC(1,:), 'LineWidth', 2);
lgd_text = u_text(1);
if param.plot_predictions
    hold on;
    % Thrust Predictions
    u_pred = uf_data_DeePC(:,1);
    u_pred = reshape(u_pred, param.m, []);
    u_pred_ind = y_pred_ind(1:end-1);
    upred_plot(1) = plot(u_pred_ind, u_pred(1,:), ':', 'LineWidth', 2);
    lgd_text = [lgd_text, upred_text(1)];
    upred_plot(1).XDataSource = 'u_pred_ind';
    upred_plot(1).YDataSource = 'u_pred(1,:)';
end
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'DeePC Total Thrust Command';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend(lgd_text);
lgd.Interpreter = 'latex';
lgd.FontSize = 10;
lgd.Location = 'northeast';

% DeePC Body rates command plot
subplot(3,2,4);
plot(u_data_DeePC(2,:), 'LineWidth', 2);
lgd_text = u_text(2);
hold on;
for i = 3 : param.m
    plot(u_data_DeePC(i,:), 'LineWidth', 2);
    lgd_text = [lgd_text, u_text(i)];
end
if param.plot_predictions
    % Body rates predictions
    for i = 2 : param.m
        upred_plot(i) = plot(u_pred_ind, u_pred(i,:), ':', 'LineWidth', 2);
        lgd_text = [lgd_text, upred_text(i)];
        upred_plot(i).XDataSource = 'u_pred_ind';
        upred_plot(i).YDataSource = strcat('u_pred(', num2str(i), ',:)');
    end
end
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'DeePC Body Rates Command';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend(lgd_text);
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'northeast';

% DeePC solve time plot
subplot(3,2,5:6);
plot(solveTime_data_DeePC, 'LineWidth', 2);
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Optimization Solve Time';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;
axes.YLabel.Interpreter = 'latex';
axes.YLabel.String = '$t_{solve}$ $[sec]$';
axes.YLabel.FontSize = 14;

if param.plot_predictions
% Update predictions on button click
w = waitforbuttonpress;
    if w
        for i = 2 : size(uf_data_DeePC, 2)
            % y
            y_pred = yf_data_DeePC(:,i);
            y_pred = reshape(y_pred, param.p, []);
            y_pred_ref = y_pred(ref_outputs,:);
            y_pred_ind = y_pred_ind + 1;
            refreshdata(ypred_plot, 'caller');
            
            % u
            u_pred = uf_data_DeePC(:,i);
            u_pred = reshape(u_pred, param.m, []);
            u_pred_ind = y_pred_ind(1:end-1);
            refreshdata(upred_plot, 'caller');

            w = waitforbuttonpress;
            if ~w
                break;
            end
        end
    end
end