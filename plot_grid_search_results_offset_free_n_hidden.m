close all;

%% Screen size used to place plots
screensize = get(groot, 'ScreenSize');
screenwidth = screensize(3);
screenheight = screensize(4);
default_width = screenwidth / 2;
default_height = screenheight / 2;
fg = 1;

%% Manual entry of data
Tini = 3;
m = 3;
p = 5;
N = 25;
n = 8;
d = 1;
grid_n_hidden = 0 : 1 : 100;
load('grid_cost_offset_free_n_hidden.mat');

%% Plot
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [0, 0, default_width, default_height];
stem(grid_n_hidden, grid_cost, 'Linewidth', 1);
axes = gca;
axis(axes, 'tight');
xl = xlim(axes);
xl(1) = xl(1) - 1;
xl(2) = xl(2) + 1;
xlim(axes, xl);
yl = ylim(axes);
yl(1) = yl(2) * 0.99;
%yl(2) = yl(2) * 1.1;
%yl(2) = 100;
ylim(axes, yl);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Number of Hidden States vs. Sum of Square Tracking Error';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$n_h$';
axes.XLabel.FontSize = 14;
axes.YLabel.Interpreter = 'latex';
axes.YLabel.String = '$\sum \|y_r-r\|_2^2$';
axes.YLabel.FontSize = 14;

%% Helper function
function y_semi_tight(ax, scale)
    axis(ax, 'tight'); % Set axis tight
    yl = ylim(ax); % Get tight axis limits
    range = yl(2) - yl(1); % Get tight axis range
    sc_range = scale * range; % Scale range
    yl(1) = yl(1) - (sc_range - range) / 2; % New ymin
    yl(2) = yl(1) + sc_range; % New ymax
    ylim(ax, yl);
end