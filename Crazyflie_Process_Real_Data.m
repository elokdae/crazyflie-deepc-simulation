clear;
close all;
clc;

% Screen size used to place plots
screensize = get(groot, 'ScreenSize');
screenwidth = screensize(3);
screenheight = screensize(4);
fg = 1;

%% Load Crazyflie parameters
param = load_crazyflie_parameters();

%% Load excitation parameters
% Main tuning parmeters

% Additional hidden states - more than model states
n_hidden = 0;

% Tini
Tini = 9;

% exc_signal_select: 1 = random signal, 2 = PRBS
exc_signal_select = 2;

% rng_seed: Random seed used for random excitation signal generation (not
% used for PRBS)
rng_seed = 0;

% Generate excitation signals
exc_param = generate_excitation_signal(param,...
    exc_signal_select, rng_seed, n_hidden, Tini);

%% Load real data
u_real_data = csvread('Real Data/PRBS P = 255, T = 10, w = 60/m_u_data_mean.csv').';
y_real_data = csvread('Real Data/PRBS P = 255, T = 10, w = 60/m_y_data_mean.csv').';
u_real_data_init = u_real_data(:,1:exc_param.exc_start_time_d);
y_real_data_init = y_real_data(:,1:exc_param.exc_start_time_d);
u_real_data_exc = u_real_data(:,exc_param.exc_start_time_d+1:end);
y_real_data_exc = y_real_data(:,exc_param.exc_start_time_d+1:end);

%% Check persistency of excitation
H_u_pe_real = data2hankel(u_real_data_exc, Tini + exc_param.Nd + 1 + param.n + param.d);
rank(H_u_pe_real, 1e-3)
if rank(H_u_pe_real, 1e-3) == size(H_u_pe_real,1)
    fprintf('Real data persistenly exciting of order Tini + Nd + 1 + n + d\n\n');
else
    fprintf('Real data *NOT* persistenly exciting\n\n');
end

%% Closed loop simulation
% Set initial condition
param.nrotor_initial_condition(1:3) = y_real_data_init(1:3,1);
param.nrotor_initial_condition(7:9) = y_real_data_init(4:6,1);
% Collect data from closed loop model using same excitation signal
% Data collection mode: 1 => non-linear model
data_collection_mode = 1;
[u_cl_sim_data_exc, y_cl_sim_data_exc,...
    u_cl_sim_data_init, y_cl_sim_data_init,...
    yaw_rate_cmd_cl_sim_data, yaw_cl_sim_data] =...
    collect_DeePC_data(param, exc_param,...
        data_collection_mode, n_hidden, Tini);
u_cl_sim_data = [u_cl_sim_data_init u_cl_sim_data_exc];
y_cl_sim_data = [y_cl_sim_data_init y_cl_sim_data_exc];
if ~param.DeePC_yaw_control
    u_cl_sim_data = [u_cl_sim_data; yaw_rate_cmd_cl_sim_data];
    y_cl_sim_data = [y_cl_sim_data; yaw_cl_sim_data];
end
fprintf('Closed loop sim complete\n\n');


%% Open loop simulatation with real input for model validation
% Find input offsets using a sliding window
window = 25;
u_real_data_offset = zeros(size(u_real_data));
for i = 1 : size(u_real_data, 2)
    start_i = i - window;
    end_i = i + window;
    if start_i < 1
        start_i = 1;
        end_i = window;
    end
    if end_i > size(u_real_data, 2)
        start_i = size(u_real_data, 2) - window;
        end_i = size(u_real_data, 2);
    end
    u_real_data_offset(:,i) = mean(u_real_data(:,start_i:end_i), 2);
end
u_real_data_offset_init = u_real_data_offset(:,1:exc_param.exc_start_time_d);
u_real_data_offset_exc = u_real_data_offset(:,exc_param.exc_start_time_d+1:end);

% 1st simulation: Initial steady state inputs
sim_time = exc_param.exc_sample_time * (exc_param.exc_start_time_d - 1);
sim_time_vector = (0 : exc_param.exc_sample_time : sim_time).';
u_in = u_real_data_init - u_real_data_offset_init + [param.nrotor_vehicle_mass_true * param.g; 0; 0; 0];
u_in_matrix = [sim_time_vector u_in.'];
param.nrotor_initial_condition(1:3) = y_real_data_init(1:3,1);
param.nrotor_initial_condition(7:9) = y_real_data_init(4:6,1);

sim('Crazyflie_InOut');

x_ol_sim_data_init = full_state_meas(:,1:9).';
y_ol_sim_data_init = param.Cd * x_ol_sim_data_init;
if ~param.DeePC_yaw_control
    y_ol_sim_data_init = [y_ol_sim_data_init; full_state_meas(:,9).'];
end

% Fit low polynomials to output data to remove drifts
% Drifts result from input offsets / double-integration errors
dedrift_data_init = true;
if dedrift_data_init
    poly_deg = 2;
    k = 0 : exc_param.exc_start_time_d - 1;
    y_real_poly = polyfitB(k, y_real_data_init(1,:), poly_deg, y_real_data_init(1,1));
    y_real_poly_out = polyval(y_real_poly, k);
    y_sim_poly = polyfitB(k, y_ol_sim_data_init(1,:), poly_deg, y_ol_sim_data_init(1,1));
    y_sim_poly_out = polyval(y_sim_poly, k);
    end_i = param.p;
    if ~param.DeePC_yaw_control
        end_i = end_i + 1;
    end
    for i = 2 : end_i
        y_real_poly = polyfitB(k, y_real_data_init(i,:), poly_deg, y_real_data_init(i,1));
        y_real_poly_out = [y_real_poly_out; polyval(y_real_poly, k)];
        y_sim_poly = polyfitB(k, y_ol_sim_data_init(i,:), poly_deg, y_ol_sim_data_init(i,1));
        y_sim_poly_out = [y_sim_poly_out; polyval(y_sim_poly, k)];
    end
    y_ol_sim_data_init = y_ol_sim_data_init - y_sim_poly_out + y_real_poly_out;
end

% 2nd simulation: Excitation inputs
sim_time = exc_param.exc_sample_time * (exc_param.T - 1);
sim_time_vector = (0 : exc_param.exc_sample_time : sim_time).';
u_in = u_real_data_exc - u_real_data_offset_exc + [param.nrotor_vehicle_mass_true * param.g; 0; 0; 0];
u_in_matrix = [sim_time_vector u_in.'];
param.nrotor_initial_condition(1:3) = y_real_data_exc(1:3,1);
param.nrotor_initial_condition(7:9) = y_real_data_exc(4:6,1);

sim('Crazyflie_InOut');

x_ol_sim_data_exc = full_state_meas(:,1:9).';
y_ol_sim_data_exc = param.Cd * x_ol_sim_data_exc;
if ~param.DeePC_yaw_control
    y_ol_sim_data_exc = [y_ol_sim_data_exc; full_state_meas(:,9).'];
end

% Fit low order polynomials to output data to remove drifts
% Drifts result from input offsets / double-integration errors
dedrift_data_exc = true;
if dedrift_data_exc
    poly_deg = 2;
    k = 0 : exc_param.T - 1;
    y_real_poly = polyfitB(k, y_real_data_exc(1,:), poly_deg, y_real_data_exc(1,1));
    y_real_poly_out = polyval(y_real_poly, k);
    y_sim_poly = polyfitB(k, y_ol_sim_data_exc(1,:), poly_deg, y_ol_sim_data_exc(1,1));
    y_sim_poly_out = polyval(y_sim_poly, k);
    end_i = param.p;
    if ~param.DeePC_yaw_control
        end_i = end_i + 1;
    end
    for i = 2 : end_i
        y_real_poly = polyfitB(k, y_real_data_exc(i,:), poly_deg, y_real_data_exc(i,1));
        y_real_poly_out = [y_real_poly_out; polyval(y_real_poly, k)];
        y_sim_poly = polyfitB(k, y_ol_sim_data_exc(i,:), poly_deg, y_ol_sim_data_exc(i,1));
        y_sim_poly_out = [y_sim_poly_out; polyval(y_sim_poly, k)];
    end
    y_ol_sim_data_exc = y_ol_sim_data_exc - y_sim_poly_out + y_real_poly_out;
end

y_ol_sim_data = [y_ol_sim_data_init y_ol_sim_data_exc];

%% Input Plot
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [0, 0, screenwidth, screenheight];
% thrust sub-plot
subplot(2,2,1);
plot(u_real_data(1,:));
hold on;
plot(u_cl_sim_data(1,:));
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Real vs. simulated total thrust';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$t$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend('$F_{real}$ $[N]$', '$F_{sim}$ $[N]$');
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'northwest';
% roll rate sub-plot
subplot(2,2,2);
plot(u_real_data(2,:));
hold on;
plot(u_cl_sim_data(2,:));
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Real vs. simulated $x$ body angular rate';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$t$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend('$\omega^{(B)}_{x_{real}}$ $[\frac{rad}{s}]$', '$\omega^{(B)}_{x_{sim}}$ $[\frac{rad}{s}]$');
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'northwest';
% pitch rate sub-plot
subplot(2,2,3);
plot(u_real_data(3,:));
hold on;
plot(u_cl_sim_data(3,:));
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Real vs. simulated $y$ body angular rate';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$t$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend('$\omega^{(B)}_{y_{real}}$ $[\frac{rad}{s}]$', '$\omega^{(B)}_{y_{sim}}$ $[\frac{rad}{s}]$');
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'northwest';
% yaw rate sub-plot
subplot(2,2,4);
plot(u_real_data(4,:));
hold on;
plot(u_cl_sim_data(4,:));
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Real vs. simulated $z$ body angular rate';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$t$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend('$\omega^{(B)}_{z_{real}}$ $[\frac{rad}{s}]$', '$\omega^{(B)}_{z_{sim}}$ $[\frac{rad}{s}]$');
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'northwest';

%% Closed Loop Simulation Output Plot
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [0, 0, screenwidth, screenheight];
% x sub-plot
subplot(2,3,1);
plot(y_real_data(1,:));
hold on;
plot(y_cl_sim_data(1,:));
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Real vs. simulated inertial $x$ position';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$t$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend('$p^{(I)}_{x_{real}}$ $[m]$', '$p^{(I)}_{x_{sim}}$ $[m]$');
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'northwest';
% y sub-plot
subplot(2,3,2);
plot(y_real_data(2,:));
hold on;
plot(y_cl_sim_data(2,:));
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Real vs. simulated inertial $y$ position';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$t$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend('$p^{(I)}_{y_{real}}$ $[m]$', '$p^{(I)}_{y_{sim}}$ $[m]$');
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'northwest';
% z sub-plot
subplot(2,3,3);
plot(y_real_data(3,:));
hold on;
plot(y_cl_sim_data(3,:));
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Real vs. simulated inertial $z$ position';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$t$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend('$p^{(I)}_{z_{real}}$ $[m]$', '$p^{(I)}_{z_{sim}}$ $[m]$');
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'northwest';
% roll angle sub-plot
subplot(2,3,4);
plot(y_real_data(4,:));
hold on;
plot(y_cl_sim_data(4,:));
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Real vs. simulated roll angle';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$t$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend('$\gamma_{real}$ $[rad]$', '$\gamma_{sim}$ $[rad]$');
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'northwest';
% pitch angle sub-plot
subplot(2,3,5);
plot(y_real_data(5,:));
hold on;
plot(y_cl_sim_data(5,:));
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Real vs. simulated pitch angle';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$t$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend('$\beta_{real}$ $[rad]$', '$\beta_{sim}$ $[rad]$');
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'northwest';
% yaw angle sub-plot
subplot(2,3,6);
plot(y_real_data(6,:));
hold on;
plot(y_cl_sim_data(6,:));
axes = gca;
y_semi_tight(axes, 1.2);
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Real vs. simulated yaw angle';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$t$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend('$\alpha_{real}$ $[rad]$', '$\alpha_{sim}$ $[rad]$');
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'northwest';

%% Open Loop Simulation Output Plot
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [0, 0, screenwidth, screenheight];
% x sub-plot
subplot(2,3,1);
plot(y_real_data(1,:));
hold on;
plot(y_ol_sim_data(1,:));
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Real vs. open loop simulated x';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend('$x_{real}$ $[m]$', '$x_{olsim}$ $[m]$');
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'southeast';
% y sub-plot
subplot(2,3,2);
plot(y_real_data(2,:));
hold on;
plot(y_ol_sim_data(2,:));
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Real vs. open loop simulated y';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend('$y_{real}$ $[m]$', '$y_{olsim}$ $[m]$');
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'southeast';
% z sub-plot
subplot(2,3,3);
plot(y_real_data(3,:));
hold on;
plot(y_ol_sim_data(3,:));
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Real vs. open loop simulated z';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend('$z_{real}$ $[m]$', '$z_{olsim}$ $[m]$');
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'southeast';
% roll angle sub-plot
subplot(2,3,4);
plot(y_real_data(4,:));
hold on;
plot(y_ol_sim_data(4,:));
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Real vs. open loop simulated roll angle';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend('$\theta_{r_{real}}$ $[rad]$', '$\theta_{r_{olsim}}$ $[rad]$');
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'southeast';
% pitch angle sub-plot
subplot(2,3,5);
plot(y_real_data(5,:));
hold on;
plot(y_ol_sim_data(5,:));
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Real vs. open loop simulated pitch angle';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend('$\theta_{p_{real}}$ $[rad]$', '$\theta_{p_{olsim}}$ $[rad]$');
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'southeast';
% yaw angle sub-plot
subplot(2,3,6);
plot(y_real_data(6,:));
hold on;
plot(y_ol_sim_data(6,:));
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Real vs. open loop simulated yaw angle';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$ $[timesteps]$';
axes.XLabel.FontSize = 14;
lgd = legend('$\theta_{y_{real}}$ $[rad]$', '$\theta_{y_{olsim}}$ $[rad]$');
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'southeast';

%% y-data Hankel matrix SVD
if param.DeePC_yaw_control
    H_y_real = data2hankel(y_real_data_exc, Tini + exc_param.Nd + 1);
else
    H_y_real = data2hankel(y_real_data_exc(1:end-1,:), Tini + exc_param.Nd + 1);
end
H_y_sim = data2hankel(y_cl_sim_data_exc, Tini + exc_param.Nd + 1);
s_real = svd(H_y_real);
s_sim = svd(H_y_sim);

% SVD plot
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [0, 0, screenwidth, screenheight];
plot(sqrt(s_real));
hold on;
plot(sqrt(s_sim));
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Output hankel matrix singular values real vs. simulation';
axes.Title.FontSize = 18;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 10;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = '$k$';
axes.XLabel.FontSize = 14;
lgd = legend('$\sigma(H_{y_{real}})$', '$\sigma(H_{y_{clsim}})$');
lgd.Interpreter = 'latex';
lgd.FontSize = 12;
lgd.Location = 'northeast';

%% Helper function
function y_semi_tight(ax, scale)
    axis(ax, 'tight'); % Set axis tight
    yl = ylim(ax); % Get tight axis limits
    range = yl(2) - yl(1); % Get tight axis range
    sc_range = scale * range; % Scale range
    yl(1) = yl(1) - (sc_range - range) / 2; % New ymin
    yl(2) = yl(1) + sc_range; % New ymax
    ylim(ax, yl);
end