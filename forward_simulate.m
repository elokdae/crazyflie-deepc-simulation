function y_sim = forward_simulate(param, data_collection_mode,...
    uini, yini, uf)

%% Warp [uini, yini]
uini = reshape(uini, param.m, []);
yini = reshape(yini, param.p, []);

%% Set initial state to initial yini
param_backup = param;
param.nrotor_initial_condition(1) = yini(1,1);
param.nrotor_initial_condition(2) = yini(2,1);
param.nrotor_initial_condition(3) = yini(3,1);
if param.measure_angles
    param.nrotor_initial_condition(7) = yini(4,1);
    param.nrotor_initial_condition(8) = yini(5,1);
end
if param.DeePC_yaw_control
    param.nrotor_initial_condition(9) = yini(end,1);
end
assignin('base','param', param);

%% Set input to simulation
if ~param.DeePC_yaw_control
    uini = [uini; zeros(1, size(uini, 2))];
    uf = [uf; zeros(1, size(uf, 2))];
end
u_in = [uini, uf].';
sim_time = size(u_in, 1) * param.sample_time_controller_outer;
time_vec = 0 : param.sample_time_controller_outer : sim_time - param.sample_time_controller_outer;
u_in_matrix = [time_vec.', u_in];
assignin('base','u_in_matrix', u_in_matrix);

%% Perform simulation

assignin('base','sim_time', sim_time);
if data_collection_mode == 0 %|| data_collection_mode == 1
    sim('Crazyflie_Linear_Model_Gravity_InOut');
else
    sim('Crazyflie_InOut');
end

%% Set parameters back to original
param = param_backup;
assignin('base','param', param);

%% Get CLEAN (from actual not measure state) y-data
x_sim = full_state_act(:,1:9).';
y_sim = param.Cd * x_sim;

end

