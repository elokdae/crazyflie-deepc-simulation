function [DeePC_param, yalmip_optimizer, yalmip_slack_optimizer]...
    = setup_DeePC_yalmip(param, exc_param,...
    u_data, y_data,...
    Tini, svd_weighing, svd_cutoff, lambda1_g, lambda2_g, lambda1_s, lambda2_s,...
    Q_x, Q_y, Q_z, Q_yaw, R_thrust, R_roll, R_pitch, R_yaw,...
    P_x, P_x_pitch, P_y, P_y_roll, P_z, P_roll, P_pitch, P_yaw)

%% DECISION VARIABLES: [g]
% g: Trajectory mapper [U_p; Y_p; U_f; Y_f] * g = [uini; yini; u; y] [R^Ng]

%% Hankel matrices
H_u = data2hankel(u_data, Tini + exc_param.Nd + 1);
H_y = data2hankel(y_data, Tini + exc_param.Nd + 1);
DeePC_param.U_p = H_u(1:param.m*Tini,:);
DeePC_param.U_f = H_u(param.m*Tini+1:end-param.m,:);
DeePC_param.Y_p = H_y(1:param.p*Tini,:);
DeePC_param.Y_f = H_y(param.p*Tini+1:end,:);

%% SVD Weighing
DeePC_param.Ng = size(DeePC_param.U_p, 2);
[~, ~, DeePC_param.V] = svd(H_y);
if svd_weighing
    DeePC_param.Wg = DeePC_param.V * blkdiag(zeros(svd_cutoff), eye(DeePC_param.Ng - svd_cutoff)) * DeePC_param.V.';
else
    DeePC_param.Wg = eye(DeePC_param.Ng);
end

%% YALMIP input variables
% These variables are passed to YALMIP optimizer in every iteration
% They are used to define dynamic objectives/constraints
DeePC_param.NuIni = param.m * Tini;
DeePC_param.NyIni = param.p * Tini;
uini_yalmip = sdpvar(DeePC_param.NuIni, 1);
yini_yalmip = sdpvar(DeePC_param.NyIni, 1);
r_yalmip = sdpvar(param.p, 1);
gs_yalmip = sdpvar(DeePC_param.Ng, 1);

%% Reference outputs
% Reference is x, y, z, yaw
% If DeePC yaw control is disabled, do not track yaw
if param.DeePC_yaw_control
    if param.measure_angles
        DeePC_param.ref_outputs = [1; 2; 3; 6];
    else
        DeePC_param.ref_outputs = [1; 2; 3; 4];
    end
    DeePC_param.ref_states = [1; 2; 3; 9];
else
    DeePC_param.ref_outputs = [1; 2; 3];
    DeePC_param.ref_states = [1; 2; 3];
end
DeePC_param.Nref = length(DeePC_param.ref_outputs);

%% Cost matrices
DeePC_param.Q = zeros(param.p); % Output cost matrix
DeePC_param.Q(1,1) = Q_x;
DeePC_param.Q(2,2) = Q_y;
DeePC_param.Q(3,3) = Q_z;
DeePC_param.R = zeros(param.m); % Input cost matrix
DeePC_param.R(1,1) = R_thrust;
DeePC_param.R(2,2) = R_roll;
DeePC_param.R(3,3) = R_pitch;
DeePC_param.P = zeros(param.p); % Terminal cost matrix
DeePC_param.P(1,1) = P_x;
DeePC_param.P(2,2) = P_y;
DeePC_param.P(3,3) = P_z;
if param.measure_angles
    DeePC_param.P(1,5) = P_x_pitch;
    DeePC_param.P(2,4) = P_y_roll;
    DeePC_param.P(4,2) = P_y_roll;
    DeePC_param.P(4,4) = P_roll;
    DeePC_param.P(5,1) = P_x_pitch;
    DeePC_param.P(5,5) = P_pitch;
end
if param.DeePC_yaw_control
    DeePC_param.Q(end,end) = Q_yaw;
    DeePC_param.R(end,end) = R_yaw;
    DeePC_param.P(end,end) = P_yaw;
end

%% Quadratic cost matrix in style of quadprog
Nu = param.m * exc_param.Nd;
Ny = param.p * exc_param.Nd;
DeePC_param.Ns = DeePC_param.NyIni;
H_g = DeePC_param.U_f.' * kron(eye(exc_param.Nd), DeePC_param.R) * DeePC_param.U_f;
H_g = H_g + DeePC_param.Y_f(1:end-param.p,:).' * kron(eye(exc_param.Nd), DeePC_param.Q) * DeePC_param.Y_f(1:end-param.p,:);
H_g = H_g + DeePC_param.Y_f(end-param.p+1:end,:).' * DeePC_param.P * DeePC_param.Y_f(end-param.p+1:end,:);
H_g = H_g + lambda2_g * DeePC_param.Wg;
H_g = H_g + lambda2_s * DeePC_param.Y_p.' * DeePC_param.Y_p;
H = H_g;
H_min_eig = min(eig(H))

%% Linear cost vector in style of quadprog
if param.DeePC_yaw_control
    DeePC_param.us = [param.nrotor_vehicle_mass_for_controller * param.g;...
        0; 0; 0];
else
    DeePC_param.us = [param.nrotor_vehicle_mass_for_controller * param.g;...
        0; 0];
end
us_vec = repmat(DeePC_param.us, exc_param.Nd, 1);
r_vec_yalmip = repmat(r_yalmip, exc_param.Nd, 1);

f_g = -us_vec.' * kron(eye(exc_param.Nd), DeePC_param.R) * DeePC_param.U_f;
f_g = f_g - r_vec_yalmip.' * kron(eye(exc_param.Nd), DeePC_param.Q) * DeePC_param.Y_f(1:end-param.p,:);
f_g = f_g - r_yalmip.' * DeePC_param.P * DeePC_param.Y_f(end-param.p+1:end,:);
f_g = f_g - gs_yalmip.' * lambda2_g * DeePC_param.Wg;
f_g = f_g - yini_yalmip.' * lambda2_s * DeePC_param.Y_p;
f_g = f_g.';
f = f_g;

%% Input jerk constraints
thrust_jerk_min = -inf;
thrust_jerk_max = inf;
body_rates_jerk_min = -inf * [1; 1; 1];
body_rates_jerk_max = inf * [1; 1; 1];
input_jerk_min = eye(param.m, 4) * [thrust_jerk_min; body_rates_jerk_min];
input_jerk_min(isnan(input_jerk_min)) = -inf;
input_jerk_max = eye(param.m, 4) * [thrust_jerk_max; body_rates_jerk_max];
input_jerk_max(isnan(input_jerk_max)) = inf;

%% Output jerk constraints
position_jerk_min = -inf * [1; 1; 1];
position_jerk_max = inf * [1; 1; 1];
velocity_jerk_min = -inf * [1; 1; 1];
velocity_jerk_max = inf * [1; 1; 1];
angle_jerk_min = -inf * [1; 1; 1];
angle_jerk_max = inf * [1; 1; 1];
output_jerk_min = param.Cd * [position_jerk_min; velocity_jerk_min; angle_jerk_min];
output_jerk_min(isnan(output_jerk_min)) = -inf;
output_jerk_max = param.Cd * [position_jerk_max; velocity_jerk_max; angle_jerk_max];
output_jerk_max(isnan(output_jerk_max)) = inf;

%% Inequality constraints in style of quadprog
A = -DeePC_param.U_f;
A = [A; DeePC_param.U_f];
A = [A; -DeePC_param.Y_f];
A = [A; DeePC_param.Y_f];
% Input jerk constraints: Implements a discrete 2nd derivative
% [u(k+2) - u(k+1)] - [u(k+1) - u(k)] = u(k+2) - 2u(k+1) + u(k)
U_f_jerk = DeePC_param.U_f(2*param.m+1:end,:)...
    - 2 * DeePC_param.U_f(param.m+1:end-param.m,:)...
    + DeePC_param.U_f(1:end-2*param.m,:);
A = [A; -U_f_jerk];
A = [A; U_f_jerk];
% Output jerk constraints: Implements a discrete 2nd derivative
% [y(k+2) - y(k+1)] - [y(k+1) - y(k)] = y(k+2) - 2y(k+1) + y(k)
Y_f_jerk = DeePC_param.Y_f(2*param.p+1:end,:)...
    - 2 * DeePC_param.Y_f(param.p+1:end-param.p,:)...
    + DeePC_param.Y_f(1:end-2*param.p,:);
A = [A; -Y_f_jerk];
A = [A; Y_f_jerk];

b = -repmat(param.input_min, exc_param.Nd, 1);
b = [b; repmat(param.input_max, exc_param.Nd, 1)];
b = [b; -repmat(param.output_min, exc_param.Nd + 1, 1)];
b = [b; repmat(param.output_max, exc_param.Nd + 1, 1)];
% Input jerk constraints
b = [b; -repmat(input_jerk_min, exc_param.Nd - 2, 1)];
b = [b; repmat(input_jerk_max, exc_param.Nd - 2, 1)];
% Output jerk constraints
b = [b; -repmat(output_jerk_min, exc_param.Nd - 1, 1)];
b = [b; repmat(output_jerk_max, exc_param.Nd - 1, 1)];

lb = [-inf * ones(DeePC_param.Ng, 1)];
ub = [inf * ones(DeePC_param.Ng, 1)];

%% Equality constraints in style of quadprog - main optimization
Aeq = DeePC_param.U_p;
beq = uini_yalmip;
if param.two_stage_opt
    Aeq = [Aeq; DeePC_param.Y_p];
    beq = [beq; yini_yalmip];
end

%% Equality constraints in style of quadprog - slack optimization
% Find minimum slack that satisfies equality constraint first
if param.two_stage_opt
    Aeq_slack = [DeePC_param.U_p zeros(DeePC_param.NuIni, DeePC_param.Ns)];
    Aeq_slack = [Aeq_slack; DeePC_param.Y_p -eye(DeePC_param.Ns)];
end

%% YALMIP setup - main optimization
if param.verbose
    options = sdpsettings('solver', param.yalmip_solver, 'verbose', 2);
else
    options = sdpsettings('solver', param.yalmip_solver, 'verbose', 0);
end

% OSQP tolerances
options.osqp.eps_abs = 1e-6;
options.osqp.eps_rel = 1e-6;
options.osqp.eps_prim_inf = 1e-6;
options.osqp.eps_dual_inf = 1e-6;

% Decision variables
x_yalmip = sdpvar(DeePC_param.Ng, 1);

% Constraints
constraints = A * x_yalmip <= b;
constraints = [constraints; lb <= x_yalmip];
constraints = [constraints; x_yalmip <= ub];
constraints = [constraints; Aeq * x_yalmip == beq];

% Objective
objective = 1/2 * x_yalmip.' * H * x_yalmip;
objective = objective + f.' * x_yalmip;
if svd_weighing
    Vt_g = DeePC_param.V.' * x_yalmip(1:DeePC_param.Ng);
    Vt_gs = DeePC_param.V.' * gs_yalmip;
    objective = objective + lambda1_g * norm(Vt_g(svd_cutoff+1:end) - Vt_gs(svd_cutoff+1:end), 1);
else
    objective = objective + lambda1_g * norm(x_yalmip(1:DeePC_param.Ng) - gs_yalmip, 1);
end
objective = objective + lambda1_s * norm(x_yalmip(DeePC_param.Ng+1:end), 1);

yalmip_optimizer = optimizer(constraints, objective, options,...
    [uini_yalmip; yini_yalmip; r_yalmip; gs_yalmip], [x_yalmip; DeePC_param.Y_p * x_yalmip - yini_yalmip; DeePC_param.us; objective]);

%% YALMIP setup - slack optimization
% Find minimum slack that satisfies equality constraint first
% Everything stays the same except constraints and objective
if param.two_stage_opt
    % Constraints - only care about equality constraint
    constraints_slack = Aeq_slack * x_yalmip == beq;

    % Objective - only penalize slack
    objective_slack = 1/2 * x_yalmip(DeePC_param.Ng+1:end).' * x_yalmip(DeePC_param.Ng+1:end);

    yalmip_slack_optimizer = optimizer(constraints_slack, objective_slack, options,...
        [uini_yalmip; yini_yalmip], x_yalmip(DeePC_param.Ng+1:end));
else
    yalmip_slack_optimizer = [];
end
end