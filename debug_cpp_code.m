clc;
clear;

matlab_var = csvread('osqp_A.csv');
cpp_var = csvread('~/work/D-FaLL-System/Deepc_data/output/osqp_A.csv');
diff = abs(cpp_var - matlab_var);
max(diff(:))